/**
 * @param {Torchlight.lib.Range} armorRange
 * @param {string} armorType
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.DualWieldBonus = function DualWieldBonus(bonusPercent) {
    this.toString = function toString() {
        return bonusPercent + "% Damage bonus when dual-wielding";
    }
};
Torchlight.t2.effects.DualWieldBonus.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
