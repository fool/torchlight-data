
Torchlight.t2.skills.berserker.shadow.Wolfpack = function Wolfpack() {
    this.name        = "wolfpack";
    this.displayName = "Wolfpack";
    this.description = "You unleash a pack of 5 spectral wolves that spread out and attack enemies all around you.";
    this.tree        = "shadow";
    this.skillLevelRequiredTier = 2;
    this.manaCost    = [0, 42, 43, 44, 46, 48, 51, 54, 58, 62, 67, 72, 78, 84, 91, 98];
    this.range       = [0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5];
    this.tierBonuses = [
        "Wolves can strike multiple targets",
        "2 additional wolves appear",
        "Mana cost decreased"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %",    calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Physical Damage", calculatePhysicalDamage)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var physicalDamage = calculatePhysicalDamage(skillLevel, playerLevel);

        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.Damage(new Torchlight.lib.Range(physicalDamage), "Physical")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 5 * (skillLevel - 1);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculatePhysicalDamage(skillLevel, playerLevel) {
        return 10.5 - (0.5 * (skillLevel + (2 * playerLevel)));
    }
};
Torchlight.t2.skills.berserker.shadow.Wolfpack.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
