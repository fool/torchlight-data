
Torchlight.t2.skills.outlander.warfare.RuneVault = function RuneVault() {
    this.name = "runevault";
    this.displayName = "Rune Vault";
    this.description = "You vault swiftly backward, away from your target. This vault leaves behind a mystic sigil that damages and blinds foes in a four-meter radius";
    this.tree = "warfare";
    this.skillLevelRequiredTier = 1;
    this.manaCost = [0, 9, 10, 11, 12, 12, 13, 15, 16, 18, 20, 23, 24, 28, 31, 34];
    this.tierBonuses = [
        "Sigil radius extended to 5 meters. Blindness extended to 4 seconds. Allows use of health and mana steal abilities",
        "Knockback added. Blindness extended to 5 seconds. Allows use of all weapon-based abilities",
        "Radius extended to 6 meters. Blindness extended to 6 seconds. Builds Charge"
    ];
    this.getRange = function getRange(skillLevel) {
        return Torchlight.lib.squeeze([4, 5, 5, 6], skillLevel / 5);
    };
    var knockback = 30;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Knockback", knockback),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Blind Chance", calculateBlindChance),
        new Torchlight.t2.skills.Attribute(this, "Blind Duration", calculateBlindDuration)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var blindChance = calculateBlindChance(skillLevel);
        var blindDuration = calculateBlindDuration(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.Blindness(blindChance, blindDuration),
            new Torchlight.t2.effects.Knockback(knockback)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 7 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBlindChance(skillLevel) {
        return 47 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBlindDuration(skillLevel) {
        return Torchlight.lib.squeeze([3, 4, 5, 6], skillLevel / 5);
    }

};
Torchlight.t2.skills.outlander.warfare.RuneVault.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
