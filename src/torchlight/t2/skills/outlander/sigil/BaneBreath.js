
Torchlight.t2.skills.outlander.sigil.BaneBreath = function BaneBreath() {
    this.name = "banebreath";
    this.displayName = "Bane Breath";
    this.description = "A blast of cursed breath poisons enemies. If they are killed, they are converted into Shadowling Fiends that fight for you for ten seconds.";
    this.tree = "sigil";
    this.skillLevelRequiredTier = 2;
    this.manaCost = [0, 15, 15, 16, 16, 17, 18, 19, 21, 23, 26, 28, 31, 34, 37, 41 ];
    var cooldown = 0.7;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "Shadowlings move and attack 10% faster",
        "Shadowlings move and attack 20% faster",
        "Shadowlings move and attack 30% faster"
    ];
    var poisonDamageDuration = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Poison Damage", calculatePoisonDamage),
        new Torchlight.t2.skills.Attribute(this, "Poison Damage Added", calculatePosionAddDamage),
        new Torchlight.t2.skills.Attribute(this, "Minions Damage", calculateMinionsDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var poisonDamage = calculatePoisonDamage(skillLevel, playerLevel);
        var poisonDamageAdded = calculatePosionAddDamage(skillLevel, playerLevel);
        var minionsDamage = calculateMinionsDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.Damage(poisonDamageAdded, "Poison"),
            new Torchlight.t2.effects.Damage(poisonDamage, "Poison", poisonDamageDuration),
            new Torchlight.t2.effects.MinionsDealDamage(minionsDamage, "Physical"),
            new Torchlight.t2.effects.ExplosionOnDeath()
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculatePoisonDamage(skillLevel, playerLevel) {
        var skill = (skillLevel / 3);
        var player = (playerLevel / 20);
        var lower = 300 + Math.pow(skill + 0.5, player); // 5k to 7.7k on lvl 15 / 100... ??
        var upper = 350 + Math.pow(skill + 1,   player);
        return new Torchlight.lib.Range(lower, upper);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculatePosionAddDamage(skillLevel, playerLevel) {
        var skill = (skillLevel / 3);
        var player = (playerLevel / 20);
        var lower = 300 + Math.pow(skill + 0.5, player); // 5k to 7.7k on lvl 15 / 100... ??
        var upper = 350 + Math.pow(skill + 1,   player);
        return new Torchlight.lib.Range(lower, upper);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateMinionsDamage(skillLevel, playerLevel) {
        var skill = (skillLevel / 3);
        var player = (playerLevel / 20);
        var lower = 300 + Math.pow(skill + 0.5, player); // 5k to 7.7k on lvl 15 / 100... ??
        var upper = 350 + Math.pow(skill + 1,   player);
        return new Torchlight.lib.Range(lower, upper);
    }

};
Torchlight.t2.skills.outlander.sigil.BaneBreath.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
