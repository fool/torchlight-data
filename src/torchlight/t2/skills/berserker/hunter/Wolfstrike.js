
Torchlight.t2.skills.berserker.hunter.Wolfstrike = function Wolfstrike() {
    this.manaCost = [0, 18, 19, 20, 20, 20, 22, 23, 24, 27, 29, 31, 34, 38, 41, 45];
    this.range = [0, 6, 6, 6, 6, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 9];
    var cooldown = 0.5;
    this.cooldown = [cooldown];
    this.skillLevelRequiredTier = 3;
    this.name = "wolfstrike";
    this.displayName = "Wolfstrike";
    this.description = "You lunge forward in a 6 meter dash, claws slashing, rending all foes in your path.";
    this.tree = "hunter";
    this.tierBonuses = [
        "Lunge distance increased to 7 meters",
        "Lunge distance increased to 8 meters",
        "Lunge distance increased to 9 meters"
    ];
    var stunDuration = 3;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown",      cooldown),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", stunDuration),
        new Torchlight.t2.skills.Attribute(this, "% DPS Added",   calculateWeaponDpsDamage),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance",   calculateStunChance),
        new Torchlight.t2.skills.Attribute(this, "Knockback",     calculateKnockback)
    ];

    /**
     * @param {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var weaponDps  = calculateWeaponDpsDamage(skillLevel);
        var stunChance = calculateStunChance(skillLevel);
        var knockback  = calculateKnockback(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Physical"),
            new Torchlight.t2.effects.Stun(stunChance, stunDuration),
            new Torchlight.t2.effects.Knockback(knockback)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDpsDamage(skillLevel) {
        return 84 + (4 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunChance(skillLevel) {
        return 30 + (3 * skillLevel);
    }

    /**
     * @param skillLevel
     * @returns {number}
     */
    function calculateKnockback(skillLevel) {
        return 20 + (1 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.hunter.Wolfstrike.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
