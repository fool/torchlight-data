/**
 * @param {number} multiplier
 * @constructor
 */
Torchlight.t2.effects.DamageShieldArmorMultiplier = function DamageShieldArmorMultiplier(multiplier) {
    this.toString = function toString() {
        return "Damage is increased to " + multiplier + " times your Shield's Armor";
    }
};
Torchlight.t2.effects.DamageShieldArmorMultiplier.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
