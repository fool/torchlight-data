Torchlight.t2.classes.berserker = {};
Torchlight.t2.classes.berserker.skills = {
    hunter: [
        new Torchlight.t2.skills.berserker.hunter.Eviscerate(),
        new Torchlight.t2.skills.berserker.hunter.Howl(),
        new Torchlight.t2.skills.berserker.hunter.Raze(),
        new Torchlight.t2.skills.berserker.hunter.Wolfstrike(),
        new Torchlight.t2.skills.berserker.hunter.BattleRage(),
        new Torchlight.t2.skills.berserker.hunter.Rupture(),
        new Torchlight.t2.skills.berserker.hunter.Ravage(),
        new Torchlight.t2.skills.berserker.hunter.BloodHunger(),
        new Torchlight.t2.skills.berserker.hunter.Executioner(),
        new Torchlight.t2.skills.berserker.hunter.Rampage()
    ],
    tundra: [
        new Torchlight.t2.skills.berserker.tundra.FrostBreath(),
        new Torchlight.t2.skills.berserker.tundra.Stormclaw(),
        new Torchlight.t2.skills.berserker.tundra.StormHatchet(),
        new Torchlight.t2.skills.berserker.tundra.NorthernRage(),
        new Torchlight.t2.skills.berserker.tundra.IceShield(),
        new Torchlight.t2.skills.berserker.tundra.Permafrost(),
        new Torchlight.t2.skills.berserker.tundra.GlacialShatter(),
        new Torchlight.t2.skills.berserker.tundra.ColdSteelMastery(),
        new Torchlight.t2.skills.berserker.tundra.ShatterStorm(),
        new Torchlight.t2.skills.berserker.tundra.RageRetaliation()
    ],
    shadow: [
        new Torchlight.t2.skills.berserker.shadow.ShadowBurst(),
        new Torchlight.t2.skills.berserker.shadow.WolfShade(),
        new Torchlight.t2.skills.berserker.shadow.Shadowbind(),
        new Torchlight.t2.skills.berserker.shadow.SavageRush(),
        new Torchlight.t2.skills.berserker.shadow.ChainSnare(),
        new Torchlight.t2.skills.berserker.shadow.BattleStandard(),
        new Torchlight.t2.skills.berserker.shadow.Wolfpack(),
        new Torchlight.t2.skills.berserker.shadow.FrenzyMastery(),
        new Torchlight.t2.skills.berserker.shadow.ShredArmor(),
        new Torchlight.t2.skills.berserker.shadow.RedWolf()
    ]
};
Torchlight.t2.classes.berserker.getSkill = function getSkill(name) {
    return Torchlight.t2.classes.character.getSkillFromTree(name, Torchlight.t2.classes.berserker.skills);
};
