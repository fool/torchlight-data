
Torchlight.t2.skills.outlander.lore.Sandstorm = function Sandstorm() {
    this.skillLevelRequiredTier = 3;
    this.name        = "sandstorm";
    this.displayName = "Sandstorm";
    this.description = "You hurl your glaive, generating a whirlwind that cuts through foes and saps their health. " +
                       "The sandstorm travels 20 meters and generates 1.5% of your charge bar Charge for every " +
                       "enemy hit.";
    this.tree        = "lore";
    this.manaCost    = [0, 18, 19, 19, 19, 21, 22, 23, 25, 27, 29, 32, 35, 38, 42, 45];
    this.getRange    = function getRange(skillLevel) {
        return Torchlight.lib.squeeze([20, 20, 40, 50], skillLevel / 5);
    };
    this.tierBonuses = [
        "Killed foes emit clouds of plague flies",
        "Range extended to 40 meters",
        "Range extended to 50 meters"
    ];
    var knockback   = 15;
    var stunChance  = 100;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Knockback", knockback),
        new Torchlight.t2.skills.Attribute(this, "Stun", stunChance),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", calculateStunDuration),
        new Torchlight.t2.skills.Attribute(this, "Physical Damage", calculatePhysicalDamage)

    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var stunDuration = calculateStunDuration(skillLevel);
        var physicalDamage = calculatePhysicalDamage(skillLevel);
        var effects = [
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.Stun(stunChance, stunDuration),
            new Torchlight.t2.effects.Damage(physicalDamage, "Physical")
        ];

        return effects;
    };


    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunDuration(skillLevel) {
        return 0.9 + (0.1 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculatePhysicalDamage(skillLevel) {
        return new Torchlight.lib.Range(8 + (4 * skillLevel));
    }
};
Torchlight.t2.skills.outlander.lore.Sandstorm.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
