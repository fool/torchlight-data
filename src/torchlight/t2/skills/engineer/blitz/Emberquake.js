
Torchlight.t2.skills.engineer.blitz.Emberquake = function Emberquake() {
    this.name = "emberquake";
    this.displayName = "Emberquake";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "blitz";
    this.skillLevelRequiredTier = 6;
    this.manaCost = [0, 42, 43, 43, 44, 46, 48, 49, 51, 54, 57, 59, 63, 66, 71, 74];
    var cooldown = 1;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "Fire damage increased by 20%",
        "Fire damage increased by 40%",
        "Fire damage increased by 60%"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage", calculateFireDamage),
        new Torchlight.t2.skills.Attribute(this, "Chance to Burn", calculateChanceToBurn),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDps)
    ];

    /**
     * http://i.imgur.com/eT4o7.png
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var fireDamage = new Torchlight.lib.Range(calculateFireDamage(skillLevel, playerLevel));
        var burnChance = calculateChanceToBurn(skillLevel);
        var weaponDps = calculateWeaponDps(skillLevel);
        return [
            new Torchlight.t2.effects.Damage(fireDamage, "Fire"),
            new Torchlight.t2.effects.spells.Burn(burnChance),
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Fire")
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateFireDamage(skillLevel, playerLevel) {
        return 79 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToBurn(skillLevel) {
        return 18 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 33 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.blitz.Emberquake.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
