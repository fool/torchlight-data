
Torchlight.t2.skills.embermage.inferno.InfernalCollapse = function InfernalCollapse() {
    this.name        = "infernalcollapse";
    this.displayName = "Infernal Collapse";
    this.description = "You create a concentrated sphere of heat, which suddenly expands outward in a devastating 4 meter blast.";
    this.tree        = "inferno";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 27, 28, 28, 29, 30, 31, 31, 33, 35, 39, 42, 46, 49, 54, 58];
    this.range    = [0,  4,  4,  4,  4,  5,  5,  5,  5,  5,  6,  6,  6,  6,  6,  7];
    this.getCooldown = function getCooldown(skillLevel) {
        return Math.max(2.6 - (0.1 * skillLevel), 0);
    };
    this.tierBonuses = [
        "Blast radius increased to 5 meters. Increased Knockback.",
        "Blast radius increased to 6 meters. Increased Knockback.",
        "Blast radius increased to 7 meters. Increased Knockback."
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage", calculateFireDamage),
        new Torchlight.t2.skills.Attribute(this, "Knockback", calculateKnockback)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var fireDamage = calculateFireDamage(skillLevel, playerLevel);
        var knockback = calculateKnockback(skillLevel);
        return [
            new Torchlight.t2.effects.Damage(fireDamage, "Fire"),
            new Torchlight.t2.effects.Knockback(knockback)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateFireDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(600, 1000);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateKnockback(skillLevel) {
        return Torchlight.lib.squeeze([30, 40, 50, 65], skillLevel / 5);
    }
};
Torchlight.t2.skills.embermage.inferno.InfernalCollapse.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
