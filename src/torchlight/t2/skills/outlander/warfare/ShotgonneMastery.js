
Torchlight.t2.skills.outlander.warfare.ShotgonneMastery = function ShotgonneMastery() {
    this.name = "shotgonnemastery";
    this.displayName = "Shotgonne Mastery";
    this.description = "Your expertise with shotgonnes increases the chance that your blasts shock and disorient your targets";
    this.tree = "warfare";
    this.passive = true;
    this.skillLevelRequiredTier = 1;
    var stunDuration = 2;
    var blindDuration = 3;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", stunDuration),
        new Torchlight.t2.skills.Attribute(this, "Blind Duration", blindDuration),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance", calculateStunChance),
        new Torchlight.t2.skills.Attribute(this, "Knockback", calculateKnockback),
        new Torchlight.t2.skills.Attribute(this, "Blind Chance", calculateBlindChance)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var stunChance = calculateStunChance(skillLevel);
        var knockback = calculateKnockback(skillLevel);
        var blindChance = calculateBlindChance(skillLevel);
        return [
            new Torchlight.t2.effects.Stun(stunChance, stunDuration),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.Blindness(blindChance, 100, blindDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunChance(skillLevel) {
        return 0 + (4 *skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateKnockback(skillLevel) {
        return 0 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBlindChance(skillLevel) {
        return 15 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.warfare.ShotgonneMastery.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
