
Torchlight.t2.skills.engineer.aegis.ChargeReconstitution = function ChargeReconstitution() {
    this.name = "chargereconstitution";
    this.displayName = "Charge Reconstitution";
    this.description = "Whenever the Engineer spends a Charge on an ability, they regain a portion of their health over the next two seconds.";
    this.tree = "aegis";
    this.passive = true;
    this.skillLevelRequiredTier = 2;
    var healthRecoveryDuration = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "healthRecoveryDuration", healthRecoveryDuration),
        new Torchlight.t2.skills.Attribute(this, "Health Recovered", calculateHealthRecovered)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var healthRecovered = calculateHealthRecovered(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.buffs.Recover(healthRecovered, Torchlight.t2.effects.Effect.recover.Health, healthRecoveryDuration)
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateHealthRecovered(skillLevel, playerLevel) {
        return skillLevel + playerLevel;
    }
};
Torchlight.t2.skills.engineer.aegis.ChargeReconstitution.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
