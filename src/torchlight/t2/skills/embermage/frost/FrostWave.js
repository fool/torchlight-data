
Torchlight.t2.skills.embermage.frost.FrostWave = function FrostWave() {
    this.name = "frostwave";
    this.displayName = "Frost Wave";
    this.description = "You hurl forth a wave of ice shards, impaling and freezing foes in its path.";
    this.tree = "frost";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 21, 44];
    var iceDuration = 7;
    this.tierBonuses = [
        "A spear of 3 frost waves are released",
        "Frost wave s peed increased",
        "A speard of 5 frost waves are released"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Ice Duration", iceDuration),
        new Torchlight.t2.skills.Attribute(this, "Ice Damage", calculateIceDamage),
        new Torchlight.t2.skills.Attribute(this, "Freeze Chance", calculateChanceToFreeze),
        new Torchlight.t2.skills.Attribute(this, "Ice Damage Over Time", calculateIceDamageOverTime)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var iceDamage = calculateIceDamage(skillLevel, playerLevel);
        var freezeChance = calculateChanceToFreeze(skillLevel);
        var iceDamageOverTime = calculateIceDamageOverTime(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.Damage(iceDamage, "Ice"),
            new Torchlight.t2.effects.spells.Freeze(freezeChance, iceDuration),
            new Torchlight.t2.effects.Damage(iceDamageOverTime, "Ice", iceDuration)
        ];
    };

    /**
     *
     * @param   {number}skillLevel
     * @param   {number}playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateIceDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(800, 850);
    }

    /**
     *
     * @param   {number}skillLevel
     * @returns {number}
     */
    function calculateChanceToFreeze(skillLevel) {
        return 50 + (3 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateIceDamageOverTime(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(900);
    }

};
Torchlight.t2.skills.embermage.frost.FrostWave.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
