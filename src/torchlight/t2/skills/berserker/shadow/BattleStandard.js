
Torchlight.t2.skills.berserker.shadow.BattleStandard = function BattleStandard() {
    this.name        = "battlestandard";
    this.displayName = "Battle Standard";
    this.description = "The ancient standard of your tribe imbues you and your allies with increased evasion, resistance to knockback, and increased charge gain while within its 9 meter influence.";
    this.tree        = "shadow";
    this.skillLevelRequiredTier = 5;
    this.manaCost = [0, 23, 23, 24, 24, 25, 25, 26, 27, 29, 31, 34, 36, 39, 41, 44];
    this.duration = [0, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 50];
    this.range    = [0,  9,  9,  9,  9,  9,  9,  9,  9,  9, 16, 16, 16, 16, 16, 16];
    var cooldown  = 15;
    this.cooldown = [cooldown];
    var effectDuration = 4;
    this.tierBonuses   = [
        "Increased mana recharge",
        "Influence range increased to 15 metres",
        "Standard duration doubled "
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown",             cooldown),
        new Torchlight.t2.skills.Attribute(this, "Effect Duration",      effectDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana",                 this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Duration",             this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "Range",                this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Dodge %",              calculateDodgeRate),
        new Torchlight.t2.skills.Attribute(this, "Knockback Resistance", calculateKnockbackResistance),
        new Torchlight.t2.skills.Attribute(this, "Charge Rate Increase", calculateChargeRate),
        new Torchlight.t2.skills.Attribute(this, "Mana Recovery Rate",   calculateManaRecovery,         5)
    ];

    /**
     * @param {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var dodgeRate = calculateDodgeRate(skillLevel);
        var knockbackResistance = calculateKnockbackResistance(skillLevel);
        var chargeRate = calculateChargeRate(skillLevel);

        var effects = [
            new Torchlight.t2.effects.Dodge(dodgeRate, effectDuration),
            new Torchlight.t2.effects.KnockbackResistance(knockbackResistance, effectDuration),
            new Torchlight.t2.effects.ChargeRate(chargeRate, effectDuration)
        ];

        if (skillLevel >= 5) {
            var manaRecovery = calculateManaRecovery(skillLevel);
            effects.push(new Torchlight.t2.effects.buffs.Recover(manaRecovery, Torchlight.t2.effects.Effect.recover.Mana, effectDuration);
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDodgeRate(skillLevel) {
        return 10 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateKnockbackResistance(skillLevel) {
        return 68 + (2 * skillLevel);
    }

    /**
     * @param   {number}  skillLevel
     * @returns {number}
     */
    function calculateChargeRate(skillLevel) {
        return 10 + (2 * skillLevel);
    }

    /**
     * @param   {number}  skillLevel
     * @returns {number}
     */
    function calculateManaRecovery(skillLevel) {
        return 22 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.shadow.BattleStandard.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
