/**
 *
 * @param {string} stat
 * @param {number} multiplier
 * @constructor
 */
Torchlight.t2.effects.DamageEqualToStat = function DamageEqualToStat(stat, multiplier) {
    this.toString = function toString() {
        return "Damage equal to " + multiplier +"x your " + stat;
    }
};
Torchlight.t2.effects.DamageEqualToStat.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
