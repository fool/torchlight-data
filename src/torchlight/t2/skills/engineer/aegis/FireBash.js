
Torchlight.t2.skills.engineer.aegis.FireBash = function FireBash() {
    this.skillLevelRequiredTier = 5;
    this.name        = "firebash";
    this.displayName = "Fire Bash";
    this.description = "You channel a potent blast of energy through your shield up to six meters away. Damage is " +
                       "derived from your shield’s armour value. Initial physical damage is tripled and the blast " +
                       "range is increased to 9 meters with one charge.";
    this.tree        = "aegis";
    this.range       = [9];
    this.manaCost    = [0, 20, 20, 21, 21, 22, 22, 23, 24, 26, 27, 30, 32, 35, 36, 39];
    this.arc         = [0, 90, 90, 90, 90, 90, 90, 90, 90, 90, 130];
    this.tierBonuses = [
        "Burn time increased to 5 seconds",
        "Damage arc widened by 40 degrees to hit more targets",
        "Targets take 50% more fire damage while burning"
    ];

    var knockback = 35;
    var fireDamageTaken = 50;
    var fireDamageTakenDuration = 5;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Knockback",            knockback),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage Taken",    fireDamageTaken,         15),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage Duration", fireDamageTakenDuration, 15),
        new Torchlight.t2.skills.Attribute(this, "Mana",                 this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage",          calculateDamagePercent)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damage = calculateDamagePercent(skillLevel);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(damage),
            new Torchlight.t2.effects.Knockback(knockback)
        ];

        if (skillLevel >= 15) {
            effects.push(new Torchlight.t2.effects.DamageTakenPercent(fireDamageTaken, "Fire", fireDamageTakenDuration));
        }
        return effects;
    };


    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamagePercent(skillLevel) {
        return 95 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.aegis.FireBash.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
