
Torchlight.t2.skills.engineer.blitz.Supercharge = function Supercharge() {
    this.name = "supercharge";
    this.displayName = "Supercharge";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "blitz";
    this.passive = true;
    this.skillLevelRequiredTier = 1;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Chance to cast", calculateChanceToCast),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Bonus Charge", calculateBonusCharge)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var chanceToCast = calculateChanceToCast(skillLevel);
        var weaponDps = calculateWeaponDps(skillLevel);
        var bonusCharge = calculateBonusCharge(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.spells.ChargeRefill(bonusCharge),
            new Torchlight.t2.effects.CastSpell(chanceToCast, "Supercharge Weapon", "strike")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 20 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBonusCharge(skillLevel) {
        return 0 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToCast(skillLevel) {
        return 2 + (0.5 * skillLevel);
    }

};
Torchlight.t2.skills.engineer.blitz.Supercharge.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
