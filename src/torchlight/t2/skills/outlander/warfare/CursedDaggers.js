
Torchlight.t2.skills.outlander.warfare.CursedDaggers = function CursedDaggers() {
    this.name = "curseddaggers";
    this.displayName = "Cursed Daggers";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "warfare";
    this.skillLevelRequiredTier = 3;
    this.manaCost = [0, 18, 19, 19, 19, 20, 21, 22, 24, 26, 28, 31, 34, 37, 40, 43];
    this.tierBonuses = [
        "Number of daggers increased to 11. You gain .5% increased move speed per enemy hit for 4 seconds. ",
        "Number of daggers increased to 13. You gain .5% critical hit chance per enemy hit for 4 seconds. ",
        "Number of daggers increased to 15. You gain .5% increased damage per enemy hit for 4 seconds. "
    ];
    var poisonDamageDuration = 4;
    var allDamageDuration = 8;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Poison Damage Duration", poisonDamageDuration),
        new Torchlight.t2.skills.Attribute(this, "All Damage Reduction Duration", allDamageDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Poison Damage", calculatePoisonDamage),
        new Torchlight.t2.skills.Attribute(this, "All Damage Reduction", calculateAllDamageReduction),
        new Torchlight.t2.skills.Attribute(this, "Knockback", calculateKnockback),
        new Torchlight.t2.skills.Attribute(this, "Daggers", calculateDaggers)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var posionDamage = new Torchlight.lib.Range(calculatePoisonDamage(skillLevel, playerLevel));
        var allDamageReduction = calculateAllDamageReduction(skillLevel);
        var knockback = calculateKnockback(skillLevel);
        return [
            new Torchlight.t2.effects.Damage(posionDamage, "Poison", poisonDamageDuration),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.DamageReduction(allDamageReduction, "All", allDamageDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDaggers(skillLevel) {
        return Torchlight.lib.squeeze([9, 11, 13, 15], skillLevel / 5);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAllDamageReduction(skillLevel) {
        return 19 + (1 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateKnockback(skillLevel) {
        return 2.7 + (0.3 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculatePoisonDamage(skillLevel, playerLevel) {
        return 38 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.warfare.CursedDaggers.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
