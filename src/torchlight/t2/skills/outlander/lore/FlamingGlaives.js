
Torchlight.t2.skills.outlander.lore.FlamingGlaives = function FlamingGlaives() {
    this.skillLevelRequiredTier = 6;
    this.name        = "flamingglaives";
    this.displayName = "Flaming Glaives";
    this.description = "You hurl two flaming glaives that circle outward in concentric rings, slashing all foes in range and setting them alight.";
    this.tree        = "lore";
    this.manaCost    = [0, 25, 26, 26, 27, 27, 28, 29, 31, 32, 34,  36, 38, 49, 42, 44];
    this.tierBonuses = [
        "Glaive size increased, allowing it to strike more accurately",
        "Fire and Poison Damage Taken is increased by 30% for 6 seconds",
        "Fire and Poison Damage Taken is increased by 45% for 6 seconds"
    ];
    var effectDuration = 6;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Effect Duration", effectDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS", calculateDps),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage", calculateFireDamage),
        new Torchlight.t2.skills.Attribute(this, "Effect Damage", calculateEffectDamageTaken)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateDps(skillLevel);
        var fireDamage = new Torchlight.lib.Range(calculateFireDamage(skillLevel, playerLevel));

        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Fire"),
            new Torchlight.t2.effects.Damage(fireDamage, "Fire", effectDuration)
        ];

        if (effects >= 10) {
            var damageTaken = calculateEffectDamageTaken(skillLevel);
            effects.push(new Torchlight.t2.effects.DamageTakenPercent(damageTaken, "Fire", effectDuration));
            effects.push(new Torchlight.t2.effects.DamageTakenPercent(damageTaken, "Poison", effectDuration));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDps(skillLevel) {
        return 67 + (3 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateFireDamage(skillLevel, playerLevel) {
        return 13 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateEffectDamageTaken(skillLevel) {
        return Torchlight.lib.squeeze([0, 0, 30, 45], skillLevel / 5);
    }
};
Torchlight.t2.skills.outlander.lore.FlamingGlaives.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
