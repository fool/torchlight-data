
Torchlight.t2.effects.ThreeHatchets = function ThreeHatchets(duration) {
    this.toString = function toString() {
        return "3 hatchets are thrown at once";
    }
};
Torchlight.t2.effects.ThreeHatchets.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
