/**
 * @param {Torchlight.lib.Range} range
 * @param {string}              damageType
 * @param {Number}              [duration]
 * @constructor
 */
Torchlight.t2.effects.Damage = function Damage(range, damageType, duration) {
    this.toString = function toString() {
        return "+ " + range.toString() + " " + damageType + " Damage" + this.durationText(duration);
    }
};
Torchlight.t2.effects.Damage.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
