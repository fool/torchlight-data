
Torchlight.t2.skills.outlander.sigil.DeathRitual = function DeathRitual() {
    this.name = "deathritual";
    this.displayName = "Death Ritual";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "sigil";
    this.passive = true;
    this.skillLevelRequiredTier = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Lifetime", calculateShadowlingLifetime),
        new Torchlight.t2.skills.Attribute(this, "Damage", calculateShadowlingDamage)

    ];

    /**
     * @param   {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var lifetime = calculateShadowlingLifetime(skillLevel);
        var damage = calculateShadowlingDamage(skillLevel);
        return [
            new Torchlight.t2.effects.ShadowlingMastery(lifetime, damage)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateShadowlingLifetime(skillLevel) {
        return 10 + (1.5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateShadowlingDamage(skillLevel) {
        return 0 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.sigil.DeathRitual.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
