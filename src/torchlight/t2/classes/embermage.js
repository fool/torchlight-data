Torchlight.t2.classes.embermage = {};
Torchlight.t2.classes.embermage.skills = {
    frost: [
        new Torchlight.t2.skills.embermage.frost.IcyBlast(),
        new Torchlight.t2.skills.embermage.frost.Hailstorm(),
        new Torchlight.t2.skills.embermage.frost.FrostPhase(),
        new Torchlight.t2.skills.embermage.frost.ElementalBoon(),
        new Torchlight.t2.skills.embermage.frost.FrostWave(),
        new Torchlight.t2.skills.embermage.frost.AstralAlly(),
        new Torchlight.t2.skills.embermage.frost.IcePrison(),
        new Torchlight.t2.skills.embermage.frost.StaffMastery(),
        new Torchlight.t2.skills.embermage.frost.FrozenFate(),
        new Torchlight.t2.skills.embermage.frost.IceBrand()
    ],
    inferno: [
        new Torchlight.t2.skills.embermage.inferno.MagmaSpear(),
        new Torchlight.t2.skills.embermage.inferno.MagmaMace(),
        new Torchlight.t2.skills.embermage.inferno.Firebombs(),
        new Torchlight.t2.skills.embermage.inferno.BlazingPillar(),
        new Torchlight.t2.skills.embermage.inferno.InfernalCollapse(),
        new Torchlight.t2.skills.embermage.inferno.ImmolationAura(),
        new Torchlight.t2.skills.embermage.inferno.Firestorm(),
        new Torchlight.t2.skills.embermage.inferno.ChargeMastery(),
        new Torchlight.t2.skills.embermage.inferno.ElementalAttunement(),
        new Torchlight.t2.skills.embermage.inferno.FireBrand()
    ],
    storm: [
        new Torchlight.t2.skills.embermage.storm.PrimasticBolt(),
        new Torchlight.t2.skills.embermage.storm.ShockingBurst(),
        new Torchlight.t2.skills.embermage.storm.ThunderLocus(),
        new Torchlight.t2.skills.embermage.storm.ArcBeam(),
        new Torchlight.t2.skills.embermage.storm.DeathsBounty(),
        new Torchlight.t2.skills.embermage.storm.Shockbolts(),
        new Torchlight.t2.skills.embermage.storm.ShockingOrb(),
        new Torchlight.t2.skills.embermage.storm.PrimasticRift(),
        new Torchlight.t2.skills.embermage.storm.WandChaos(),
        new Torchlight.t2.skills.embermage.storm.LightningBrand()
    ]
};
Torchlight.t2.classes.embermage.getSkill = function getSkill(name) {
    return Torchlight.t2.classes.character.getSkillFromTree(name, Torchlight.t2.classes.embermage.skills);
};
