/**
 * Ex:
 *   Cast Speed reduced by 100% for 1.6 seconds
 *
 * @param {number} percent    This must be positive
 * @param {number} speedName  This can be positive or negative
 * @param {number} [duration] how long this lasts
 * @constructor
 */
Torchlight.t2.effects.buffs.SpeedIncrease = function Speed(percent, speedName, duration) {
    this.percent = Math.abs(percent);
    this.speedName = Torchlight.t2.effects.Effect.validateSpeedName(speedName, Torchlight.t2.effects.Effect.speeds.Attack);
    this.duration = duration;
    this.toString = function toString() {
        return this.speedName + " Speed +" + this.affectedByPercent(percent) + this.durationText(duration);
    };

    /**
     * @param   {Torchlight.t2.effects.Effect} effect
     * @returns {Torchlight.t2.effects.Effect|null}
     */
    this.combine = function combine(effect) {
        var result = null;
        if (effect instanceof Torchlight.t2.effects.buffs.SpeedIncrease) {
            if (this.speedName === effect.speedName) {
                var combinedPercent = Math.min(this.percent + effect.percent, 100);
                var combinedDuration = undefined;
                if (typeof this.duration === "number") {
                    combinedDuration = this.duration;
                }
                if (typeof effect.duration === "number") {
                    if (combinedDuration) {
                        combinedDuration += effect.duration;
                    } else {
                        combinedDuration = effect.duration;
                    }
                }
                result = new Torchlight.t2.effects.buffs.SpeedIncrease(combinedPercent, this.speedName, combinedDuration);
            }

        }
        return result;
    }
};
Torchlight.t2.effects.buffs.SpeedIncrease.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
