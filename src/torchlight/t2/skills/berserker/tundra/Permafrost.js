
Torchlight.t2.skills.berserker.tundra.Permafrost = function Permafrost() {
    this.name = "permafrost";
    this.displayName = "Permafrost";
    this.description = "The bone-cracking cold of the north expands outward from you, slowly sapping the life of all foes within 16 meters.";
    this.tree = "tundra";
    this.skillLevelRequiredTier = 5;
    this.tierBonuses = [
        "Adds a 25% chance to freeze foes for 6 seconds",
        "Adds a 25% chance to completely immobilise foes for 6 seconds",
        "Chance to immobilise foes is increased to 50% "
    ];
    this.manaCost = [0, 29, 29, 31, 31, 32, 32, 33, 35, 37, 40, 44, 46, 50, 53, 57];
    var freezeDuration = 6;
    var immobilizeDuration = 6;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Freeze Duration", freezeDuration, 5),
        new Torchlight.t2.skills.Attribute(this, "Immobilization Duration", immobilizeDuration, 10),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Ice Damage", calculcateIceDamage),
        new Torchlight.t2.skills.Attribute(this, "Ice Damage Duration", calculateIceDamageDuration),
        new Torchlight.t2.skills.Attribute(this, "Freeze Chance", calculateFreezeChance, 5),
        new Torchlight.t2.skills.Attribute(this, "ImmobilizeChance", calculateImmobilizeChance, 10)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var iceDamage         = calculcateIceDamage(skillLevel, playerLevel);
        var iceDamageDuration = calculateIceDamageDuration(skillLevel);
        var freezeChance      = calculateFreezeChance(skillLevel);
        var immobilizeChance  = calculateImmobilizeChance(skillLevel);


        var effects = [
            new Torchlight.t2.effects.Damage(iceDamage, "Ice", iceDamageDuration)
        ];

        if (skillLevel >= 5) {
            effects.push(new Torchlight.t2.effects.spells.Freeze(freezeChance, freezeDuration));
        }

        if (skillLevel >= 10) {
            effects.push(new Torchlight.t2.effects.Immobilize(immobilizeChance, immobilizeDuration));
        }

        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number} seconds
     */
    this.getCooldown = function getCooldown(skillLevel) {
        return 10.5 - (0.5 * skillLevel);
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculcateIceDamage(skillLevel, playerLevel) {
        var dmg = 5 + (10 * skillLevel);
        return new Torchlight.lib.Range(dmg, dmg);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateIceDamageDuration(skillLevel) {
        return 10.5 - (0.5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFreezeChance(skillLevel) {
        if (skillLevel < 5) {
            return 0;
        }
        return 25;
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateImmobilizeChance(skillLevel) {
        return Torchlight.lib.squeeze([0, 0, 25, 50], skillLevel / 5);
    }

};
Torchlight.t2.skills.berserker.tundra.Permafrost.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
