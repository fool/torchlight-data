/**
 * @param {number} duration
 * @param {number} damage
 * @constructor
 */
Torchlight.t2.effects.ShadowlingMastery = function ShadowlingMastery(duration, damage) {
    this.toString = function toString() {
        return "Shadowlings live for " + duration + " seconds and do " + damage + "% additional damage"
    }
};
Torchlight.t2.effects.ShadowlingMastery.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
