Torchlight.t2.effects.DamageIceArmorPercent = function DamageIceArmorPercent(percent, duration) {
    this.toString = function toString() {
        return percent + "% to Ice Armor for " + duration + " seconds";
    }
};
Torchlight.t2.effects.DamageIceArmorPercent.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
