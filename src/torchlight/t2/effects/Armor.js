
Torchlight.t2.effects.Armor = function Armor(armorValue, armorType, duration) {
    this.toString = function toString() {
        return armorValue + " to " + armorType + " armor" + this.durationText(duration);
    }
};
Torchlight.t2.effects.Armor.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
