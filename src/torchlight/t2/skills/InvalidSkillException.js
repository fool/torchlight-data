/**
 * Thrown when a skill is improperly defined
 */
Torchlight.t2.skills.InvalidSkillException = function InvalidSkillException(message) {
    this.message = message;
};
