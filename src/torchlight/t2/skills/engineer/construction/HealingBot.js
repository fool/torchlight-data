
Torchlight.t2.skills.engineer.construction.HealingBot = function HealingBot() {
    this.name = "healingbot";
    this.displayName = "Healing Bot";
    this.description = "You deploy a small drone that generates energy pulses, healing both you and your allies";
    this.tree = "construction";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 25, 26, 27, 29, 33, 36, 39, 44, 49, 56, 63, 69, 77, 86, 96];
    var cooldown = 10;
    this.cooldown = [cooldown];
    this.getRange = function getRange(skillLevel) {
        return 7.5 + (0.5 * skillLevel);
    };
    this.tierBonuses = [
        "The energy pulse also regenerates Mana",
        "The Engineer and allies also get an 8% Armor boost",
        "The Engineer and allies also get a 16% Armor boost"
    ];
    var effectDuration = 4;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Effect Duration", effectDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Healing Pulse Period", calculateHealingPulsePeriod),
        new Torchlight.t2.skills.Attribute(this, "Health Recovered", calculateHealthRecovered),
        new Torchlight.t2.skills.Attribute(this, "Mana Recovered", calculateManaRecovered, 5),
        new Torchlight.t2.skills.Attribute(this, "All Armor", calculateAllArmor, 10)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var healingPulsePeriod = calculateHealingPulsePeriod(skillLevel);
        var healingPulseRange = this.getRange(skillLevel);
        var healthRecovered = calculateHealthRecovered(skillLevel, playerLevel);
        var effects = [
            new Torchlight.t2.effects.HealingPulse(healingPulsePeriod, healingPulseRange),
            new Torchlight.t2.effects.HealthRecoveryPerSecond(healthRecovered, effectDuration)
        ];

        if (skillLevel >= 5) {
            var manaRecovered = calculateManaRecovered(skillLevel, playerLevel);
            effects.push(new Torchlight.t2.effects.ManaRecoveryPerSecond(manaRecovered, effectDuration));
        }

        if (skillLevel >= 10) {
            var allArmor = calculateAllArmor(skillLevel);
            effects.push(new Torchlight.t2.effects.buffs.ArmorPercent(allArmor, "All"));
        }
        return effects;
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateHealthRecovered(skillLevel, playerLevel) {
        return 28 + (2 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateManaRecovered(skillLevel, playerLevel) {
        return 28 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAllArmor(skillLevel) {
        return Torchlight.lib.squeeze([0, 0, 8, 16], skillLevel / 5);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateHealingPulsePeriod(skillLevel) {
        return Math.max(12.5 - (0.5 * skillLevel), 0);
    }

};
Torchlight.t2.skills.engineer.construction.HealingBot.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
