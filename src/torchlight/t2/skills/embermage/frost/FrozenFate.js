
Torchlight.t2.skills.embermage.frost.FrozenFate = function FrozenFate() {
    this.name = "frozenfate";
    this.displayName = "Frozen Fate";
    this.description = "When you kill an enemy, you have a chance to freeze up to 4 additional enemies within a 7 meter radius.";
    this.tree = "frost";
    this.passive = true;
    this.skillLevelRequiredTier = 1;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Immobilize Chance", calculateImmobilizeChance),
        new Torchlight.t2.skills.Attribute(this, "Immobilize Duration", calculateImmobilizeDuration)
    ];

    /**
     * http://i.imgur.com/eT4o7.png
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var immobilizeChance = calculateImmobilizeChance(skillLevel);
        var immobilizeDuration = calculateImmobilizeDuration(skillLevel);
        return [
            new Torchlight.t2.effects.Immobilize(immobilizeChance, immobilizeDuration)
        ];
    };

    function calculateImmobilizeChance(skillLevel) {
        return 16 + (4 * skillLevel);
    }

    function calculateImmobilizeDuration(skillLevel) {
        return 2.8 + (0.2 * skillLevel);
    }
};
Torchlight.t2.skills.embermage.frost.FrozenFate.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
