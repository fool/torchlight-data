
Torchlight.t2.skills.outlander.warfare.VortexHex = function VortexHex() {
    this.name = "vortexhex";
    this.displayName = "Vortex Hex";
    this.description = "You summon a fetish which draws nearby foes toward it and has a chance to stun them. A maximum of 3 foes may be drawn at one time";
    this.tree = "warfare";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 30, 31, 31, 32, 33, 34, 35, 37, 39, 43, 47, 51, 55, 60, 64];
    var cooldown = 3;
    var duration = 15;
    this.cooldown = [cooldown];
    this.duration = [duration];
    this.tierBonuses = [
        "Foes are damaged when drawn",
        "Up to five foes can be drawn at one time",
        "Drawn foes transfer Mana to caster "
    ];
    var stunDuration = 3;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration", duration),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", stunDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance", calculateStunChance),
        new Torchlight.t2.skills.Attribute(this, "Poison Damage", calculateDamage, 5),
        new Torchlight.t2.skills.Attribute(this, "Mana Recovery", calculateManaRecovery, 15)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var stunChance = calculateStunChance(skillLevel);
        var effects = [
            new Torchlight.t2.effects.Stun(stunChance, stunDuration)
        ];

        if (skillLevel >= 5) {
            var poisonDamage = new Torchlight.lib.Range(calculateDamage(skillLevel, playerLevel));
            effects.push(new Torchlight.t2.effects.Damage(poisonDamage, "Posion"));
        }

        if (skillLevel >= 15) {
            var manaRecovery = calculateManaRecovery(skillLevel, playerLevel);
            effects.push(new Torchlight.t2.effects.ManaRecoveryPerSecond(manaRecovery));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunChance(skillLevel) {
        return Torchlight.lib.squeeze([2, 4, 6, 8], skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateDamage(skillLevel, playerLevel) {
        return Torchlight.lib.squeeze([2, 4, 6, 8], skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateManaRecovery(skillLevel, playerLevel) {
        return Torchlight.lib.squeeze([2, 4, 6, 8], skillLevel);
    }

};
Torchlight.t2.skills.outlander.warfare.VortexHex.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
