
Torchlight.t2.effects.TransferDamage = function TransferDamage(percent, duration) {
    this.toString = function toString() {
        return "Transfers " + percent + "% of damage dealt for " + duration + " seconds";
    }
};
Torchlight.t2.effects.TransferDamage.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
