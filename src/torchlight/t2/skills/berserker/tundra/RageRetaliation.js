
Torchlight.t2.skills.berserker.tundra.RageRetaliation = function RageRetaliation() {
    this.name = "rageretaliation";
    this.displayName = "Rage Retaliation";
    this.description = "When you are struck by a nearby enemy, your inner rage physically manifests and retaliates against that foe. Individual targets can only be hit once per second at most.";
    this.tree = "tundra";
    this.passive = true;
    this.skillLevelRequiredTier = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateDamageDps),
        new Torchlight.t2.skills.Attribute(this, "Time Between Attacks", calculateMinimumTimeBetweenAttacks)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damageDps = calculateDamageDps(skillLevel);
        var minimumTimeBetweenAttacks = calculateMinimumTimeBetweenAttacks(skillLevel);

        return [
            new Torchlight.t2.effects.DamageEqualToWeaponDps(damageDps),
            new Torchlight.t2.effects.MinimumTimeBetweenAttacks(minimumTimeBetweenAttacks)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamageDps(skillLevel) {
        var dps = 120;
        if (skillLevel === 12) {
            dps = 130;
        } else if (skillLevel === 13) {
            dps = 140;
        } else if (skillLevel === 14) {
            dps = 150;
        } else if (skillLevel > 14) {
            dps = 160;
        }
        return dps;
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateMinimumTimeBetweenAttacks(skillLevel) {
        return Math.max(2.2 - (0.2 * skillLevel), 0);
    }
};
Torchlight.t2.skills.berserker.tundra.RageRetaliation.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
