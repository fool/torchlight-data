
Torchlight.t2.skills.embermage.frost.StaffMastery = function StaffMastery() {
    this.name = "staffmastery";
    this.displayName = "Staff Mastery";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "frost";
    this.skillLevelRequiredTier = 0;
    this.passive = true;
    var armorReductionDuration = 8;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Armor Reduction Duration", armorReductionDuration),
        new Torchlight.t2.skills.Attribute(this, "Armor Reduction", calculateArmorReduction)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var armorReduction = calculateArmorReduction(skillLevel);
        return [
            new Torchlight.t2.effects.EnemyArmorReductionPercent(armorReduction, "Electric", armorReductionDuration),
            new Torchlight.t2.effects.EnemyArmorReductionPercent(armorReduction, "Poison", armorReductionDuration),
            new Torchlight.t2.effects.EnemyArmorReductionPercent(armorReduction, "Ice", armorReductionDuration),
            new Torchlight.t2.effects.EnemyArmorReductionPercent(armorReduction, "Fire", armorReductionDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateArmorReduction(skillLevel) {
        return 4 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.embermage.frost.StaffMastery.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
