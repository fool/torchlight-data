
Torchlight.t2.effects.PullsTargetCloser = function PullsTargetCloser() {
    this.toString = function toString() {
        return "Pulls target closer";
    }
};
Torchlight.t2.effects.PullsTargetCloser.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
