
Torchlight.t2.effects.Execute = function Execute(percent, duration) {

    var d = "";
    if (typeof duration === "number" && duration > 0) {
        d = " for " + duration + " seconds";
    }
    this.toString = function toString() {
        return "+" + percent + "% Chance to Execute" + d;
    }
};
Torchlight.t2.effects.Execute.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
