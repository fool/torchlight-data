Torchlight.t2.effects.PercentOfWeaponDpsAdded = function PercentOfWeaponDpsAdded(percent) {
    this.toString = function toString() {
        return "Damage is increased to " + percent + "% of your weapon's DPS";
    }
};
Torchlight.t2.effects.PercentOfWeaponDpsAdded.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
