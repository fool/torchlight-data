
Torchlight.t2.effects.Blindness = function Blindness(percent, blindLevel, duration) {
    this.toString = function toString() {
        return percent + "% chance of " + blindLevel +"% Blindness for " + duration + " seconds";
    }
};
Torchlight.t2.effects.Blindness.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
