/**
 * @param {number} chance
 * @constructor
 */
Torchlight.t2.effects.ShadowlingAllyOnKill = function ShadowlingAllyOnKill(chance) {
    this.toString = function toString() {
        return chance + "% chance on kill to Spawn Shadowling Ally";
    }
};
Torchlight.t2.effects.ShadowlingAllyOnKill.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
