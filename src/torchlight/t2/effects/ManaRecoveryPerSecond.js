/**
 * @constructor
 */
Torchlight.t2.effects.ManaRecoveryPerSecond = function ManaRecoveryPerSecond(mana) {

    this.toString = function toString() {
        return mana + " Mana recovery per second";
    }
};
Torchlight.t2.effects.ManaRecoveryPerSecond.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
