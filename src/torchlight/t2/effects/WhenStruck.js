
Torchlight.t2.effects.WhenStruck = function WhenStruck(percent, effect) {
    this.toString = function toString() {
        return percent + "% chance to " + effect + " when struck";
    }
};
Torchlight.t2.effects.WhenStruck.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
