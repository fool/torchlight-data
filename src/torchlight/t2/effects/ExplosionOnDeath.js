/**
 * @param {number} [chance]
 * @constructor
 */
Torchlight.t2.effects.ExplosionOnDeath = function ExplosionOnDeath(chance) {
    var c = "";
    if (typeof chance === "number") {
        c = chance + "% chance of ";
    }
    this.toString = function toString() {
        return c + "Explosion on enemy death";
    }
};
Torchlight.t2.effects.ExplosionOnDeath.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
