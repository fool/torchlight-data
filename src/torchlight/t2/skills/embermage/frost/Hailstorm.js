
Torchlight.t2.skills.embermage.frost.Hailstorm = function Hailstorm() {
    this.name = "hailstorm";
    this.displayName = "Hailstorm";
    this.description = "You call down shards of ice from the sky, pummeling foes within a 15 meter radius. Hailstorm gains no Charge.";
    this.tree = "frost";
    this.skillLevelRequiredTier = 1;
    this.manaCost = [0, 18, 18, 19, 20, 21, 22, 24, 27, 31, 34, 38, 41, 46, 51, 57];
    this.getCooldown = function getCooldown(skillLevel) {
        return Math.max(2.6 - (0.1 * skillLevel), 0);
    };
    var stunDuration = 3;
    var freezeDuration = 6;
    var armorReductionDuration = 6;
    var range = 15;
    this.range = [range];
    this.tierBonuses = [
        "Targets become 20% more susceptible to Ice and Electric damage for 6 seconds",
        "Targets become 40% more susceptible to Ice and Electric damage for 6 seconds",
        "Targets become 60% more susceptible to Ice and Electric damage for 6 seconds"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Range", range),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", stunDuration),
        new Torchlight.t2.skills.Attribute(this, "Freeze Duration", freezeDuration),
        new Torchlight.t2.skills.Attribute(this, "Armor Reduction Duration", armorReductionDuration),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance", calculateChanceToStun),
        new Torchlight.t2.skills.Attribute(this, "Freeze Chance", calculateChanceToFreeze),
        new Torchlight.t2.skills.Attribute(this, "Damage Increase", calculateDamageIncrease)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var stunChance     = calculateChanceToStun(skillLevel);
        var freezeChance   = calculateChanceToFreeze(skillLevel);
        var iceDamage      = calculateIceDamage(skillLevel, playerLevel);
        var damageIncrease = calculateDamageIncrease(skillLevel);
        var effects = [
            new Torchlight.t2.effects.Stun(stunChance, stunDuration),
            new Torchlight.t2.effects.spells.Freeze(freezeChance, freezeDuration),
            new Torchlight.t2.effects.Damage(iceDamage, "Ice")
        ];

        if (skillLevel >= 5) {
            effects.push(new Torchlight.t2.effects.DamageTakenPercent(damageIncrease, "Electric", freezeDuration));
            effects.push(new Torchlight.t2.effects.DamageTakenPercent(damageIncrease, "Ice", freezeDuration));
        }
        return [];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateIceDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(48 + (2 * skillLevel))
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToStun(skillLevel) {
        return 48 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToFreeze(skillLevel) {
        return 5 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamageIncrease(skillLevel) {
        return Torchlight.lib.squeeze([0, 20, 40, 60], skillLevel);
    }
};
Torchlight.t2.skills.embermage.frost.Hailstorm.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
