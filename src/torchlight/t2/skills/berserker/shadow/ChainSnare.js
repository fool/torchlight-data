
Torchlight.t2.skills.berserker.shadow.ChainSnare = function ChainSnare() {
    this.name = "chainsnare";
    this.displayName = "Chain Snare";
    this.description = "Spearing all enemies within 6 meters, you draw them swiftly into melee range.";
    this.tree = "shadow";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 14, 14, 15, 15, 15, 16, 16, 17, 18, 20, 22, 24, 26, 28, 30];
    this.range    = [0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 9];
    var cooldown = 0.9;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "Stun duration increased to 2 seconds",
        "Range increased to 9 meters",
        "Shatters shields"
    ];
    var stunChance = 75;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown",            cooldown),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance",         stunChance),
        new Torchlight.t2.skills.Attribute(this, "Pulls Target Closer", ''),
        new Torchlight.t2.skills.Attribute(this, "Breaks Shields",      '',                     15),
        new Torchlight.t2.skills.Attribute(this, "Mana",                this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range",               this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %",        calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration",       calculateStunDuration)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var stunDuration = calculateStunDuration(skillLevel);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.PullsTargetCloser(),
            new Torchlight.t2.effects.Stun(stunChance, stunDuration)
        ];

        if (skillLevel >= 15) {
            effects.push(new Torchlight.t2.effects.spells.BreakShields(100));
        }

        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 16 + (4 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunDuration(skillLevel) {
        var duration = 1;
        if (skillLevel >= 5) {
            duration = 2;
        }
        return duration;
    }
};
Torchlight.t2.skills.berserker.shadow.ChainSnare.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
