
Torchlight.t2.skills.berserker.hunter.Rupture = function Rupture() {
    this.name = "rupture";
    this.displayName = "Rupture";
    this.description = "You knock a single enemy back with a devastating strike. After a brief delay, the target explodes, damaging all foes within 4 meters.";
    this.tree = "hunter";
    this.skillLevelRequiredTier = 2;
    this.manaCost    = [0, 31, 31, 31, 31, 32, 33, 35, 36, 38, 41, 44, 48, 51, 55, 58];
    this.tierBonuses = [
        "Explosion radius increased to 5 meters",
        "Explosion radius increased to 6 meters",
        "Explosion radius increased to 7 meters"
    ];
    var knockback = 35;
    var stunDuration = 3;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Stun Duration",    stunDuration),
        new Torchlight.t2.skills.Attribute(this, "Knockback",        knockback),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance",      calculateStunChance),
        new Torchlight.t2.skills.Attribute(this, "Explosion Radius", calculateExplosionRadius),
        new Torchlight.t2.skills.Attribute(this, "Physical Damage",  calculatePhysicalDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var stunChance = calculateStunChance(skillLevel);
        var explosionRadius = calculateExplosionRadius(skillLevel);
        var physicalDamage = calculatePhysicalDamage(skillLevel, playerLevel);

        return [
            new Torchlight.t2.effects.Stun(stunChance, stunDuration),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.ExplosionRadius(explosionRadius),
            new Torchlight.t2.effects.Damage(new Torchlight.lib.Range(physicalDamage), "Physical")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunChance(skillLevel) {
        return 47 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateExplosionRadius(skillLevel) {
        return Torchlight.lib.squeeze([4, 5, 6, 7], skillLevel / 5);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculatePhysicalDamage(skillLevel, playerLevel) {
        return skillLevel * 5 + playerLevel * 2;
    }
};
Torchlight.t2.skills.berserker.hunter.Rupture.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
