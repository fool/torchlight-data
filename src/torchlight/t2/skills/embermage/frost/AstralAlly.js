
Torchlight.t2.skills.embermage.frost.AstralAlly = function AstralAlly() {
    this.name = "astralally";
    this.displayName = "Astral Ally";
    this.description = "You summon an astral clone of a fellow Embermage, who joins you in battle with powerful spells.";
    this.tree = "frost";
    this.skillLevelRequiredTier = 6;
    this.manaCost = [0, 57, 100];
    this.duration = [0, 20, 20, 20, 20, 30, 30, 30, 30, 30, 40, 40, 40, 40, 40, 50];
    this.tierBonuses = [
        "The Astral Ally lasts for 30 seconds",
        "The Astral Ally lasts for 40 seconds",
        "The Astral Ally lasts for 50 seconds"
    ];
    this.attributes= [
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Duration", this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "All Damage %", calculateAllDamagePercent),
        new Torchlight.t2.skills.Attribute(this, "Physical Armor %", calculateArmorPercent),
        new Torchlight.t2.skills.Attribute(this, "Minions Damage", calculateMinionDamage)

    ];

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    this.getCooldown = function getCooldown(skillLevel) {
        return 62 - (2 * skillLevel);
    };

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var allDamagePercent = calculateAllDamagePercent(skillLevel);
        var armorPercent = calculateArmorPercent(skillLevel);
        var minionDamage = calculateMinionDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.DamagePercent(new Torchlight.lib.Range(allDamagePercent), "All"),
            new Torchlight.t2.effects.buffs.ArmorPercent(armorPercent, "Physical"),
            new Torchlight.t2.effects.MinionsDealDamage(minionDamage, "Physical")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAllDamagePercent(skillLevel) {
        return -4 + (4 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateArmorPercent(skillLevel) {
        return -4 + (4 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateMinionDamage(skillLevel, playerLevel) {
        var skill  = (skillLevel / 3);
        var player = (playerLevel / 20);
        var lower  = 300 + Math.pow(skill + 0.5, player); // 5k to 7.7k on lvl 15 / 100... ??
        var upper  = 350 + Math.pow(skill + 1,   player);
        return new Torchlight.lib.Range(lower, upper);
    }


};
Torchlight.t2.skills.embermage.frost.AstralAlly.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
