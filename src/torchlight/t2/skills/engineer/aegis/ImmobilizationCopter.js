
Torchlight.t2.skills.engineer.aegis.ImmobilizationCopter = function ImmobilizationCopter() {
    this.name = "immobilizationcopter";
    this.displayName = "Immobilization Copter";
    this.description = "You deploy an immobilization drone which slows enemies within range of its beam. A maximum of 3 targets may be affected at once.";
    this.tree = "aegis";
    this.skillLevelRequiredTier = 6;
    this.manaCost = [0, 15, 15, 15, 16, 16, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26];
    var cooldown = 10;
    var duration = 30;
    this.cooldown = [cooldown];
    this.duration = [duration];
    this.tierBonuses = [
        "A maximum of 5 targets may be affected at once",
        "Electrical damage inflicted while slowed",
        "Targets are intermittently interrupted"
    ];
    var effectDuration = 0.2;
    var interruptChance = 25;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown",         cooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration",         duration),
        new Torchlight.t2.skills.Attribute(this, "Intterupt Chance", interruptChance,                 15),
        new Torchlight.t2.skills.Attribute(this, "Mana",             this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Movement Speed",   calculateMovementSpeedReduction),
        new Torchlight.t2.skills.Attribute(this, "Attack Speed",     calculateAttackSpeedReduction),
        new Torchlight.t2.skills.Attribute(this, "Cast Speed",       calculateAttackSpeedReduction),
        new Torchlight.t2.skills.Attribute(this, "Electric Damage",  calculateElectricDamage,         10)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var movementSpeed = calculateMovementSpeedReduction(skillLevel);
        var attackSpeed = calculateAttackSpeedReduction(skillLevel);
        var castSpeed = attackSpeed;

        var effects = [
            new Torchlight.t2.effects.debuffs.SpeedDecrease(movementSpeed, Torchlight.t2.effects.Effect.speeds.Movement, effectDuration),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(castSpeed, Torchlight.t2.effects.Effect.speeds.Cast, effectDuration),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(attackSpeed, Torchlight.t2.effects.Effect.speeds.Attack, effectDuration)
        ];

        if (skillLevel >= 10) {
            var electricDamage = new Torchlight.lib.Range(calculateElectricDamage(skillLevel, playerLevel));
            effects.push(new Torchlight.t2.effects.Damage(electricDamage, "Electric", effectDuration));
        }

        if (skillLevel >= 15) {
            effects.push(new Torchlight.t2.effects.buffs.Interrupt(interruptChance));
        }

        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateMovementSpeedReduction(skillLevel) {
        return -1 * (18 + (2 * skillLevel));
    }
    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAttackSpeedReduction(skillLevel) {
        return -1 * (28 + (2 * skillLevel));
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateElectricDamage(skillLevel, playerLevel) {
        return -5 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.aegis.ImmobilizationCopter.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
