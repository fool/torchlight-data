
Torchlight.t2.skills.outlander.sigil.MasterOfTheElements = function MasterOfTheElements() {
    this.name = "masteroftheelements";
    this.displayName = "Master Of The Elements";
    this.description = "Your expertise with the elements allows you to deal more damage with them. Poison receives twice the benefit of the other elements";
    this.tree = "sigil";
    this.passive = true;
    this.skillLevelRequiredTier = 0;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Ice/Fire/Electric Damage", calculateDamage),
        new Torchlight.t2.skills.Attribute(this, "Poison Damage", calculatePoisonDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var damage = calculateDamage(skillLevel);
        var poisonDamage = calculatePoisonDamage(skillLevel);
        return [
            new Torchlight.t2.effects.DamagePercent(damage,       "Ice"),
            new Torchlight.t2.effects.DamagePercent(damage,       "Fire"),
            new Torchlight.t2.effects.DamagePercent(damage,       "Electric"),
            new Torchlight.t2.effects.DamagePercent(poisonDamage, "Poison")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamage(skillLevel) {
        return 10 + (1.5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePoisonDamage(skillLevel) {
        return 10 + (1.5 * skillLevel);
    }

};
Torchlight.t2.skills.outlander.sigil.MasterOfTheElements.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
