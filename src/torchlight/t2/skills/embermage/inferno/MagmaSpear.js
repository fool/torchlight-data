
Torchlight.t2.skills.embermage.inferno.MagmaSpear = function MagmaSpear() {
    this.name = "magmaspear";
    this.displayName = "Magma Spear";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "inferno";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0,  9, 10, 11, 12, 13, 14, 16, 18, 20, 22, 25, 28, 31, 34, 38];
    this.range    = [0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 24, 24, 24, 24, 24, 24];
    this.tierBonuses = [
        "Range extended to 24 meters",
        "Burn time increased to 6 seconds",
        "Magma Spears ricochet"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Burn Chance", calculateBurnChance),
        new Torchlight.t2.skills.Attribute(this, "Burn Time", calculateBurnDuration)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var burnChance = calculateBurnChance(skillLevel);
        var burnDuration = calculateBurnDuration(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Fire"),
            new Torchlight.t2.effects.spells.Burn(burnChance, burnDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 32 + (1 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBurnChance(skillLevel) {
        return 30 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBurnDuration(skillLevel) {
        return Torchlight.lib.squeeze([3, 3, 6], skillLevel / 5);
    }
};
Torchlight.t2.skills.embermage.inferno.MagmaSpear.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
