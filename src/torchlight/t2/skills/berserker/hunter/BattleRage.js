
Torchlight.t2.skills.berserker.hunter.BattleRage = function BattleRage() {
    this.name = "battlerage";
    this.displayName = "Battle Rage";
    this.description = "You enter an enraged state during which your damage is amplified based on the number of foes within 4 meters. The rage lasts for 60 seconds.";
    this.tree = "hunter";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 21, 21, 21, 22, 22, 23, 24, 25, 27, 29, 32, 35, 37, 41, 44];
    this.range    = [0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6];
    var cd = 50;
    var durationSeconds = 60;
    this.duration = [durationSeconds];
    this.cooldown = [cd];
    this.tierBonuses = [
        "Damage you take is also reduced by foe proximity",
        "Aurus range increased to 5m",
        "Aurus range increased to 6m"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Duration",           durationSeconds),
        new Torchlight.t2.skills.Attribute(this, "Cooldown",           cd),
        new Torchlight.t2.skills.Attribute(this, "Mana",               this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range",              this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Damage Reduction %", calculatePhysicalDamageReductionForMonster),
        new Torchlight.t2.skills.Attribute(this, "% All Damage Added", calculatePercentAllDamageAddForMonster,      5)

    ];

    /**
     * @param {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var damageAdd = calculatePercentAllDamageAddForMonster(skillLevel);
        var range = this.getRange(skillLevel);
        var effects = [
            new Torchlight.t2.effects.DamageForMonster(damageAdd, "All", range)
        ];

        if (skillLevel >= 5) {
            var damageReduction = calculatePhysicalDamageReductionForMonster(skillLevel);
            effects.push(new Torchlight.t2.effects.DamageTakenForMonster(damageReduction, "Physical", range));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePercentAllDamageAddForMonster(skillLevel) {
        return 2.5 + (.5* skillLevel);
    }

    /**
     *
     * @param   {number}  skillLevel
     * @returns {number}  50
     */
    function calculatePhysicalDamageReductionForMonster(skillLevel) {
        return (skillLevel * 2 * .5) - .5;
    }
};
Torchlight.t2.skills.berserker.hunter.BattleRage.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
