
Torchlight.t2.skills.outlander.sigil.RepulsionHex = function RepulsionHex() {
    this.name = "repulsionhex";
    this.displayName = "Repulsion Hex";
    this.description = "You summon a floating hex avatar, which repulses enemies who draw too near";
    this.tree = "sigil";
    this.skillLevelRequiredTier = 3;
    this.manaCost = [0, 27, 27, 28, 28, 29, 31, 33, 35, 38, 41, 45, 49, 54, 59, 64 ];
    var cooldown = 35;
    this.cooldown = [cooldown];
    this.getDuration = function getDuration(skillLevel) {
        return 36 + (4 * skillLevel);
    };
    this.tierBonuses = [
        "The hex has a 10% chance to cause enemies to flee for 5 seconds",
        "The hex has a 20% chance to cause enemies to flee for 5 seconds",
        "The hex has a 30% chance to cause enemies to flee for 5 seconds"
    ];
    var fleeDuration = 5;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Flee Duration", fleeDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Duration", this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "Hex Fire Rate", calculateHexFireRate),
        new Torchlight.t2.skills.Attribute(this, "Flee Chance", calculateFleeChance),
        new Torchlight.t2.skills.Attribute(this, "Interrupt", calculateInterruptChance),
        new Torchlight.t2.skills.Attribute(this, "Knockback", calculateKnockback)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var hexFireRate = calculateHexFireRate(skillLevel);
        var interrupt = calculateInterruptChance(skillLevel);
        var knockback = calculateKnockback(skillLevel);
        var effects = [
            new Torchlight.t2.effects.HexFireRate(hexFireRate),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.buffs.Interrupt(interrupt)
        ];

        if (effects >= 5) {
            var fleeChance = calculateFleeChance(skillLevel);
            effects.push(new Torchlight.t2.effects.spells.Flee(fleeChance, fleeDuration));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateHexFireRate(skillLevel) {
        return Math.max(1.05 - (0.05 * skillLevel), 0);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFleeChance(skillLevel) {
        return Torchlight.lib.squeeze([0, 10, 20, 30], skillLevel / 5);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateKnockback(skillLevel) {
        return 17.5 + (2.5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateInterruptChance(skillLevel) {
        return 5 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.sigil.RepulsionHex.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
