Torchlight.t2.classes.outlander = {};
Torchlight.t2.classes.outlander.skills = {
    lore: [
        new Torchlight.t2.skills.outlander.lore.GlaiveThrow(),
        new Torchlight.t2.skills.outlander.lore.TanglingShot(),
        new Torchlight.t2.skills.outlander.lore.GlaiveSweep(),
        new Torchlight.t2.skills.outlander.lore.Sandstorm(),
        new Torchlight.t2.skills.outlander.lore.BrambleWall(),
        new Torchlight.t2.skills.outlander.lore.BurningLeap(),
        new Torchlight.t2.skills.outlander.lore.FlamingGlaives(),
        new Torchlight.t2.skills.outlander.lore.DodgeMastery(),
        new Torchlight.t2.skills.outlander.lore.PoisonBurst(),
        new Torchlight.t2.skills.outlander.lore.ShareTheWealth()

    ],
    sigil: [
        new Torchlight.t2.skills.outlander.sigil.BladePact(),
        new Torchlight.t2.skills.outlander.sigil.ShadowShot(),
        new Torchlight.t2.skills.outlander.sigil.BaneBreath(),
        new Torchlight.t2.skills.outlander.sigil.StonePact(),
        new Torchlight.t2.skills.outlander.sigil.Shadowmantle(),
        new Torchlight.t2.skills.outlander.sigil.RepulsionHex(),
        new Torchlight.t2.skills.outlander.sigil.ShadowlingBrute(),
        new Torchlight.t2.skills.outlander.sigil.MasterOfTheElements(),
        new Torchlight.t2.skills.outlander.sigil.ShadowlingAmmo(),
        new Torchlight.t2.skills.outlander.sigil.DeathRitual()
    ],
    warfare: [
        new Torchlight.t2.skills.outlander.warfare.RapidFire(),
        new Torchlight.t2.skills.outlander.warfare.RuneVault(),
        new Torchlight.t2.skills.outlander.warfare.ChaosBurst(),
        new Torchlight.t2.skills.outlander.warfare.CursedDaggers(),
        new Torchlight.t2.skills.outlander.warfare.VortexHex(),
        new Torchlight.t2.skills.outlander.warfare.ShatteringGlaive(),
        new Torchlight.t2.skills.outlander.warfare.VenomousHail(),
        new Torchlight.t2.skills.outlander.warfare.LongRangeMastery(),
        new Torchlight.t2.skills.outlander.warfare.ShotgonneMastery(),
        new Torchlight.t2.skills.outlander.warfare.Akimbo()

    ]
};
Torchlight.t2.classes.outlander.getSkill = function getSkill(name) {
    return Torchlight.t2.classes.character.getSkillFromTree(name, Torchlight.t2.classes.outlander.skills);
};
