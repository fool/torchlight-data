/**
 * @param {number} chance
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.spells.Burn = function Burn(chance, duration) {
    this.chance = chance;
    this.duration = duration;

    this.toString = function toString() {
        return chance + "% chance to Burn" + this.durationText(duration);
    };

    this.combine = function combine(effect) {
        var result = null;
        if (effect instanceof Torchlight.t2.effects.spells.Burn) {
            var combinedChance = Math.min(this.chance + effect.chance, 100);
            var combinedDuration = this.duration + effect.duration;
            result = new Torchlight.t2.effects.spells.Burn(combinedChance, combinedDuration);
        }
        return result;
    };
};
Torchlight.t2.effects.spells.Burn.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
