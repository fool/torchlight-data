/**
 * Converts floats to ints
 *
 * @param   {number} value
 * @returns {number} Always gauranteed to be a whole number
 */
Torchlight.lib.truncate = function truncate(value)
{
    if (value < 0) {
        return Math.ceil(value);
    }
    return Math.floor(value);
};
