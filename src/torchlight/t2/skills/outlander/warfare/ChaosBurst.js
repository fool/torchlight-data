
Torchlight.t2.skills.outlander.warfare.ChaosBurst = function ChaosBurst() {
    this.name = "chaosburst";
    this.displayName = "Chaos Burst";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "warfare";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 12, 12, 12, 12, 13, 14, 15, 16, 18, 20, 22, 24, 27, 29, 32];
    this.tierBonuses = [
        "Missile speed increased 50%",
        "Missiles have a 30% chance to poison their target",
        "2 additional missiles per burst"
    ];
    var poisonChance = 30;
    var poisonDuration = 5;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Poison Chance", poisonChance, 10),
        new Torchlight.t2.skills.Attribute(this, "Poison Duration", poisonDuration, 10),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS", calculateWeaponDps)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var dps = calculateWeaponDps(skillLevel);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(dps)
        ];

        if (skillLevel >= 10) {
            effects.push(new Torchlight.t2.effects.Poison(poisonChance, poisonDuration));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 38 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.warfare.ChaosBurst.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
