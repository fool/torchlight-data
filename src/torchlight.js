var fs   = require("fs");
var path = require("path");
var os   = require("os");


var Catter = function Catter() {
    /**
     * SYNC
     * @param file
     * @param matcher
     * @returns {Array}
     */
    function recursivelyFindFilesMatching(file, matcher) {
        var pathsToCheck = [file];
        var filesFound = [];

        while (pathsToCheck.length > 0) {
            var curr = pathsToCheck.shift();
            //console.log("Searching " + curr);
            var relativeFilePaths = fs.readdirSync(curr);
            for (var key in relativeFilePaths)  {
                if (relativeFilePaths.hasOwnProperty(key)) {
                    var filePath = relativeFilePaths[key];
                    var fullPath = curr + path.sep + filePath;
                    if (filePath === "torchlight.js") { /* dont include this file */
                        continue;
                    }

                    //console.log("fs.statSync(" + fullPath + ")");
                    var stat = fs.statSync(fullPath);
                    if (stat.isDirectory()) {
                        pathsToCheck.push(fullPath);
                    } else if (matcher(filePath)) {
                        filesFound.push(fullPath);
                    }
                }
            }
        }
        return filesFound;
    }

    function jsFileMatcher(filename) {
        return filename.match(/js$/);
    }

    this.build = function build(destination) {
        var baseDir = __dirname + path.sep + "torchlight";
        var dirs = [
            [baseDir, "t2", "effects"],
            [baseDir, "t2", "skills"],
            [baseDir, "t2", "classes"]
        ];

        var filesFound = [
            [baseDir, "namespaces.js"].join(path.sep),
            [baseDir, "lib", "Range.js"].join(path.sep),
            [baseDir, "lib", "squeeze.js"].join(path.sep),
            [baseDir, "lib", "truncate.js"].join(path.sep),
            [baseDir, "lib", "toFixed.js"].join(path.sep),
            [baseDir, "t2", "effects", "Effect.js"].join(path.sep)
        ];
        dirs.map(function (dir) {
            var files = recursivelyFindFilesMatching(dir.join(path.sep), jsFileMatcher);
            for (var key in files) {
                var file = files[key];
                if (filesFound.indexOf(file) === -1) {
                    filesFound.push(file);
                }
            }
        });

        if (fs.existsSync(destination)) {
            fs.unlinkSync(destination);
        }

        var parentDirectory = path.dirname(destination);
        if (!fs.existsSync(parentDirectory)) {
            fs.mkdirSync(parentDirectory);
        }

        for (var key in filesFound) {
            var file = filesFound[key];
            var data = fs.readFileSync(file);
            fs.writeFileSync(destination, data + os.EOL, {
                encoding: 'utf8', flag: 'a'
            });
            console.log("appended: " + file);
        }
    };
};


module.exports.Catter = Catter;
