
Torchlight.t2.skills.berserker.hunter.Rampage = function Rampage() {
    this.name        = "rampage";
    this.displayName = "Rampage";
    this.description = "Each enemy you kill with a melee weapon has a chance of spurring you into an adrenaline-fueled rampage.";
    this.tree = "hunter";
    this.passive = true;
    this.skillLevelRequiredTier = 2;
    var attackSpeed     = 25;
    var castSpeed       = 25;
    var movementSpeed   = 12;
    var duration = 5;
    this.duration       = [duration];
    this.attributes     = [
        new Torchlight.t2.skills.Attribute(this, "Attack Speed",   attackSpeed),
        new Torchlight.t2.skills.Attribute(this, "Cast Speed",     castSpeed),
        new Torchlight.t2.skills.Attribute(this, "Movement Speed", movementSpeed),
        new Torchlight.t2.skills.Attribute(this, "Duration",       duration),
        new Torchlight.t2.skills.Attribute(this, "Rampage Chance", calculateRampageChance)
    ];

    /**
     * @param {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var rampageChance = calculateRampageChance(skillLevel);

        return [
            new Torchlight.t2.effects.RampageChance(rampageChance),
            new Torchlight.t2.effects.buffs.SpeedIncrease(attackSpeed,   Torchlight.t2.effects.Effect.speeds.Attack,   duration),
            new Torchlight.t2.effects.buffs.SpeedIncrease(castSpeed,     Torchlight.t2.effects.Effect.speeds.Cast,     duration),
            new Torchlight.t2.effects.buffs.SpeedIncrease(movementSpeed, Torchlight.t2.effects.Effect.speeds.Movement, duration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateRampageChance(skillLevel) {
        return 3 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.hunter.Rampage.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
