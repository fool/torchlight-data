
Torchlight.t2.skills.outlander.sigil.BladePact = function BladePact() {
    this.name = "bladepact";
    this.displayName = "Blade Pact";
    this.description = "You invoke an ancient curse that slows your foes and undermines their defenses. The Blade Pact lasts for seven seconds, and any enemy passing through the pact's area suffers the effects for the listed duration.";
    this.tree = "sigil";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 10, 11, 12, 13, 14, 16, 18, 20, 22, 25, 28, 31, 34, 38, 42];
    var cooldown = 1;
    this.cooldown = [cooldown];
    this.getDuration = function getDuration(skillLevel) {
        return Torchlight.lib.squeeze([7, 9, 11, 13], skillLevel / 5);
    };
    this.tierBonuses = [
        "The pact lasts for 9 seconds",
        "The pact lasts for 11 seconds",
        "The pact lasts for 13 seconds"
    ];
    var effectDuration = 3;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Effect Duration", effectDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Duration", this.getDuration)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var attackSpeedReduction   = calculateAttackSpeedReduction(skillLevel);
        var movementSpeedReduction = calculateMovementSpeedReduction(skillLevel);
        var physicalArmorReduction = calculatePhysicalArmorReduction(skillLevel);
        return [
            new Torchlight.t2.effects.debuffs.SpeedDecrease(attackSpeedReduction, Torchlight.t2.effects.Effect.speeds.Attack, effectDuration),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(movementSpeedReduction, Torchlight.t2.effects.Effect.speeds.Movement, effectDuration),
            new Torchlight.t2.effects.buffs.ArmorPercent(physicalArmorReduction, effectDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAttackSpeedReduction(skillLevel) {
        return -1 * (13 + (2 * skillLevel));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateMovementSpeedReduction(skillLevel) {
        return -1 * (13.5 + (1.5 * skillLevel));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePhysicalArmorReduction(skillLevel) {
        return -1 * (6 + (2 * skillLevel));
    }
};
Torchlight.t2.skills.outlander.sigil.BladePact.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
