
Torchlight.t2.skills.embermage.storm.PrimasticRift = function PrimasticRift() {
    this.name = "primasticrift";
    this.displayName = "Primastic Rift";
    this.description = "Your chaotic energy teleports enemies away when they strike you, with a chance of random elemental effects.";
    this.tree = "storm";
    this.passive = true;
    this.skillLevelRequiredTier = 0;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Chance to cast", calculateChanceToCast)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var chanceToCast = calculateChanceToCast(skillLevel);
        return [
            new Torchlight.t2.effects.CastSpell(chanceToCast, "Rift Warp", "when struck")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToCast(skillLevel) {
        return 10 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.embermage.storm.PrimasticRift.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
