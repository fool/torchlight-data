
Torchlight.t2.skills.embermage.frost.IcePrison = function IcePrison() {
    this.name = "iceprison";
    this.displayName = "Ice Prison";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "frost";
    this.skillLevelRequiredTier = 5;
    this.manaCost = [0, 32, 44];
    this.cooldown = [0, 9];
    this.duration = [0, 5];
    this.tierBonuses = [
        "Ice pillars inflict damage when shattering",
        "Foes within the prison are damaged over time",
        "Cooldown reduced to 2 seconds"
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        return [];
    };
};
Torchlight.t2.skills.embermage.frost.IcePrison.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
