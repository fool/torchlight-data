
Torchlight.t2.skills.engineer.blitz.Onslaught = function Onslaught() {
    this.name = "onslaught";
    this.displayName = "Onslaught";
    this.description = "The Engineer leaps forward, slamming their melee weapon into the ground. Foes at the point of impact are damaged and slowed significantly.";
    this.tree = "blitz";
    this.skillLevelRequiredTier = 3;
    this.manaCost = [0, 27, 27, 28, 28, 23, 24, 26, 28, 30, 24, 27, 29, 32, 35, 25];
    var cooldown = 1;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "Mana cost reduced by 20%",
        "Mana cost reduced by 40%",
        "Mana cost reduced by 60%"
    ];
    var speedEffect = -50;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Attack/Movement/Cast Speed %", speedEffect),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Speed Effect Duration", calculateEffectDuration)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var effectDuration = calculateEffectDuration(skillLevel);
        return [
            new Torchlight.t2.effects.DamagePercent(weaponDps, "Electric"),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(speedEffect, Torchlight.t2.effects.Effect.speeds.Movement, effectDuration),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(speedEffect, Torchlight.t2.effects.Effect.speeds.Attack,   effectDuration),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(speedEffect, Torchlight.t2.effects.Effect.speeds.Cast,     effectDuration)
        ];
    };


    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateEffectDuration(skillLevel) {
        return 1 + (1 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 57 + (3 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.blitz.Onslaught.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
