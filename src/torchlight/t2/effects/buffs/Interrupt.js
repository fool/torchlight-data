
Torchlight.t2.effects.buffs.Interrupt = function Interrupt(percent) {
    this.percent = percent;
    this.toString = function toString() {
        return "+" + this.percent + "% Interrupt chance";
    };

    this.combine = function combine(effect) {
        var result = null;
        if (effect instanceof Torchlight.t2.effects.buffs.Interrupt) {
            var combinedPercent = Math.max(this.percent, effect.percent, 100);
            result = new Torchlight.t2.effects.buffs.Interrupt(combinedPercent);
        }
        return result;
    };
};
Torchlight.t2.effects.buffs.Interrupt.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
