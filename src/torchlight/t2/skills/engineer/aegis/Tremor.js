
Torchlight.t2.skills.engineer.aegis.Tremor = function Tremor() {
    this.name = "tremor";
    this.displayName = "Tremor";
    this.description = "Discharging the energy from your suit into the earth causes a massive shockwave to expand outward, knocking back and weakening foes within 24 meters. Tremor only does the listed damage when it uses one Charge";
    this.tree = "aegis";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 18, 18, 18, 19, 19, 29, 21, 22, 24, 26, 28, 31, 33, 36, 39];
    var cooldown = 10;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "Enemies have 20% chance to flee for 10 seconds",
        "Weakened foes do 20% less physical damage",
        "All targets are stunned for 2 seconds"
    ];
    var knockback = 30;
    var stunDuration = 2;
    var physicalDamageReduction = 20;
    var effectDuration = 10;
    var chanceToFlee = 20;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Knockback", knockback),
        new Torchlight.t2.skills.Attribute(this, "Effect Duration", effectDuration),
        new Torchlight.t2.skills.Attribute(this, "Chance To Flee", chanceToFlee, 5),
        new Torchlight.t2.skills.Attribute(this, "Physical Damage Reduction", physicalDamageReduction, 10),
        new Torchlight.t2.skills.Attribute(this, "Stun", stunDuration, 15),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateDps),
        new Torchlight.t2.skills.Attribute(this, "Interrupt Chance", calculateInterruptChance),
        new Torchlight.t2.skills.Attribute(this, "Physical Damage Taken Increase", calculatePhysicalDamageTaken)
    ];

    /**
     * http://img1.gamersky.com/image2012/09/20120928m_7/10.jpg
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var dps = calculateDps(skillLevel);
        var interruptChance = calculateInterruptChance(skillLevel);
        var physicalDamageTakenIncrease = calculatePhysicalDamageTaken(skillLevel);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(dps),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.buffs.Interrupt(interruptChance),
            new Torchlight.t2.effects.DamageTakenPercent(physicalDamageTakenIncrease, effectDuration)
        ];

        if (skillLevel >= 5) {
            effects.push(new Torchlight.t2.effects.spells.Flee(chanceToFlee, effectDuration))
        }

        if (skillLevel >= 10) {
            effects.push(new Torchlight.t2.effects.EnemyDamageReduced(physicalDamageTakenIncrease, "Physical"))
        }

        if (skillLevel >= 15) {
            effects.push(new Torchlight.t2.effects.Stun(100, stunDuration))
        }

        return effects;
    };


    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDps(skillLevel) {
        return 45 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateInterruptChance(skillLevel) {
        return 36 + (4 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePhysicalDamageTaken(skillLevel) {
        return 8 + (2 * skillLevel);
    }

};
Torchlight.t2.skills.engineer.aegis.Tremor.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
