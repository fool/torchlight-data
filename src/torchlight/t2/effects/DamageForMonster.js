/**
 * +3% All Damage for each monster within 4m
 *
 * @param {number} percent
 * @param {string} damageType
 * @param {number} range
 * @constructor
 */
Torchlight.t2.effects.DamageForMonster = function DamageForMonster(percent, damageType, range) {
    this.toString = function toString() {
        return "+" + percent + "% " + damageType + " damage for each monster within " + range + "m";
    }
};
Torchlight.t2.effects.DamageForMonster.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
