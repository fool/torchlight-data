
Torchlight.t2.skills.engineer.aegis.SwordAndBoard = function SwordAndBoard() {
    this.name = "swordandboard";
    this.displayName = "Sword and Board";
    this.description = "Your skill with a shield goes beyond \"defense\" and straight to \"offense,\" adding some of your shield's armor value directly to your melee attacks as physical damage. 20% of shield armor added to melee attacks.;
    this.tree = "aegis";
    this.passive = true;
    this.skillLevelRequiredTier = 0;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Percent of Shield Armor added", calculateShieldArmorPercent)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var shieldArmorPercent = calculateShieldArmorPercent(skillLevel);
        return [
            new Torchlight.t2.effects.DamageShieldArmorPercentAdd(shieldArmorPercent)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateShieldArmorPercent(skillLevel) {
        return 15 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.aegis.SwordAndBoard.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
