
Torchlight.t2.effects.spells.Freeze = function Freeze(percent, duration) {
    this.percent = percent;
    this.duration = duration;
    this.toString = function toString() {
        return percent + "% chance to Freeze target for " + duration + " seconds";
    };


    /**
     * @param   {Torchlight.t2.effects.Effect} effect
     * @returns {Torchlight.t2.effects.spells.Freeze|null}
     */
    this.combine = function combine(effect) {
        var result = null;
        if (effect instanceof Torchlight.t2.effects.spells.Freeze) {
            var combinedPercent = Math.max(this.percent + effect.percent, 100);
            var combinedDuration = (this.duration + effect.duration) || undefined;
            result = new Torchlight.t2.effects.spells.Freeze(combinedPercent, combinedDuration);
        }
        return result;
    };
};
Torchlight.t2.effects.spells.Freeze.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
