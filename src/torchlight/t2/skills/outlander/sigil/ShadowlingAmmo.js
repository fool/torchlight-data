
Torchlight.t2.skills.outlander.sigil.ShadowlingAmmo = function ShadowlingAmmo() {
    this.skillLevelRequiredTier = 1;
    this.name        = "shadowlingammo";
    this.displayName = "Shadowling Ammo";
    this.description = "You curse your projectiles with shadow magic that has a chance of converting slain " +
                       "enemies into Shadlowing bats that fight for your for ten seconds. Death ritual improves " +
                       "Shadlowing summon duration and attack damage.";
    this.tree        = "sigil";
    this.passive     = true;
    this.attributes  = [
        new Torchlight.t2.skills.Attribute(this, "Minion Damage", calculateMinionDamage),
        new Torchlight.t2.skills.Attribute(this, "Shadowling Chance", calculateShadowingAllyChance)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var minionDamage = calculateMinionDamage(skillLevel, playerLevel);
        var shadowingAlly = calculateShadowingAllyChance(skillLevel);
        return [
            new Torchlight.t2.effects.ShadowlingAllyOnKill(shadowingAlly),
            new Torchlight.t2.effects.MinionsDealDamage(minionDamage, "Physical")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateShadowingAllyChance(skillLevel) {
        return 8 + (4 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateMinionDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(Math.max(1.05 - (0.05 * skillLevel), 0));
    }

};
Torchlight.t2.skills.outlander.sigil.ShadowlingAmmo.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
