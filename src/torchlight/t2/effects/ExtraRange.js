
Torchlight.t2.effects.ExtraRange = function ExtraRange(range) {
    this.toString = function toString() {
        return "+" + range + " to Bow, Crossbow, Pistol and Wand range";
    }
};
Torchlight.t2.effects.ExtraRange.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
