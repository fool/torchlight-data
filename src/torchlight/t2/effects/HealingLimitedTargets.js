
Torchlight.t2.effects.HealingLimitedTargets = function HealingLimitedTargets(targets) {
    this.toString = function toString() {
        return "The healing is limited to a maximum of " + targets + " targets per burst";
    }
};
Torchlight.t2.effects.HealingLimitedTargets.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
