
Torchlight.t2.effects.SlowResistance = function SlowResistance(percent, duration) {
    var d = "";
    if (typeof duration === "number" && duration > 0) {
        d = " for " + duration + " seconds";
    }
    this.toString = function toString() {
        return percent + "% Slow Resistance" + d;
    }
};
Torchlight.t2.effects.SlowResistance.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
