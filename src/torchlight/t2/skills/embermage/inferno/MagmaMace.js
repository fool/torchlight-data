
Torchlight.t2.skills.embermage.inferno.MagmaMace = function MagmaMace() {
    this.name = "magmamace";
    this.displayName = "Magma Mace";
    this.description = "You summon an enormous mace of fire that channels the energy of your weapons, stunning foes in a 5 meter arc and setting them ablaze. Magma Mace does not build Charge.";
    this.tree = "inferno";
    this.skillLevelRequiredTier = 1;
    this.manaCost = [0,	12, 13, 14, 15, 16, 17, 19, 22, 24, 27, 30, 32, 36, 40, 45];
    this.tierBonuses = [
        "Magma Mace has 25% chance to break shields",
        "Hit slows enemy attacks by 33% for 3 seconds",
        "Hit slows enemy attacks by 67% for 3 seconds"
    ];
    var shieldBreakChance = 25;
    var stunChance = 80;
    var stunDuration = 3;
    var attackSpeedReductionDuration = 3;
    var fireDamageDuration = 6;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Stun Chance", stunChance),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", stunDuration),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage Duration", fireDamageDuration),
        new Torchlight.t2.skills.Attribute(this, "Break Shields", shieldBreakChance, 5),
        new Torchlight.t2.skills.Attribute(this, "Attack Speed Reduction Duration", attackSpeedReductionDuration, 10),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage", calculateFireDamage),
        new Torchlight.t2.skills.Attribute(this, "Attack speed reduced", calculateAttackSpeedReduction, 10)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var fireDamage = new Torchlight.lib.Range(calculateWeaponDps(skillLevel));
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Fire"),
            new Torchlight.t2.effects.Stun(stunChance, stunDuration),
            new Torchlight.t2.effects.Damage(fireDamage, "Fire", fireDamageDuration)
        ];

        if (skillLevel >= 5) {
            effects.push(new Torchlight.t2.effects.spells.BreakShields(shieldBreakChance));
        }

        if (skillLevel >= 10) {
            var attackSpeed = calculateAttackSpeedReduction(skillLevel);
            effects.push(new Torchlight.t2.effects.debuffs.SpeedDecrease(attackSpeed, Torchlight.t2.effects.Effect.speeds.Attack, attackSpeedReductionDuration));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 47 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFireDamage(skillLevel) {
        return Math.pow(1.6, skillLevel)
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAttackSpeedReduction(skillLevel) {
        return -1 * Torchlight.lib.squeeze([0, 0, 33, 67], skillLevel / 5);
    }
};
Torchlight.t2.skills.embermage.inferno.MagmaMace.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
