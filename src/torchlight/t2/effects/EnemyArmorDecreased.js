/**
 * Add Armor type?
 *
 * @param {number} percent
 * @param {number} duration optional
 * @constructor
 */
Torchlight.t2.effects.EnemyArmorDecreased = function EnemyArmorDecreased(percent, duration) {
    var d = "";
    if (typeof duration === "number" && duration > 0) {
        d = " for " + duration + " seconds";
    }
    this.toString = function toString() {
        return "Enemy Armor decreased by " + percent + "%" + d
    }
};
Torchlight.t2.effects.EnemyArmorDecreased.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
