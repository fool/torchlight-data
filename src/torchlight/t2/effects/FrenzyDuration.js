
Torchlight.t2.effects.FrenzyDuration = function FrenzyDuration(duration) {
    this.toString = function toString() {
        return "Frenzy duration increased to " + duration + " seconds";
    }
};
Torchlight.t2.effects.FrenzyDuration.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
