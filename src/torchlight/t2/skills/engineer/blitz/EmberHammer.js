
Torchlight.t2.skills.engineer.blitz.EmberHammer = function EmberHammer() {
    this.name = "emberhammer";
    this.displayName = "Ember Hammer";
    this.description = "You deliver a powerful sidelong swing which channels ember energy into a wide, 270 degree arc, destroying shields. Ember Hammer does not gain Charge";
    this.tree = "blitz";
    this.skillLevelRequiredTier = 2;
    this.manaCost = [0, 16, 16, 17, 17, 18, 19, 20, 22, 24, 27, 30, 33, 36, 39, 43];
    this.tierBonuses = [
        "Damage is amplified by 5% for every Charge",
        "Damage is amplified by 10% for every Charge",
        "Damage is amplified by 15% for every Charge"
    ];
    var shieldBreakChance = 100;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Shield Break Chance", shieldBreakChance),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDps)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var dps = calculateWeaponDps(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(dps, "Electric"),
            new Torchlight.t2.effects.spells.BreakShields(shieldBreakChance)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 79 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamageAmplification(skillLevel) {
        return Torchlight.lib.squeeze([0, 5, 10, 15], skillLevel / 5);
    }
};
Torchlight.t2.skills.engineer.blitz.EmberHammer.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
