
Torchlight.t2.skills.engineer.blitz.FlameHammer = function FlameHammer() {
    this.name = "flamehammer";
    this.displayName = "Flame Hammer";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "blitz";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 14, 15, 16, 17, 18, 20, 23, 26, 28, 32, 36, 40, 45, 50, 55];
    this.tierBonuses = [
        "Creates 5 flaming splinters travelling 8 meters",
        "Creates 6 flaming splinters travelling 11 meters",
        "Creates 7 flaming splinters travelling 14 meters"
    ];
    var burnDuration = 3;
    this.getRange = function getRange(skillLevel) {
        return Torchlight.lib.squeeze([5, 8, 11, 14], skillLevel / 5);
    };
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Burn Duration", burnDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Flaming Splinters", calculateFlamingSplinters),
        new Torchlight.t2.skills.Attribute(this, "Weapon Dps", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Burn Chance", calculateBurnChance)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var burnChance = calculateBurnChance(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Fire"),
            new Torchlight.t2.effects.spells.Burn(burnChance, burnDuration)
        ];
    };


    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFlamingSplinters(skillLevel) {
        return Torchlight.lib.squeeze([4, 5, 6, 7], skillLevel / 5);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 64 + (4 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBurnChance(skillLevel) {
        return 10 + (5 * skillLevel);
    }

};
Torchlight.t2.skills.engineer.blitz.FlameHammer.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
