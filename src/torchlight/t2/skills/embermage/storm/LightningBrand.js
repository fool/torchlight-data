
Torchlight.t2.skills.embermage.storm.LightningBrand = function LightningBrand() {
    this.name = "lightningbrand";
    this.displayName = "Lightning Brand";
    this.description = "When you hit a shocked enemy, you do an additional burst of lightning damage.";
    this.tree = "storm";
    this.passive = true;
    this.skillLevelRequiredTier = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Lightning Damage", calculateDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damage = calculateDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.Damage(damage, "Lightning")
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateDamage(skillLevel, playerLevel) {
        var lower = 2215;
        var upper = 2847;
        return new Torchlight.lib.Range(lower, upper);
    }
};
Torchlight.t2.skills.embermage.storm.LightningBrand.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
