
Torchlight.t2.skills.engineer.blitz.SeismicSlam = function SeismicSlam() {
    this.name = "seismicslam";
    this.displayName = "Seismic Slam";
    this.description = "Slams the ground with the Engineer's mechanically-augmented boot, stunning nearby enemies and splashing them with volatile fuel to deal fire damage";
    this.tree = "blitz";
    this.skillLevelRequiredTier = 1;
    this.manaCost = [0, 12, 12, 12, 13, 13, 15, 16, 17, 20, 22, 25, 27, 30, 33, 36];
    var cooldown = 1;
    this.cooldown = [1];
    this.tierBonuses = [
        "Radius increased to 6 meters",
        "Radius increased to 8 meters",
        "Radius increased to 10 meters"
    ];
    var stunDuration = 2;
    var interruptChance = 100;
    var burnDuration = 5;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", stunDuration),
        new Torchlight.t2.skills.Attribute(this, "Interrupt Chance", interruptChance),
        new Torchlight.t2.skills.Attribute(this, "Burn Duration", burnDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance", calculateChanceToStun),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage", calculateFireDamage),
        new Torchlight.t2.skills.Attribute(this, "Burn Damage", calculateBurnDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var stunChance = calculateChanceToStun(skillLevel);
        var fireDamage = new Torchlight.lib.Range(calculateFireDamage(skillLevel, playerLevel));
        var burnDamage = calculateBurnDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.Stun(stunChance, stunDuration),
            new Torchlight.t2.effects.spells.Burn(burnDamage, burnDuration),
            new Torchlight.t2.effects.Damage(fireDamage, "Fire"),
            new Torchlight.t2.effects.buffs.Interrupt(interruptChance)
        ];
    };


    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToStun(skillLevel) {
        return 62 + (2 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateFireDamage(skillLevel, playerLevel) {
        return 62 + (2 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateBurnDamage(skillLevel, playerLevel) {
        return 1 + (1 * skillLevel);
    }


};
Torchlight.t2.skills.engineer.blitz.SeismicSlam.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
