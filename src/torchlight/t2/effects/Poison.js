/**
 * @param {number} chance
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.Poison = function Poison(chance, duration) {
    this.toString = function toString() {
        return chance + "% chance to Poison" + this.durationText(duration);
    }
};
Torchlight.t2.effects.Poison.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
