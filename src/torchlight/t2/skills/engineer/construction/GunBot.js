
Torchlight.t2.skills.engineer.construction.GunBot = function GunBot() {
    this.name = "gunbot";
    this.displayName = "Gun Bot";
    this.description = "You deploy a small drone that assaults your enemies with a hail of gunfire, firing 5 rounds per second. Each rank increases the bot's damage potential";
    this.tree = "construction";
    this.skillLevelRequiredTier = 3;
    this.manaCost = [0, 41, 42, 43, 43, 45, 47, 50, 53, 58, 63, 69, 76, 83, 90, 98];
    this.getCooldown = function getCooldown(skillLevel) {
        return Torchlight.lib.squeeze([3, 2.5, 2, 1.5], skillLevel / 5);
    };
    var duration = 60;
    this.duration = [duration];
    this.tierBonuses = [
        "Firing range doubled. Summoning cooldown reduced to 2.5 minutes",
        "Shots pierce enemies. Summoning cooldown reduced to 2 minutes",
        "Triple-spread shots. Summoning cooldown reduced to 1.5 minutes"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Duration", duration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "All Damage", calculateAllDamage),
        new Torchlight.t2.skills.Attribute(this, "Minions Damage", calculateMinionsDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var allDamage = calculateAllDamage(skillLevel);
        var minionsDamage = calculateMinionsDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.DamagePercent(allDamage, "All"),
            new Torchlight.t2.effects.MinionsDealDamage(minionsDamage, "Physical")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAllDamage(skillLevel) {
        return 28 + (2 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateMinionsDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(28 + (2 * skillLevel));
    }
};
Torchlight.t2.skills.engineer.construction.GunBot.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
