
Torchlight.t2.skills.embermage.inferno.ElementalAttunement = function ElementalAttunement() {
    this.name = "elementalattunement";
    this.displayName = "Elemental Attunement";
    this.description = "The Embermage is finely attuned to the elements, inflicting elemental effects on nearby enemies and extending the duration of all elemental effects. ";
    this.tree = "inferno";
    this.skillLevelRequiredTier = 1;
    this.passive = true;
    var strikeDuration = 1;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Strike Duration", strikeDuration),
        new Torchlight.t2.skills.Attribute(this, "Strike Chance", calculateChanceStrike),
        new Torchlight.t2.skills.Attribute(this, "Effect Duration Extended", calculateEffectDurationExtended)
    ];

    /**
     * @param   {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var effectsDurationExtended = calculateEffectDurationExtended(skillLevel);
        var strikeChance = calculateChanceStrike(skillLevel);
        return [
            new Torchlight.t2.effects.spells.Burn(strikeChance, strikeDuration),
            new Torchlight.t2.effects.Shock(strikeChance, strikeDuration),
            new Torchlight.t2.effects.spells.Freeze(strikeChance, strikeDuration),
            new Torchlight.t2.effects.Poison(strikeChance, strikeDuration),
            new Torchlight.t2.effects.ExtendedElementalEffects(effectsDurationExtended)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceStrike(skillLevel) {
        return skillLevel;
    }

    /**
     * All Burn, Shock, Freeze, and Poison effects caused by your embermage are extended by this many seconds.
     *
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateEffectDurationExtended(skillLevel) {
        if (skillLevel === 2) {
            return 1;
        }
        return skillLevel;
    }
};
Torchlight.t2.skills.embermage.inferno.ElementalAttunement.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
