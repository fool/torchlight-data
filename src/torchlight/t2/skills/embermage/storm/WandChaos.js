
Torchlight.t2.skills.embermage.storm.WandChaos = function WandChaos() {
    this.name = "wandchaos";
    this.displayName = "Wand Chaos";
    this.description = "Attacks using your wand can result in bizarre, random elemental distrubances. These effects can also happen with the Magma Mace, Magama Spear, Icy Blast, and Shockbolts skills.";
    this.tree = "storm";
    this.passive = true;
    this.skillLevelRequiredTier = 1;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Chance to cast", calculateChanceToCast)
    ];

    /**
     * http://i.imgur.com/FPeSH9E.jpg
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var chanceToCast = calculateChanceToCast(skillLevel);
        return [
            new Torchlight.t2.effects.CastSpell(chanceToCast, "Bizzare Effect", "strike")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToCast(skillLevel) {
        return 0 + (8 * skillLevel);
    }
};
Torchlight.t2.skills.embermage.storm.WandChaos.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
