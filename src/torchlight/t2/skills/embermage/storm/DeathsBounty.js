
Torchlight.t2.skills.embermage.storm.DeathsBounty = function DeathsBounty() {
    this.name = "deathsbounty";
    this.displayName = "Death's Bounty";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "storm";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 21, 21, 21, 22, 22, 23, 24, 25, 27, 29, 32, 35, 37, 41, 44];
    this.getCooldown = function getCooldown(skillLevel) {
        return Math.max(2.5 - (0.1 * skillLevel), 0);
    };
    this.getDuration = function getDuration(skillLevel) {
        return 8 + (2 * skillLevel);
    };
    this.tierBonuses = [
        "4 soul bolts are released. Additional 33% movement speed reduction on targets. ",
        "5 soul bolts are released. Additional 50% casting speed. ",
        "6 soul bolts are released. Targets are stunned for 1 second. "
    ];
    var movementSpeedReductionDuration = 2;
    var movementSpeedReduction = -33;
    var castingSpeedIncrease = 50;
    var stunDuration = 1;
    var stunChance = 100;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Stun", stunChance, 15),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration", this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "Health/s", calculateHealthRecovery),
        new Torchlight.t2.skills.Attribute(this, "Mana/s", calculateManaRecovery),
        new Torchlight.t2.skills.Attribute(this, "Soul Bolts", calculateSoulBolts),
        new Torchlight.t2.skills.Attribute(this, "Movement Speed Reduction", movementSpeedReduction, 5),
        new Torchlight.t2.skills.Attribute(this, "Casting Speed Increase", castingSpeedIncrease, 10)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var healthRecovery = calculateHealthRecovery(skillLevel);
        var manaRecovery = calculateManaRecovery(skillLevel);
        var effects = [
            new Torchlight.t2.effects.HealthRecoveryPerSecond(healthRecovery),
            new Torchlight.t2.effects.ManaRecoveryPerSecond(manaRecovery)
        ];

        if (skillLevel >= 5) {
            effects.push(new Torchlight.t2.effects.debuffs.SpeedDecrease(movementSpeedReduction, Torchlight.t2.effects.Effect.speeds.Movement, movementSpeedReductionDuration));
        }

        if (skillLevel >= 10) {
            effects.push(new Torchlight.t2.effects.buffs.SpeedIncrease(castingSpeedIncrease, Torchlight.t2.effects.Effect.speeds.Cast));
        }

        if (skillLevel >= 15) {
            effects.push(new Torchlight.t2.effects.Stun(stunChance, stunDuration));
        }

        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateSoulBolts(skillLevel) {
        return Torchlight.lib.squeeze([3, 4, 5, 6], skillLevel / 5);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateHealthRecovery(skillLevel) {
        return 4.5 + (0.5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateManaRecovery(skillLevel) {
        return 4.5 + (0.5 * skillLevel);
    }

};
Torchlight.t2.skills.embermage.storm.DeathsBounty.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
