/**
 *
 * @param {number} value
 * @param {string} resistanceType
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.Resistance = function Resistance(value, resistanceType, duration) {
    var sign = value > 0 ? '+' : '';
    this.toString = function toString() {
        return sign + value + " " + resistanceType + " Resistance" + this.durationText(duration);
    }
};
Torchlight.t2.effects.Resistance.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
