/**
 * @constructor
 */
Torchlight.t2.effects.GeneratesNoCharge = function GeneratesNoCharge() {
    this.toString = function toString() {
        return "Generates no charge";
    }
};
Torchlight.t2.effects.GeneratesNoCharge.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
