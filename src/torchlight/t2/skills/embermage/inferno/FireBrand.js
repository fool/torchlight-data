
Torchlight.t2.skills.embermage.inferno.FireBrand = function FireBrand() {
    this.name = "firebrand";
    this.displayName = "Fire Brand";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "inferno";
    this.skillLevelRequiredTier = 2;
    this.passive = true;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Fire Damage", calculateFireDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var fireDamage = calculateFireDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.Damage(fireDamage, "Fire")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateFireDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(500);
    }
};
Torchlight.t2.skills.embermage.inferno.FireBrand.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
