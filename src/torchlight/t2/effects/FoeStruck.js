
Torchlight.t2.effects.FoeStruck = function FoeStruck(percent, effect) {
    var p = percent + "% chance each foe struck will result in ";
    if (percent === 100) {
        p = "Each foe struck will "
    }

    this.toString = function toString() {
        return p + effect.toString();
    }
};
Torchlight.t2.effects.FoeStruck.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
