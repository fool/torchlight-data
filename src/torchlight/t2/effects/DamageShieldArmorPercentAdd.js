/**
 * @param {number} percent
 * @constructor
 */
Torchlight.t2.effects.DamageShieldArmorPercentAdd = function DamageShieldArmorPercentAdd(percent) {
    this.toString = function toString() {
        return percent + "% of shield Armor added to melee attacks";
    }
};
Torchlight.t2.effects.DamageShieldArmorPercentAdd.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
