
Torchlight.t2.skills.engineer.construction.ChargeDomination = function ChargeDomination() {
    this.name = "chargedomination";
    this.displayName = "Charge Domination";
    this.description = "When the Engineer slays an enemy, they have a chance to absorb enough energy to fill their Charge bar. This effect cannot trigger more than once every three seconds.";
    this.tree = "construction";
    this.passive = true;
    this.skillLevelRequiredTier = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Charge Refill Chance", calculateChance)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var chance = calculateChance(skillLevel);
        return [
            new Torchlight.t2.effects.spells.ChargeRefill(100, chance)
        ];
    };


    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChance(skillLevel) {
        return 2.5 + (0.5 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.construction.ChargeDomination.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
