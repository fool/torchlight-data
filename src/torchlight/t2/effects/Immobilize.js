
Torchlight.t2.effects.Immobilize = function Immobilize(percent, duration) {
    this.toString = function toString() {
        return percent + "% chance to immobilize Target for " + duration + " seconds";
    }
};
Torchlight.t2.effects.Immobilize.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
