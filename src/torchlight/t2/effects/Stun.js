
Torchlight.t2.effects.Stun = function Stun(percent, duration) {
    var dur = "";
    if (typeof duration === "number") {
        dur = " for " + duration + " seconds";
    }
    this.toString = function toString() {
        return percent + "% chance to Stun target" + dur;
    }
};
Torchlight.t2.effects.Stun.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
