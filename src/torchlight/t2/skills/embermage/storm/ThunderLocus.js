
Torchlight.t2.skills.embermage.storm.ThunderLocus = function ThunderLocus() {
    this.name = "thunderlocus";
    this.displayName = "Thunder Locus";
    this.description = "You summon a concentraited thunderstorm at a target location which blasts foes within 7 meters for the duration of it's existence. Thunder locus does not build Charge.";
    this.tree = "storm";
    this.skillLevelRequiredTier = 2;
    this.manaCost = [0, 25, 26, 26, 27, 29, 31, 33, 35, 38, 43, 48, 53, 57, 64, 69];
    this.duration = [0, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42];
    var cooldown = 10;
    var range = 7;
    this.cooldown = [cooldown];
    this.range = [range];
    this.tierBonuses = [
        "The Locus can blast two foes at once",
        "The Locus can blast three foes at once",
        "The Locus can blast four foes at once"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Range", range),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Duration", this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "Damage", calculateDamage),
        new Torchlight.t2.skills.Attribute(this, "Summon Duration", calculateSummonDuration),
        new Torchlight.t2.skills.Attribute(this, "Max Targets", calculateMaxTargets)
    ];

    /**
     * http://i.imgur.com/lKuL0L7.jpg
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damage = calculateDamage(skillLevel, playerLevel);
        var summonDuration = calculateSummonDuration(skillLevel);
        return [
            new Torchlight.t2.effects.Damage(damage, "Electric"),
            new Torchlight.t2.effects.SummonDuration(summonDuration)
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(skillLevel * 5  + playerLevel * 10);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateSummonDuration(skillLevel) {
        return 12 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateMaxTargets(skillLevel) {
        return Torchlight.lib.squeeze([1, 2, 3, 4], skillLevel / 5);
    }
};
Torchlight.t2.skills.embermage.storm.ThunderLocus.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
