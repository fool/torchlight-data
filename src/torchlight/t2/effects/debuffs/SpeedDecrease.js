/**
 * Ex:
 *   Cast Speed reduced by 100% for 1.6 seconds
 *
 * @param {number} percent
 * @param {number} speedName  This can be positive or negative
 * @param {number} [duration] how long this lasts
 * @constructor
 */
Torchlight.t2.effects.buffs.SpeedDecrease = function Speed(percent, speedName, duration) {
    /**
     * Percent will always be negative
     * @type {number}
     */
    this.percent = Math.max(-1 * Math.abs(percent), -100);
    this.speedName = Torchlight.t2.effects.Effect.validateSpeedName(speedName, Torchlight.t2.effects.Effect.speeds.Attack);
    this.duration = duration;
    this.toString = function toString() {
        return this.speedName + " Speed " + this.affectedByPercent(percent) + this.durationText(duration);
    };

    /**
     * @param   {Torchlight.t2.effects.Effect} effect
     * @returns {Torchlight.t2.effects.Effect|null}
     */
    this.combine = function combine(effect) {
        var result = null;
        if (effect instanceof Torchlight.t2.effects.buffs.SpeedDecrease) {
            if (this.speedName === effect.speedName) {
                var combinedPercent = Math.max(his.percent + effect.percent, -100);
                var combinedDuration = undefined;
                if (typeof this.duration === "number") {
                    combinedDuration = this.duration;
                }
                if (typeof effect.duration === "number") {
                    if (combinedDuration) {
                        combinedDuration += effect.duration;
                    } else {
                        combinedDuration = effect.duration;
                    }
                }
                result = new Torchlight.t2.effects.buffs.SpeedDecrease(combinedPercent, this.speedName, combinedDuration);
            }

        }
        return result;
    }
};
Torchlight.t2.effects.buffs.SpeedDecrease.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
