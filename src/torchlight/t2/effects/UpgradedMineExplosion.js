/**
 * @param {number} chance
 * @constructor
 */
Torchlight.t2.effects.UpgradedMineExplosion = function UpgradedMineExplosion(chance) {
    this.toString = function toString() {
        return chance + "% chance for an upgraded mine explosion";
    }
};
Torchlight.t2.effects.UpgradedMineExplosion.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
