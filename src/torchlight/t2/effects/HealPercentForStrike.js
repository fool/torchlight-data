
Torchlight.t2.effects.HealPercentForStrike = function HealPercentForStrike(percent, targets) {
    this.toString = function toString() {
        return "Heals" + percent + "% of your health for up to " + targets + " targets struck"
    }
};
Torchlight.t2.effects.HealPercentForStrike.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
