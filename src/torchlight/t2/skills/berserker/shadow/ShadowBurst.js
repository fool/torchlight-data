
Torchlight.t2.skills.berserker.shadow.ShadowBurst = function ShadowBurst() {
    this.name = "shadowburst";
    this.displayName = "Shadow Burst";
    this.description = "You transform into a spectral wolf and lunge through your enemies, damaging them even as you heal your own body. Shadow bursts also has a chance to immediately destroy the shields of any foes struck.";
    this.tree = "shadow";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 9, 10, 11, 12, 13, 14, 16, 18, 20, 22, 25, 28, 31, 34, 38];
    this.range    = [0, 5,  5,  5,  5,  7,  7,  7,  7,  7,  9,  9,  9,  9,  9, 11];
    var cooldown = 0.7;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "Healing can be derived from 3 targets per Burst. Chance to destroy shields is increased to 50%",
        "Healing can be derived from 3 targets per Burst. Chance to destroy shields is increased to 50%",
        "Healing can be derived from 5 targets per Burst. Chance to destroy shields is increased to 100%"
    ];
    var targetsStruck = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown",        cooldown),
        new Torchlight.t2.skills.Attribute(this, "Targets Struck",  targetsStruck),
        new Torchlight.t2.skills.Attribute(this, "Mana",            this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range",           this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Heal %",          calculateHeal),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %",    calculateElectricDamage),
        new Torchlight.t2.skills.Attribute(this, "Max Targets",     calculateMaxTargets),
        new Torchlight.t2.skills.Attribute(this, "Break Shields %", calculateBreakShieldChance)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var healPercent = calculateHeal(skillLevel);
        var electricDamage = calculateElectricDamage(skillLevel);
        var maxTargets = calculateMaxTargets(skillLevel);
        var breakShieldChance = calculateBreakShieldChance(skillLevel);
        return [
            new Torchlight.t2.effects.HealPercentForStrike(healPercent, targetsStruck),
            new Torchlight.t2.effects.PercentOfWeaponDps(electricDamage, "Eletric"),
            new Torchlight.t2.effects.HealingLimitedTargets(maxTargets),
            new Torchlight.t2.effects.spells.BreakShields(breakShieldChance)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateHeal(skillLevel) {
        return 4.5 + (0.5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateElectricDamage(skillLevel) {
        return 28 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateMaxTargets(skillLevel) {
        return Torchlight.lib.squeeze([2, 3, 4, 5], skillLevel / 5);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */

    function calculateBreakShieldChance(skillLevel) {
        return Torchlight.lib.squeeze([25, 50, 75, 100], skillLevel / 5);
    }
};
Torchlight.t2.skills.berserker.shadow.ShadowBurst.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
