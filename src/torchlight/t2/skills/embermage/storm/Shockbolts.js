
Torchlight.t2.skills.embermage.storm.Shockbolts = function Shockbolts() {
    this.name = "shockbolts";
    this.displayName = "Shockbolts";
    this.description = "You release a large electric missle which detonates and releases 4 electronically chaged bolts, which follow the ground and ricochet off neighboring objects";
    this.tree = "storm";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 23, 23, 24, 24, 25, 25, 26, 27, 29, 31, 34, 36, 39, 41, 44];
    this.tierBonuses = [
        "Bolts have a 10% chance to immobilize, increased to 5 bolts at impact",
        "Bolts increase in speed, increase to 6 bolts at impact",
        "Increases to 7 bolts at impact"
    ];
    var immobilizeChance = 10;
    var immobilizeDuration = 3;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Immobilize Chance", immobilizeChance, 5),
        new Torchlight.t2.skills.Attribute(this, "Immobilize Duration", immobilizeDuration, 5),
        new Torchlight.t2.skills.Attribute(this, "Bolts", calculateBolts),
        new Torchlight.t2.skills.Attribute(this, "Electric Damage", calculateElectricDamage)
    ];

    /**
     * http://i.imgur.com/EIBeUKN.jpg
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var electricDamage = calculateElectricDamage(skillLevel);

        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(electricDamage, "Electric"),
            new Torchlight.t2.effects.Immobilize(immobilizeChance, immobilizeDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateElectricDamage(skillLevel) {
        return 23 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBolts(skillLevel) {
        return Torchlight.lib.squeeze([4, 5, 6, 7], skillLevel / 5);
    }
};
Torchlight.t2.skills.embermage.storm.Shockbolts.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
