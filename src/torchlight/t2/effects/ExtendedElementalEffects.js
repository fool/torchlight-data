/**
 * @param {number} chance
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.ExtendedElementalEffects = function Armor(chance, duration) {
    this.toString = function toString() {
        return '+' + duration + "'seconds to all Burn, Shock, Freeze, and Posion effects";
    }
};
Torchlight.t2.effects.ExtendedElementalEffects.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
