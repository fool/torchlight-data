
Torchlight.t2.skills.outlander.lore.BurningLeap = function BurningLeap() {
    this.skillLevelRequiredTier = 5;
    this.name        = "burningleap";
    this.displayName = "Burning Leap";
    this.description = "You execute a fiery, spinning leap, up to 7 meters forward. Any foes hit along the way are " +
                       "knocked back and set aflame.";
    this.tree        = "lore";
    this.manaCost    = [0, 34, 34, 35, 35, 36, 37, 39, 40, 42, 46, 49, 53, 57, 61, 65];
    this.range       = [0,  7,  7,  7,  7,  9,  9,  9,  9,  9, 11, 11, 11, 11, 11, 13];
    this.tierBonuses = [
        "10% faster to cast, range increased to 9 meters",
        "20% faster to cast, range increased to 11 meters",
        "30% faster to cast, range increased to 13 meters"
    ];
    var knockback = 45;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Knockback",     knockback),
        new Torchlight.t2.skills.Attribute(this, "Mana",          this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range",         this.getRange),
        new Torchlight.t2.skills.Attribute(this, "DPS %",         calculateDps),
        new Torchlight.t2.skills.Attribute(this, "Brand Chance",  calculateBrandChance),
        new Torchlight.t2.skills.Attribute(this, "Fire Duration", calculateFireDuration)
    ];

    /**
     * @param   {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var weaponDps = calculateDps(skillLevel);
        var burnChance = calculateBrandChance(skillLevel);
        var fireDuration = calculateFireDuration(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Fire"),
            new Torchlight.t2.effects.spells.Burn(burnChance, fireDuration),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.GeneratesNoCharge()
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDps(skillLevel) {
        return 70 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBrandChance(skillLevel) {
        return 25 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFireDuration(skillLevel) {
        return 3.7 + (0.3 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.lore.BurningLeap.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
