/**
 *
 * @param {number} percent
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.spells.Miss = function Miss(percent, duration) {
    this.percent = percent;
    this.duration = duration;
    this.toString = function toString() {
        return percent + "% increased Chance To Miss" + this.durationText(duration);
    };

    /**
     * @param   {Torchlight.t2.effects.Effect} effect
     * @returns {Torchlight.t2.effects.spells.Miss|null}
     */
    this.combine = function combine(effect) {
        var result = null;
        if (effect instanceof Torchlight.t2.effects.spells.Miss) {
            var combinedPercent = Math.max(this.percent + effect.percent, 100);
            var combinedDuration = (this.duration + effect.duration) || undefined;
            result = new Torchlight.t2.effects.spells.Miss(combinedPercent, combinedDuration);
        }
        return result;
    };
};
Torchlight.t2.effects.spells.Miss.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
