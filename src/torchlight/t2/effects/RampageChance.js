
Torchlight.t2.effects.RampageChance = function RampageChance(percent) {
    this.toString = function toString() {
        return percent + "% chance of Rampage after killing an enemy";
    }
};
Torchlight.t2.effects.RampageChance.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
