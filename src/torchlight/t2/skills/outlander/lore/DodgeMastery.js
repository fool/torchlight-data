
Torchlight.t2.skills.outlander.lore.DodgeMastery = function DodgeMastery() {
    this.skillLevelRequiredTier = 0;
    this.name        = "dodgemastery";
    this.displayName = "Dodge Mastery";
    this.description = "Your acrobatic training increases your odds of dodging a blow.";
    this.tree        = "lore";
    this.passive     = true;
    this.attributes  = [
        new Torchlight.t2.skills.Attribute(this, "Dodge %", calculateDodgeChance)
    ];

    /**
     * @param   {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var dodgeChance = calculateDodgeChance(skillLevel);
        return [
            new Torchlight.t2.effects.Dodge(dodgeChance)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDodgeChance(skillLevel) {
        return 2 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.lore.DodgeMastery.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
