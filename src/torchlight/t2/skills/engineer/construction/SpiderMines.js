
Torchlight.t2.skills.engineer.construction.SpiderMines = function SpiderMines() {
    this.name = "spidermines";
    this.displayName = "Spider Mines";
    this.description = "You deploy a trio of spider drones that charge the nearest foes and explode in a 3 meter radius. Upgraded mines do 20% more damage with a 5 meter radius, a 2 second stun, and electric damage over time";
    this.tree = "construction";
    this.skillLevelRequiredTier = 2;
    this.manaCost = [0, 32, 33, 33, 35, 29, 31, 33, 35, 39, 32, 36, 40, 43, 48, 35];
    this.getCooldown = function getCooldown(skillLevel) {
        return 3.1 - (0.1 * skillLevel);
    };
    this.getDuration = function getDuration(skillLevel) {
        return 14 + (1 * skillLevel);
    };
    this.tierBonuses = [
        "Mana cost reduced by 20%",
        "Mana cost reduced by 40%",
        "Mana cost reduced by 60%"
    ];
    var fireDamageDuration = 3;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Fire Damage Duration", fireDamageDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration", this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "Upgraded Explosion Chance", calculateUpgradedMineExplosionChance),
        new Torchlight.t2.skills.Attribute(this, "Minion Physical Damage", calculateMinionPhysicalDamage),
        new Torchlight.t2.skills.Attribute(this, "Minion Fire Damage", calculateMinionFireDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var upgradedExplosionChance = calculateUpgradedMineExplosionChance(skillLevel);
        var minionPhysicalDamage = new Torchlight.lib.Range(calculateMinionPhysicalDamage(skillLevel, playerLevel));
        var minionFireDamage = new Torchlight.lib.Range(calculateMinionFireDamage(skillLevel, playerLevel));
        return [
            new Torchlight.t2.effects.MinionsDealDamage(minionPhysicalDamage, "Physical"),
            new Torchlight.t2.effects.MinionsDealDamage(minionFireDamage, "Fire", fireDamageDuration),
            new Torchlight.t2.effects.UpgradedMineExplosion(upgradedExplosionChance)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateUpgradedMineExplosionChance(skillLevel) {
        return 0 + (5 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateMinionPhysicalDamage(skillLevel, playerLevel) {
        return 18 + (2 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateMinionFireDamage(skillLevel, playerLevel) {
        return 18 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.construction.SpiderMines.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
