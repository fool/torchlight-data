
Torchlight.t2.skills.outlander.sigil.Shadowmantle = function Shadowmantle() {
    this.name = "shadowmantle";
    this.displayName = "Shadowmantle";
    this.description = "You call forth a shadow hex protecting you from ranged attackers. The shadowmantle can reflect damage back to the attackers, and it has a chance to blind them for 6 seconds";
    this.tree = "sigil";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 23, 23, 24, 24, 25, 25, 26, 27, 29, 31, 34, 36, 39, 41, 44];
    this.cooldown = [0, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16];
    var duration = 10;
    this.duration = [duration];
    this.tierBonuses = [
        "The hex reflects 125% of the missile damage",
        "The hex reflects 175% of the missile damage",
        "The hex reflects 250% of the missile damage"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Duration", duration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "Relefect Chance", calculateMissileReflectChance),
        new Torchlight.t2.skills.Attribute(this, "Missles Relefect %", calculateMissileReflectAmount),
        new Torchlight.t2.skills.Attribute(this, "Shadowblind %", calculateShadowblindChance)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var reflectChance = calculateMissileReflectChance(skillLevel);
        var reflectAmount = calculateMissileReflectAmount(skillLevel);
        var shadowBlindChance = calculateShadowblindChance(skillLevel);
        return [
            new Torchlight.t2.effects.ReflectMissiles(reflectChance, reflectAmount),
            new Torchlight.t2.effects.CastSpell(shadowBlindChance, "Shadowblind", "when struck")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateMissileReflectChance(skillLevel) {
        return 30 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateMissileReflectAmount(skillLevel) {
        return Torchlight.lib.squeeze([75, 125, 175, 250], skillLevel / 5);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateShadowblindChance(skillLevel) {
        return 30 + (3 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.sigil.Shadowmantle.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
