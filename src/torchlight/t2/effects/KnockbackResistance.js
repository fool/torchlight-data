
Torchlight.t2.effects.KnockbackResistance = function KnockbackResistance(percent, duration) {
    var d = "";
    if (typeof duration === "number" && duration > 0) {
        d = " for " + duration + " seconds";
    }
    this.toString = function toString() {
        return percent + "% Knockback Resistance" + d;
    }
};
Torchlight.t2.effects.KnockbackResistance.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
