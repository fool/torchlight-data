
Torchlight.t2.skills.outlander.sigil.ShadowlingBrute = function ShadowlingBrute() {
    this.name = "shadowlingbrute";
    this.displayName = "Shadowling Brute";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "sigil";
    this.skillLevelRequiredTier = 6;
    this.manaCost = [0, 10, 11, 12, 13, 14, 16, 18, 20, 22, 25, 28, 31, 34, 38, 42];
    var cooldown = 30;
    var duration = 10;
    this.cooldown = [cooldown];
    this.duration = [duration];
    this.tierBonuses = [
        "Brute gains dash attack",
        "Brute gains leap attack",
        "Brute attack rate is increased 30%"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration", duration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "All Damage", calculateAllDamage),
        new Torchlight.t2.skills.Attribute(this, "Minion Damage", calculateMinionDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var allDamage = calculateAllDamage(skillLevel);
        var minionDamage = calculateMinionDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.DamagePercent(allDamage, "All"),
            new Torchlight.t2.effects.MinionsDealDamage(minionDamage, "Physical")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAllDamage(skillLevel) {
        return 0 + (5 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateMinionDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(Math.max(1.05 - (0.05 * skillLevel), 0));
    }

};
Torchlight.t2.skills.outlander.sigil.ShadowlingBrute.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
