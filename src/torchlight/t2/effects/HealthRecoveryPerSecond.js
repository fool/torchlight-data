/**
 * @constructor
 */
Torchlight.t2.effects.HealthRecoveryPerSecond = function HealthRecoveryPerSecond(health) {

    this.toString = function toString() {
        return health + " Health recovery per second";
    }
};
Torchlight.t2.effects.HealthRecoveryPerSecond.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
