/**
 * @param {number} period
 * @param {number} radius
 * @constructor
 */
Torchlight.t2.effects.HealingPulse = function HealingPulse(period, radius) {
    this.toString = function toString() {
        return "Healing Pulse every " + period + " seconds; radius " + radius + " meters";
    }
};
Torchlight.t2.effects.HealingPulse.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
