
Torchlight.t2.skills.embermage.storm.ShockingOrb = function ShockingOrb() {
    this.name = "shockingorb";
    this.displayName = "Shocking Orb";
    this.description = "You summon a slow moving eletrical orb, which discharges enemy into foes as it passes";
    this.tree = "storm";
    this.skillLevelRequiredTier = 6;
    this.manaCost = [0, 25, 26, 26, 27, 27, 28, 29, 31, 32, 34, 36, 38, 40, 42, 44];
    var cooldown = 0.8;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "3 Orbs Released",
        "Orbs ricochet",
        "Discharge rate increased 50%"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration", this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "Weapond DPS %", calculateWeaponDps)
    ];

    /**
     * http://i.imgur.com/waVnc9y.jpg
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damage = calculateWeaponDps(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(damage, "Electric")
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 40 + (4 * skillLevel);
    }
};
Torchlight.t2.skills.embermage.storm.ShockingOrb.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
