
Torchlight.t2.skills.berserker.hunter.Ravage = function Ravage() {
    this.name        = "ravage";
    this.displayName = "Ravage";
    this.description = "You execute a brutal series of slashes, inflicting three strikes on all foes within 4 meters. Enemy armor is weakened with each strike. Requires a melee weapon equipped in the right hand.";
    this.tree        = "hunter";
    this.skillLevelRequiredTier = 6;
    this.manaCost    = [0, 4, 41, 41, 42, 44, 46, 47, 49, 51, 55, 57, 60, 63, 68, 71];
    this.range       = [0, 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  5];
    this.tierBonuses = [
        "Adds a 25% chance to freeze foes for 6 seconds",
        "Adds a 25% chance to completely immobilise foes for 6 seconds",
        "Chance to immobilise foes is increased to 50% "
    ];
    var interruptChance = 8;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Interrupt Chance",         interruptChance),
        new Torchlight.t2.skills.Attribute(this, "Knockback Resistance",     interruptChance, 5),
        new Torchlight.t2.skills.Attribute(this, "Slow Resistance",          interruptChance, 5),
        new Torchlight.t2.skills.Attribute(this, "Mana",                     this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range",                    this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %",             calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Armor Reduction",          calculateArmorReduction),
        new Torchlight.t2.skills.Attribute(this, "Armor Reduction Duration", calculateArmorReductionDuration)

    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var armorReduction = calculateArmorReduction(skillLevel, playerLevel);
        var armorReductionDuration = calculateArmorReductionDuration(skillLevel);

        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.buffs.Interrupt(interruptChance),
            new Torchlight.t2.effects.ReducedArmor("All", armorReduction, armorReductionDuration)
        ];

        if (skillLevel >= 5) {
            effects.push(new Torchlight.t2.effects.KnockbackResistance(100, 1));
            effects.push(new Torchlight.t2.effects.SlowResistance(100, 1));
        }

        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 23.5 + (1.5 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateArmorReduction(skillLevel, playerLevel) {
        return 10.5 - (0.5 * (skillLevel + (2 * playerLevel)));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateArmorReductionDuration(skillLevel) {
        var duration = 4;
        if (skillLevel >= 10) {
            duration = 6;
        }
        return duration;
    }
};
Torchlight.t2.skills.berserker.hunter.Ravage.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
