
Torchlight.t2.skills.embermage.storm.PrimasticBolt = function PrimasticBolt() {
    this.name = "primasticbolt";
    this.displayName = "Primastic Bolt";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "storm";
    this.skillLevelRequiredTier = 0;
    this.manaRate = "second";
    this.manaCost = [0, 10, 11, 12, 13, 14, 16, 18, 20, 22, 25, 28, 31, 34, 38, 42];
    this.tierBonuses = [
        "Damage increased by 10%",
        "Damage increased by 20%",
        "Damage increased by 30%"
    ];
    var effectDuration = 3;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Effect Duration", effectDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Damage", calculateDamage)
    ];

    /**
     * http://i.imgur.com/eT4o7.png
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damage = new Torchlight.lib.Range(calculateDamage(skillLevel));
        var effectChance = calculateEffectChance(skillLevel);
        return [
            new Torchlight.t2.effects.Damage(damage, "Fire"),
            new Torchlight.t2.effects.Damage(damage, "Ice"),
            new Torchlight.t2.effects.Damage(damage, "Electric"),
            new Torchlight.t2.effects.Damage(damage, "Poison"),
            new Torchlight.t2.effects.Poison(effectChance, effectDuration),
            new Torchlight.t2.effects.spells.Burn(effectChance, effectDuration),
            new Torchlight.t2.effects.Shock(effectChance, effectDuration),
            new Torchlight.t2.effects.spells.Freeze(effectChance, effectDuration)
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamage(skillLevel) {
        return 9 + (10 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateEffectChance(skillLevel) {
        return 9 + (1 * skillLevel);
    }
};
Torchlight.t2.skills.embermage.storm.PrimasticBolt.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
