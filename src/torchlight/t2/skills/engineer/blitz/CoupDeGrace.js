
Torchlight.t2.skills.engineer.blitz.CoupDeGrace = function CoupDeGrace() {
    this.name = "coupdegrace";
    this.displayName = "Coup De Grace";
    this.description = "When you strike a stunned target, an electrical surge helps finish the foe off. This can be done a maximum of once per second";
    this.tree = "blitz";
    this.passive = true;
    this.skillLevelRequiredTier = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Strength Multiplier", calculateDamageMultiplier)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var strengthMultiplier = calculateDamageMultiplier(skillLevel);
        return [
            new Torchlight.t2.effects.DamageEqualToStat("Strength", strengthMultiplier)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamageMultiplier(skillLevel) {
        return 2.5 + (0.5 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.blitz.CoupDeGrace.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
