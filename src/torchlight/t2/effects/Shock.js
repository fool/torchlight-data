/**
 * @param {number} chance
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.Shock = function Armor(chance, duration) {
    this.toString = function toString() {
        return chance + "% chance to Shock" + this.durationText(duration);
    }
};
Torchlight.t2.effects.Shock.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
