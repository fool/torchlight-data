/**
 * An attribute is a value, usually numeric, in part of a skill or skill's effect.
 *
 * Examples: Mana Cost, Range, Arc, Duration
 *
 *
 * @param {Torchlight.t2.skills.*}  skill         a skill instance
 * @param {string}                      name          The attribute name
 * @param {*}                       value         The value*
 * @param {number}                      [unlockAt]    Unlock At
 * @constructor
 */
Torchlight.t2.skills.Attribute = function Attribute(skill, name, value, unlockAt) {
    var isConstant = typeof value !== 'function';
    if (typeof unlockAt  !== "number") {
        unlockAt = -1;
    }

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    this.getValue = function getV(skillLevel, playerLevel) {
        return getValue(skillLevel, playerLevel);
    };

    /**
     * Computes the value of this attribute.
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {string}
     */
    function getValue(skillLevel, playerLevel) {
        var valueNow = -1;
        if (skillLevel >= unlockAt) {
            if (isConstant) {
                valueNow = value;
            } else {
                valueNow = value.call(skill, skillLevel, playerLevel); /* use call() to make 'this' the skill instance */
            }
        }
        return Torchlight.lib.toFixed(valueNow, 2);
    }

    this.getSkill = function getSkill() {
        return skill;
    };

    this.getName = function getName() {
        return name;
    };

    this.isConstant = isConstant;
    this.isAvailable = isAvailable;

    /**
     * @param   {number} skillLevel
     * @returns {boolean}
     */
    function isAvailable(skillLevel) {
        return skillLevel >= unlockAt;
    }

    function getLeveledValues(playerLevel) {
        var values = [];
        for (var i = Torchlight.t2.skills.Skill.minLevel + 1; i <= Torchlight.t2.skills.Skill.maxLevel; i++) {
            values.push(getValue(i, playerLevel));
        }
        return values;
    }

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {{name: string, playerLevel: *, constant: boolean, value: number}}
     */
    this.toObject = function toObject(skillLevel, playerLevel) {
        var data = {
            name: name,
            playerLevel: playerLevel,
            constant: isConstant,
            value: getValue(skillLevel, playerLevel),
            available: isAvailable(skillLevel)
        };
        if (!isConstant) {
            data.leveledValues = getLeveledValues(playerLevel);
        }
        if (unlockAt > 0) {
            data.unlockAt = unlockAt;
        }
        return data;
    };
};
