/**
 * @constructor
 */
Torchlight.t2.effects.SummonDuration = function SummonDuration(duration) {

    this.toString = function toString() {
        return duration + " seconds summon duration";
    }
};
Torchlight.t2.effects.SummonDuration.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
