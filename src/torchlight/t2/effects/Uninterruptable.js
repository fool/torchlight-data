/**
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.Uninterruptable = function Uninterruptable(duration) {
    this.toString = function toString() {
        return "Uninterruptable" + this.durationText(duration);
    }
};
Torchlight.t2.effects.Uninterruptable.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
