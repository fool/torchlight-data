/**
 * @param   {number} duration
 * @constructor
 */
Torchlight.t2.effects.Ignite = function Ignite(duration) {
    this.toString = function toString() {
        return "Targets are ignited" + this.durationText(duration);
    }
};
Torchlight.t2.effects.Ignite.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
