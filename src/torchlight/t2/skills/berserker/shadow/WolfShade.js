
Torchlight.t2.skills.berserker.shadow.WolfShade = function WolfShade() {
    this.name        = "wolfshade";
    this.displayName = "Wolf Shade";
    this.description = "You summon a Wolf Shade to sink icy fangs into your enemies and heal you with their lifeforce. The Shade can be summoned once every minute.";
    this.tree        = "shadow";
    this.skillLevelRequiredTier = 1;
    this.manaCost    = [0, 30, 31, 32, 33, 35, 38, 42, 46, 52, 58, 65, 71, 79, 87, 96];
    this.duration    = [0, 15, 15, 15, 15, 30, 30, 30, 30, 30, 45, 45, 45, 45, 45, 60];
    var cooldown     = 60;
    this.cooldown    = [cooldown];
    this.tierBonuses = [
        "Summoning duration increased to 30 seconds.",
        "Summoning duration increased to 45 seconds.",
        "Summoning duration increased to 60 seconds."
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown",       cooldown),
        new Torchlight.t2.skills.Attribute(this, "Mana",           this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Duration",       this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "% All Damage",   calculateAllDamage),
        new Torchlight.t2.skills.Attribute(this, "Health Stolen",  calculateHealthStolen),
        new Torchlight.t2.skills.Attribute(this, "Minions Damage", calculateMinionsDamage)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var percentAllDamage = calculateAllDamage(skillLevel);
        var healthStolen = calculateHealthStolen(skillLevel);
        var minionsDamage = calculateMinionsDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.DamagePercent(percentAllDamage, "All"),
            new Torchlight.t2.effects.HealthStolenToMasterPercent(healthStolen),
            new Torchlight.t2.effects.MinionsDealDamage(new Torchlight.lib.Range(minionsDamage), "Physical")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAllDamage(skillLevel) {
        return 0 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateHealthStolen(skillLevel) {
        return 3 + (2 * skillLevel);
    }

    /**
     * @fixme
     * @param skillLevel
     * @param playerLevel
     * @returns {number}
     */
    function calculateMinionsDamage(skillLevel, playerLevel) {
        return 500;
    }
};
Torchlight.t2.skills.berserker.shadow.WolfShade.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
