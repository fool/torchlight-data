/**
 * @param {number} rate
 * @constructor
 */
Torchlight.t2.effects.HexFireRate = function HexFireRate(rate) {
    this.toString = function toString() {
        return "Hex can fire every " + rate + " seconds";
    }
};
Torchlight.t2.effects.HexFireRate.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
