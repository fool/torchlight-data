
Torchlight.t2.skills.berserker.hunter.Eviscerate = function Eviscerate() {
    this.name        = "eviscerate";
    this.displayName = "Eviscerate";
    this.description = "You tear into all enemies directly in front of you, leaving them bleeding for 6 seconds. Base range is 2.7 meters within a 120 degree arc. Eviscerate attacks gain half of normal Charge. Requires a melee weapon equipped in the right hand.";
    this.tree        = "hunter";
    this.manaCost    = [0,  11,  12,  13,  14,  15,  17,  19,  22,  24,  27,  31,  34,  37,  42,  46];
    this.duration    = [0,   6,   6,   6,   6,   4,   4,   4,   4,   4,   2,   2,   2,   2,   2,   2];
    this.range       = [0, 2.7, 2.7, 2.7, 2.7,   3,   3,   3,   3,   3, 3.3, 3.3, 3.3, 3.3, 3.3,   4];
    this.arc         = [0, 120, 120, 120, 120, 180, 180, 180, 180, 180, 240, 240, 240, 240, 240, 240];
    this.tierBonuses = [
        "Range increased to 3 meters and arc to 180 degrees. Bleeding is amplified over a shorter, 4 second duration",
        "Range increased to 3.3 meters and arc to 240 degrees. Bleeding is amplified over a shorter, 2 second duration",
        "Range increased to 4 meters "
    ];
    this.skillLevelRequiredTier = 0;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Mana",           this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range",          this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Arc",            this.getArc),
        new Torchlight.t2.skills.Attribute(this, "Bleed Duration", this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "DPS %",          calculateWeaponDpsPercent),
        new Torchlight.t2.skills.Attribute(this, "Bleed Damage",   calculateBleedDamage)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDpsPercent(skillLevel);
        var bleedDamage = calculateBleedDamage(skillLevel, playerLevel);
        var bleedDuration = this.getDuration(skillLevel, playerLevel);

        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.DamageOverTime(bleedDamage, "Physical", bleedDuration)
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number} damage
     }
     */
    function calculateBleedDamage(skillLevel, playerLevel) {


//  1 12
//  2 18
//  3 24
//  6 42
// 10 68
// 14 108
// 19 172
// 25 272
// 32 424
// 40 654
// 49 1000
// 58 1548
// 68 2117
// 79 3060
// 92 4508


        var damage = 0;
        switch (skillLevel) {
            case skillLevel <= 1: /* playerLevel =  2*/
                damage = 18;
                break;
            case skillLevel <= 2: /* playerLevel =  3*/
                damage = 24;
                break;
            case skillLevel <= 3: /* playerLevel =  6*/
                damage = 42;
                break;
            case skillLevel <= 4: /* playerLevel = 10*/
                damage = 68;
                break;
            case skillLevel <= 5: /* playerLevel = 14*/
                damage = 108;
                break;
            case skillLevel <= 6: /* playerLevel = 19*/
                damage = 172;
                break;
            case skillLevel <= 7: /* playerLevel = 25*/
                damage = 272;
                break;
            case skillLevel <= 8: /* playerLevel = 32*/
                damage = 424;
                break;
            case skillLevel <= 9: /* playerLevel = 40*/
                damage = 654;
                break;
            case skillLevel <= 10: /* playerLevel = 49*/
                damage = 1000;
                break;
            case skillLevel <= 11: /* playerLevel = 58*/
                damage = 1548;
                break;
            case skillLevel <= 12: /* playerLevel = 68*/
                damage = 2117;
                break;
            case skillLevel <= 13: /* playerLevel = 79*/
                damage = 3060;
                break;
            case skillLevel <= 14: /* playerLevel = 92*/
                damage = 4508;
                break;
            case skillLevel >= 15: /* playerLevel = ??*/
                damage = 6000;
                break;
        }
        return damage;
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDpsPercent(skillLevel) {
        return 85 + (skillLevel * 3);
    }
};
Torchlight.t2.skills.berserker.hunter.Eviscerate.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
