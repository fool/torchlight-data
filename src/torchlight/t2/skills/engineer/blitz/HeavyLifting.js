
Torchlight.t2.skills.engineer.blitz.HeavyLifting = function HeavyLifting() {
    this.name = "heavylifting";
    this.displayName = "Heavy Lifting";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "blitz";
    this.passive = true;
    this.skillLevelRequiredTier = 0;
    var stunDuration = 3;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", stunDuration),
        new Torchlight.t2.skills.Attribute(this, "Attack Speed", calculateAttackSpeed),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance", calculateStunChance)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var attackSpeed = calculateAttackSpeed(skillLevel);
        var stunChance = calculateStunChance(skillLevel);
        return [
            new Torchlight.t2.effects.debuffs.SpeedDecrease(attackSpeed, Torchlight.t2.effects.Effect.speeds.Attack),
            new Torchlight.t2.effects.Stun(stunChance, stunDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAttackSpeed(skillLevel) {
        return 0 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunChance(skillLevel) {
        return 0 + (2 * skillLevel);
    }

};
Torchlight.t2.skills.engineer.blitz.HeavyLifting.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
