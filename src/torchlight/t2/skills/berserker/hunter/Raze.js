
Torchlight.t2.skills.berserker.hunter.Raze = function Raze() {
    this.manaCost = [0, 16, 16, 17, 17, 18, 19, 20, 22, 24, 27, 30, 33, 36, 39, 43];
    this.skillLevelRequiredTier = 2;
    this.name = "raze";
    this.displayName = "Raze";
    this.description = "You perform a devastating uppercut that inflicts heavy damage on a single target. With each successive hit, you grow in power. Raze does not generate Charge.";
    this.tree = "hunter";
    this.tierBonuses = [
        "Target is slowed",
        "Target has decreased attack accuracy",
        "Target is stunned"
    ];
    var duration          = 4;
    this.duration         = [duration];
    var allDamageDuration = 2;
    var interruptChance   = 100;
    var reduction         = -15;
    var chanceToMiss      = 25;
    var stunChance        = 65;
    var movementSpeedReduction = reduction;
    var attackSpeedReduction = reduction;
    var castSpeedReduction = reduction;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Duration",                       duration),
        new Torchlight.t2.skills.Attribute(this, "All Damage Reduction",           allDamageDuration),
        new Torchlight.t2.skills.Attribute(this, "Interrupt Chance",               interruptChance,           5),
        new Torchlight.t2.skills.Attribute(this, "Movement/Attack/Cast reduction", reduction,                 5),
        new Torchlight.t2.skills.Attribute(this, "Chance to Miss",                 chanceToMiss,              10),
        new Torchlight.t2.skills.Attribute(this, "Stun %",                         stunChance,                15),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS%",                    calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "All Damage %",                   calculateAllDamagePercent)
    ];

    /**
     * @param {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var allDamagePercent = calculateAllDamagePercent(skillLevel);

        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.buffs.Interrupt(interruptChance),
            new Torchlight.t2.effects.DamagePercent("All", allDamagePercent, allDamageDuration)
        ];

        if (skillLevel >= 5) {
            effects.push(new Torchlight.t2.effects.debuffs.SpeedDecrease(movementSpeedReduction, Torchlight.t2.effects.Effect.speeds.Movement, duration));
            effects.push(new Torchlight.t2.effects.debuffs.SpeedDecrease(attackSpeedReduction, Torchlight.t2.effects.Effect.speeds.Attack, duration));
            effects.push(new Torchlight.t2.effects.debuffs.SpeedDecrease(castSpeedReduction, Torchlight.t2.effects.Effect.speeds.Cast, duration));
        }

        if (skillLevel >= 10) {
            effects.push(new Torchlight.t2.effects.spells.Miss(chanceToMiss, duration));
        }

        if (skillLevel >= 15) {
            effects.push(new Torchlight.t2.effects.Stun(stunChance, duration));
        }

        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 135 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAllDamagePercent(skillLevel) {
        return 0 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.hunter.Raze.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
