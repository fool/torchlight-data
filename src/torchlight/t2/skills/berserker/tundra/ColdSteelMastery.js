
Torchlight.t2.skills.berserker.tundra.ColdSteelMastery = function ColdSteelMastery() {
    this.name = "coldsteelmastery";
    this.displayName = "Cold Steel Mastery";
    this.description = "The only thing harder than steel is cold steel.";
    this.tree = "tundra";
    this.passive = true;
    this.skillLevelRequiredTier = 0;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Physical Damage", calculatePhysicalDamage),
        new Torchlight.t2.skills.Attribute(this, "Ice Damage",      calculateIceDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var physicalDamage = calculatePhysicalDamage(skillLevel);
        var iceDamage = calculateIceDamage(skillLevel);

        return [
            new Torchlight.t2.effects.AdditionalDamagePercent(physicalDamage, "Physical"),
            new Torchlight.t2.effects.AdditionalDamagePercent(iceDamage, "Ice")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePhysicalDamage(skillLevel) {
        return 0 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateIceDamage(skillLevel) {
        return 0 + (6 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.tundra.ColdSteelMastery.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
