Torchlight.t2.effects.DamageEqualToWeaponDps = function DamageEqualToWeaponDps(percent, damageType) {
    if (damageType) {
        damageType = " as " + damageType + " damage";
    } else {
        damageType = "";
    }

    this.toString = function toString() {
        return "Damage is equal to " + percent + "% of weapon's DPS" + damageType;
    }
};
Torchlight.t2.effects.DamageEqualToWeaponDps.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
