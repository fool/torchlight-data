
Torchlight.t2.effects.ReflectMissiles = function ReflectMissiles(percent, weaponDps) {
    this.toString = function toString() {
        return percent + "% chance to reflect missiles at " + weaponDps + "% weapon DPS";
    }
};
Torchlight.t2.effects.ReflectMissiles.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
