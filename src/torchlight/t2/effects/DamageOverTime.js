
Torchlight.t2.effects.DamageOverTime = function DamageOverTime(damage, damageType, duration) {
    var d = "";
    if (duration) {
        d = "over " + duration + " seconds";
    }
    this.toString = function toString() {
        return "Inflicts " + damage + " " + damageType + " damage" + d;
    }
};
Torchlight.t2.effects.DamageOverTime.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
