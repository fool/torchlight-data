
Torchlight.t2.skills.berserker.shadow.ShredArmor = function ShredArmor() {
    this.name = "shredarmor";
    this.displayName = "Shred Armor";
    this.description = "Your inner savagery rips into your enemies' armor, leaving them more vulnerable with each hit, while at the same time stealing some of their armor for yourself. Only affects melee attacks.";
    this.tree = "shadow";
    this.passive = true;
    this.skillLevelRequiredTier = 1;
    var leechDuration = 3;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Leech Duration", leechDuration),
        new Torchlight.t2.skills.Attribute(this, "Armor Stolen",   calculateArmor)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var armor = calculateArmor(skillLevel, playerLevel);

        return [
            new Torchlight.t2.effects.CastSpell(100, "Leech Armor", "strike"),
            new Torchlight.t2.effects.Armor(armor, "Physical", leechDuration)
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateArmor(skillLevel, playerLevel) {
        return 2.9 + (1 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.shadow.ShredArmor.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
