
Torchlight.t2.skills.outlander.lore.BrambleWall = function BrambleWall() {
    this.name = "bramblewall";
    this.displayName = "Bramble Wall";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "lore";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 29, 29, 29, 30, 30, 32, 33, 35, 37, 40, 44, 49, 51, 57, 61];
    this.duration = [0, 10, 13, 16, 19, 20];
    var cooldown = 2.5;
    this.cooldown = [cooldown];
    var knockback = 45;
    var poisonGasDuration = 3;
    this.tierBonuses = [
        "Vines damage foes when raised",
        "Vine length increased",
        "Vines exude a poison gas"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Knockback", knockback),
        new Torchlight.t2.skills.Attribute(this, "Poison Gas Duration", poisonGasDuration, 15),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Duration", this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "Poison Damage", calculatePoisonDamage, 5),
        new Torchlight.t2.skills.Attribute(this, "Poison Gas", calculatePoisonGasDamage, 15)
    ];

    /**
     * http://i.imgur.com/eT4o7.png
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var effects = [
            new Torchlight.t2.effects.Knockback(knockback)
        ];

        if (skillLevel >= 5) {
            var poisonDamage = calculatePoisonDamage(skillLevel, playerLevel);
            effects.push(new Torchlight.t2.effects.Damage(poisonDamage, "Poison"));
        }

        if (skillLevel >= 15) {
            var poisonGasDamage = calculatePoisonGasDamage(skillLevel, playerLevel);
            effects.push(new Torchlight.t2.effects.PoisonGas(new Torchlight.lib.Range(poisonGasDamage), poisonGasDuration));
        }

        return effects;
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculatePoisonDamage(skillLevel, playerLevel) {
        var skill = (skillLevel / 3);
        var player = (playerLevel / 20);
        var lower = 300 + Math.pow(skill + 0.5, player); // 5k to 7.7k on lvl 15 / 100... ??
        var upper = 350 + Math.pow(skill + 1,   player);
        return new Torchlight.lib.Range(lower, upper);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculatePoisonGasDamage(skillLevel, playerLevel) {
        var skill  = (skillLevel / 3);
        var player = (playerLevel / 20);
        return 3000 + Math.pow(skill - 0.5, player);
    }
};
Torchlight.t2.skills.outlander.lore.BrambleWall.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
