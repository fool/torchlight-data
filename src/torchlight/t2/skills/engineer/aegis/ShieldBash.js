
Torchlight.t2.skills.engineer.aegis.ShieldBash = function ShieldBash() {
    this.name = "shieldbash";
    this.displayName = "ShieldBash";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "aegis";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 5, 6, 7, 8, 8, 9, 10, 11, 12, 13, 15, 16, 19, 20, 23];
    this.tierBonuses = [
        "Damage is increased to 7.5 times your Shield's Armor",
        "Damage is increased to 10 times your Shield's Armor",
        "Damage is increased to 12.5 times your Shield's Armor"
    ];
    var knockback = 25;
    var attackSpeedDuration = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Knockback", knockback),
        new Torchlight.t2.skills.Attribute(this, "attackSpeedDuration", attackSpeedDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Damage Multiplier", calculateDamageMultiplier),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance", calculateStunChance),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", calculateStunDuration),
        new Torchlight.t2.skills.Attribute(this, "Attack Speed Reduction", calculateAttackSpeedReduction)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damageMultiplier     = calculateDamageMultiplier(skillLevel);
        var attackSpeedReduction = calculateAttackSpeedReduction(skillLevel);
        var stunChance           = calculateStunChance(skillLevel);
        var stunDuration         = calculateStunDuration(skillLevel);
        return [
            new Torchlight.t2.effects.DamageShieldArmorMultiplier(damageMultiplier),
            new Torchlight.t2.effects.Stun(stunChance, stunDuration),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(attackSpeedReduction, Torchlight.t2.effects.Effect.speeds.Attack, attackSpeedDuration),
            new Torchlight.t2.effects.Knockback(knockback)
        ];
    };


    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamageMultiplier(skillLevel) {
        return Torchlight.lib.squeeze([5, 7.5, 10, 12.5], skillLevel / 5);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunChance(skillLevel) {
        return 58 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunDuration(skillLevel) {
        return 0.8 + (0.2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAttackSpeedReduction(skillLevel) {
        return -1 * (18 + (2 * skillLevel));
    }

};
Torchlight.t2.skills.engineer.aegis.ShieldBash.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
