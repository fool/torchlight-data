
Torchlight.t2.skills.engineer.aegis.DynamoField = function DynamoField() {
    this.name = "dynamofield";
    this.displayName = "DynamoField";
    this.description = "The Engineer taps into their armor's power core to project an electrical blast in a five-meter radius. This blast generates 0.2 Charge for every enemy caught in it, up to a maximum of five.";
    this.tree = "aegis";
    this.skillLevelRequiredTier = 3;
    this.manaCost = [0, 10, 11, 12, 13, 13, 15, 16, 17, 19, 20, 22, 23, 24, 25, 27];
    this.range    = [0,  5,  5,  5,  5,  7,  7,  7,  7,  7,  9,  9,  9,  9,  9, 11];
    var cooldown = 0.6;
    this.cooldown = [cooldown];
    this.duration = [0];
    this.tierBonuses = [
        "Field radius increased to 7 meters. Interrupt chance increased to 70%",
        "Field radius increased to 9 meters. Interrupt chance increased to 90%",
        "Field radius increased to 11 meters. Interrupt chance increased to 110%"
    ];
    var electricDamageDuration = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", electricDamageDuration),
        new Torchlight.t2.skills.Attribute(this, "Electric Damage Duration", electricDamageDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Electric Damage", calculateElectricDamage),
        new Torchlight.t2.skills.Attribute(this, "Charges Generated", calculateChargesGenerated),
        new Torchlight.t2.skills.Attribute(this, "Interrupt Chance", calculateInterruptChance)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel)
    {
        var chargesGenerated = calculateChargesGenerated(skillLevel);
        var electricDamage = new Torchlight.lib.Range(calculateElectricDamage(skillLevel, playerLevel));
        var interruptChance = calculateInterruptChance(skillLevel);
        return [
            new Torchlight.t2.effects.GenerateCharges(chargesGenerated),
            new Torchlight.t2.effects.Damage(electricDamage, "Electric", electricDamageDuration),
            new Torchlight.t2.effects.buffs.Interrupt(interruptChance)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChargesGenerated(skillLevel) {
        return 0.18 + (0.02 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateElectricDamage(skillLevel, playerLevel) {
        return 500;
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateInterruptChance(skillLevel) {
        return Torchlight.lib.squeeze([50, 70, 90, 110], skillLevel / 5);
    }
};
Torchlight.t2.skills.engineer.aegis.DynamoField.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
