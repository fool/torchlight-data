/**
 * This is the raw weapon item damage and not any special DPS value.
 * @param {number} percent
 * @param {string} damageType
 * @constructor
 */
Torchlight.t2.effects.PercentOfWeaponDamage = function PercentOfWeaponDamage(percent, damageType) {
    this.toString = function toString() {
        return "Inflicts " + percent + "% of weapon damage " + damageType + " damage";
    }
};
Torchlight.t2.effects.PercentOfWeaponDamage.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
