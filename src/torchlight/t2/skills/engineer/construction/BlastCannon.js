
Torchlight.t2.skills.engineer.construction.BlastCannon = function BlastCannon() {
    this.name = "blastcannon";
    this.displayName = "Blast Cannon";
    this.description = "You fire a long-range, piercing projectile from your cannon. At higher ranks, the blast leaves foes more susceptible to further fire and physical damage";
    this.tree = "construction";
    this.skillLevelRequiredTier = 1;
    this.manaCost = [0, 9, 10, 11, 12, 13, 14, 15, 17, 19, 21, 23, 25, 28, 31, 34];
    this.tierBonuses = [
        "25% chance to Blind foes for 4 seconds",
        "50% chance to Blind foes for 4 seconds",
        "75% chance to Blind foes for 4 seconds"
    ];
    var damageTakenDuration = 4;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Damage Taken Duration", damageTakenDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Damage Taken %", calculateDamageTaken),
        new Torchlight.t2.skills.Attribute(this, "Blind Chance", calculateBlindChance, 5)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var damageTaken = calculateDamageTaken(skillLevel);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.DamageTakenPercent(damageTaken, "Physical", damageTakenDuration),
            new Torchlight.t2.effects.DamageTakenPercent(damageTaken, "Fire", damageTakenDuration)
        ];
        if (skillLevel >= 5) {
            var blindChance = calculateBlindChance(skillLevel);
            effects.push(new Torchlight.t2.effects.Blindness(blindChance, 100, damageTakenDuration))
        }

        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 118 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamageTaken(skillLevel) {
        return -2 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBlindChance(skillLevel) {
        return Torchlight.lib.squeeze([0, 25, 50, 75], skillLevel / 5);
    }

};
Torchlight.t2.skills.engineer.construction.BlastCannon.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
