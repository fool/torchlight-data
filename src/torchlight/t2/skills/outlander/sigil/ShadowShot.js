
Torchlight.t2.skills.outlander.sigil.ShadowShot = function ShadowShot() {
    this.name = "shadowshot";
    this.displayName = "Shadow Shot";
    this.description = "You curse your ammunition, causing it to spilt into 3 shards upon impact, which seek additional targets";
    this.tree = "sigil";
    this.skillLevelRequiredTier = 1;
    this.manaCost = [0, 11, 11, 11, 12, 12, 13, 14, 15, 17, 19, 22, 24, 26, 29, 31];
    this.tierBonuses = [
        "Ammo splits into 4 shards",
        "Ammo splits into 5 shards",
        "Ammo splits into 6 shards"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Primary Damage", calculatePrimaryDamage),
        new Torchlight.t2.skills.Attribute(this, "Secondary Damage", calculateSecondaryDamage),
        new Torchlight.t2.skills.Attribute(this, "Shards", calculateShards)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var primaryDamage = calculatePrimaryDamage(skillLevel);
        var secondaryDamage = calculateSecondaryDamage(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(primaryDamage),
            new Torchlight.t2.effects.PercentOfWeaponDps(secondaryDamage)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePrimaryDamage(skillLevel) {
        return 72 + (4 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateSecondaryDamage(skillLevel) {
        return 74 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateShards(skillLevel) {
        return Torchlight.lib.squeeze([3, 4, 5, 6], skillLevel / 5);
    }

};
Torchlight.t2.skills.outlander.sigil.ShadowShot.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
