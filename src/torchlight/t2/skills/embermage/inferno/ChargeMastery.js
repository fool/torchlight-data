
Torchlight.t2.skills.embermage.inferno.ChargeMastery = function ChargeMastery() {
    this.name = "chargemastery";
    this.displayName = "Charge Mastery";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "inferno";
    this.skillLevelRequiredTier = 0;
    this.passive = true;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Charge decary rate reduction", calculateChargeDecayRateReduction),
        new Torchlight.t2.skills.Attribute(this, "Charge rate increase", calculateChargeRateIncrease)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var chargeRateDecayReduction = calculateChargeDecayRateReduction(skillLevel);
        var chargeRageIncrease = calculateChargeRateIncrease(skillLevel);
        return [
            new Torchlight.t2.effects.ChargeRate(chargeRageIncrease),
            new Torchlight.t2.effects.ChargeDecay(chargeRateDecayReduction)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChargeDecayRateReduction(skillLevel) {
        return 0 - (6 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChargeRateIncrease(skillLevel) {
        return 0 + (4 * skillLevel);
    }
};
Torchlight.t2.skills.embermage.inferno.ChargeMastery.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
