/**
 * Frost Breath for Berserker
 */
Torchlight.t2.skills.berserker.tundra.FrostBreath = function FrostBreath() {
    this.name = "frostbreath";
    this.displayName = "Frost Breath";
    this.description = "You channel the frigid north winds to unleash a blast of icy breath, which immobilizes enemies and makes them more susceptible to damage. The blast reaches 5 meters in a 90 degree arc";
    this.tree = "tundra";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0,  12,  13,  14,  16,  17,  19,  22,  24,  26,  30,  34,  37,  41,  46,  50];
    this.range    = [0,   5,   5,   5,   5,   6,   6,   6,   6,   6,   7,   7,   7,   7,   7,   8];
    this.arc      = [0,  90,  90,  90,  90, 120, 120, 120, 120, 120, 150];
    this.tierBonuses = [
        "Range is increased to 6 meters at a 120 degree angle",
        "Range is increased to 7 meters at a 150 degree angle",
        "Range is increased to 8 meters at a 150 degree angle"
    ];
    var weaponDpsPercent = 15;
    var freezeDuration = 4;
    var immobilizeChance = 80;
    var immobilizeDuration = 4;
    var allDamageTakenDuration = 4;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %",        weaponDpsPercent),
        new Torchlight.t2.skills.Attribute(this, "Freeze Duration",     freezeDuration),
        new Torchlight.t2.skills.Attribute(this, "Immobilize Chancee",  immobilizeChance),
        new Torchlight.t2.skills.Attribute(this, "Immobilize Duration", immobilizeDuration),
        new Torchlight.t2.skills.Attribute(this, "Manae",               this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Freeze Chance",       calculateFreezeChance),
        new Torchlight.t2.skills.Attribute(this, "All Damage Taken %",  calculateAllDamageTaken)
    ];

    /**
     *
     * @param   {number} playerLevel
     * @param   {number} skillLevel
     * @returns {}
     */
    this.getEffects = function getEffects(playerLevel, skillLevel) {
        var freezeChance = calculateFreezeChance(skillLevel);
        var allDamageTakenPercent = calculateAllDamageTaken(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDpsPercent, "Ice"),
            new Torchlight.t2.effects.spells.Freeze(freezeChance, freezeDuration),
            new Torchlight.t2.effects.Immobilize(immobilizeChance, immobilizeDuration),
            new Torchlight.t2.effects.DamageTakenPercent(allDamageTakenPercent, "All", allDamageTakenDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number} The percent chance (0 to 100) of freezing
     */
    function calculateFreezeChance(skillLevel) {
        return 18 + (2 * skillLevel)
    }

    /**
     *
     * @param   {number} skillLevel
     * @returns {number} The percent of damage added as an int (ex: 37% extra damage would have value 37);
     */
    function calculateAllDamageTaken(skillLevel) {
        return 18 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.tundra.FrostBreath.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
