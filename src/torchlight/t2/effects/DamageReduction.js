/**
 *
 * @param {number} damageReduction
 * @param {string} damageType
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.DamageReduction = function Armor(damageReduction, damageType, duration) {
    this.toString = function toString() {
        return damageType + " Damage is reduced by " + damageReduction + "%" + this.durationText(duration);
    }
};
Torchlight.t2.effects.DamageReduction.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
