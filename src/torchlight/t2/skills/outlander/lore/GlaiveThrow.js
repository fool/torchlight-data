
Torchlight.t2.skills.outlander.lore.GlaiveThrow = function GlaiveThrow() {
    this.name = "glaivethrow";
    this.displayName = "Glaive Throw";
    this.description = "You hurl a throwing glaive, slicing through your target, rebounding once to strike additional foes, and generating extra Charge";
    this.tree = "lore";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 10, 11, 12, 13, 14, 16, 18, 20, 22, 25, 28, 31, 34, 38, 42];
    this.tierBonuses = [
        "Glaive rebounds 2 times",
        "Glaive rebounds 3 times",
        "Glaive rebounds 4 times"
    ];
    var movementSpeedReductionDuration = 2;
    var interruptChance = 80;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Movement Speed Reduction Duration", movementSpeedReductionDuration),
        new Torchlight.t2.skills.Attribute(this, "Interrupt", interruptChance),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Poison Damage", calculatePoisonDamage),
        new Torchlight.t2.skills.Attribute(this, "Movement Speed Reduction", calculateMovementSpeedReduction),
        new Torchlight.t2.skills.Attribute(this, "Charge Generated", calculateChargeGenerated),
        new Torchlight.t2.skills.Attribute(this, "Glaive Rebounds", calculateGlaiveRebounds)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var poisonDamage = calculatePoisonDamage(skillLevel, playerLevel);
        var movementSpeedReduction = calculateMovementSpeedReduction(skillLevel);
        var chargeGenerated = calculateChargeGenerated(skillLevel);
        return [
            new Torchlight.t2.effects.Damage(poisonDamage, "Poison"),
            new Torchlight.t2.effects.buffs.Interrupt(interruptChance),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(movementSpeedReduction, Torchlight.t2.effects.Effect.speeds.Movement, movementSpeedReductionDuration),
            new Torchlight.t2.effects.spells.ChargeRefill(chargeGenerated)
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculatePoisonDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(28 + (2 * skillLevel));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateMovementSpeedReduction(skillLevel) {
        return -1 * (23 + (2 * skillLevel));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChargeGenerated(skillLevel) {
        return 1.8 + (0.2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateGlaiveRebounds(skillLevel) {
        return Torchlight.lib.squeeze([1, 2, 3, 4], skillLevel / 5);
    }
};
Torchlight.t2.skills.outlander.lore.GlaiveThrow.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
