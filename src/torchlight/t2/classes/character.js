Torchlight.t2.classes.character = {

    /**
     * @param   {string} name
     * @param   {object} skillTree
     * @returns {Torchlight.t2.skills.Skill|undefined}
     */
    getSkillFromTree: function getSkill(name, skillTree) {
        var allSkills = [];
        for (var key in skillTree) {
            if (skillTree.hasOwnProperty(key)) {
                allSkills = allSkills.concat(skillTree[key]);
            }
        }

        for (key in allSkills) {
            if (allSkills.hasOwnProperty(key)) {
                var skill = allSkills[key];
                if (skill.name === name) {
                    return skill;
                }
            }
        }
        return undefined;
    }
};
