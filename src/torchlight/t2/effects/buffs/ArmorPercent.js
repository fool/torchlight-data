/**
 * @param {number} percent
 * @param {string} armorType
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.buffs.ArmorPercent = function ArmorPercent(percent, armorType, duration) {
    this.percent = percent;
    this.armorType = armorType;
    this.duration = duration;
    var me = this;

    this.toString = function toString() {
        return me.percent + "% to " + me.armorType + " armor" + me.durationText(duration);
    };

    /**
     * @param   {Torchlight.t2.effects.Effect} effect
     * @returns {Torchlight.t2.effects.buffs.ArmorPercent|null}
     */
    this.combine = function combine(effect) {
        var result = null;
        if (effect instanceof Torchlight.t2.effects.buffs.ArmorPercent) {
            if (me.armorType === effect.armorType) {
                var combinedPercent = this.percent + effect.percent;
                var combinedDuration = (this.duration + effect.duration) || undefined;
                result = new Torchlight.t2.effects.buffs.ArmorPercent(combinedPercent, me.armorType, combinedDuration);
            }
        }
        return result;
    };
};
Torchlight.t2.effects.buffs.ArmorPercent.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
