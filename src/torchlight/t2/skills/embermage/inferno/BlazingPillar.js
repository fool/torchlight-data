
Torchlight.t2.skills.embermage.inferno.BlazingPillar = function BlazingPillar() {
    this.name = "blazingpillar";
    this.displayName = "Blazing Pillar";
    this.description = "You create giant pillars of intense flame that seek out and immolate nearby foes. Blazing Pillars is good at building Charge.";
    this.tree = "inferno";
    this.skillLevelRequiredTier = 3;
    this.manaCost = [0, 18,  19, 19, 19,  20,  21, 22,  24,  26,  28,   31,  34, 37,  40,  43];
    this.cooldown = [0,  9, 8.8, 8.5, 8.3,  8, 7.8, 7.5, 7.3,  7, 6.8, 6.5, 6.3,  6, 5.8, 5.5];
    this.tierBonuses = [
        "5 pillars of flame are created",
        "6 pillars of flame are created",
        "7 pillars of flame are created"
    ];
    var burnDuration = 7;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Burn Duration", burnDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage", calculateFireDamage),
        new Torchlight.t2.skills.Attribute(this, "Chance To Burn", calculateChanceToBurn),
        new Torchlight.t2.skills.Attribute(this, "Number of Pillars", calculateNumberOfPillars)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var fireDamage = calculateFireDamage(skillLevel, playerLevel);
        var chanceToBurn = calculateChanceToBurn(skillLevel);
        return [
            new Torchlight.t2.effects.Damage(fireDamage, "Fire"),
            new Torchlight.t2.effects.spells.Burn(chanceToBurn, burnDuration)
        ];
    };

    /**
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateFireDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range((5 * skillLevel) + (10 * playerLevel));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToBurn(skillLevel) {
        return -5 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateNumberOfPillars(skillLevel) {
        return Torchlight.lib.squeeze([4, 5, 6, 7], skillLevel / 5);
    }
};
Torchlight.t2.skills.embermage.inferno.BlazingPillar.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);

