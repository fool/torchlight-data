
Torchlight.t2.skills.engineer.aegis.AegisOfFate = function AegisOfFate() {
    this.name = "aegisoffate";
    this.displayName = "Aegis Of Fate";
    this.description = "When foes attack you, your armor has a chance to generate a defensive \"bubble\" around you that prevents further damage. The amount of damage this shield can absorb is equal to 200% of your Armor plus 100. While Aegis is active, you also get 50% Knockback Resistance.";
    this.tree = "aegis";
    this.passive = true;
    this.skillLevelRequiredTier = 1;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Chance to cast", calculateChanceToCast)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var chanceToCast = calculateChanceToCast(skillLevel);
        return [
            new Torchlight.t2.effects.CastSpell(chanceToCast, "Aegis", "when struck")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToCast(skillLevel) {
        return 1 + (1 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.aegis.AegisOfFate.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
