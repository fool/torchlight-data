
Torchlight.t2.skills.engineer.blitz.StormBurst = function StormBurst() {
    this.name = "stormburst";
    this.displayName = "Storm Burst";
    this.description = "A blast from your pack rockets you swiftly forward, knocking back foes. 3 bolts of energy discharge from your suit on impact, striking remote enemies. Every enemy hit also recharges your mana by 5%, for up to 5 targets";
    this.tree = "blitz";
    this.skillLevelRequiredTier = 5;
    this.manaCost  = [0, 25, 25, 26, 26, 27, 27, 29, 30, 32, 34, 37, 40, 43, 45, 48];
    this.cooldown  = [0,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  1];
    this.tierBonuses = [
        "Increase bolt discharge to 6",
        "20% chance to immobilize foes",
        "Cooldown reduced to 1 second"
    ];
    var knockback = [0, 55, 56, 57, 58, 60, 62, 65, 68, 72, 79, 86, 94, 101, 109, 117];
    var immobilizationChance = 20;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Immobilization Chance", immobilizationChance, 10),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "Weapon Dps", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Electric Damage", calculateElectricDamage),
        new Torchlight.t2.skills.Attribute(this, "Bolt Discharged", calculateBoltDischarge),
        new Torchlight.t2.skills.Attribute(this, "Knockback", calculateKnockback)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var electricDamage = calculateElectricDamage(skillLevel);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.PercentOfWeaponDps(electricDamage, "Electric"),
            new Torchlight.t2.effects.Knockback(knockback)
        ];

        if (skillLevel >= 10) {
            effects.push(new Torchlight.t2.effects.Immobilize(immobilizationChance));
        }

        return effects;
    };


    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 56 + (4 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateElectricDamage(skillLevel) {
        return 32 + (4 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBoltDischarge(skillLevel) {
        return Torchlight.lib.squeeze([3, 6], skillLevel / 5);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateKnockback(skillLevel) {
        return Torchlight.lib.squeeze(knockback, skillLevel);
    }
};
Torchlight.t2.skills.engineer.blitz.StormBurst.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
