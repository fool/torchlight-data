
Torchlight.t2.skills.embermage.frost.ElementalBoon = function ElementalBoon() {
    this.name = "elementalboon";
    this.displayName = "Elemental Boon";
    this.description = "You imbue yourseld with a bolstering aura which increases your element damages and resistances. Boon's benefits are also applied to any allies within 12 meters.";
    this.tree = "frost";
    this.skillLevelRequiredTier = 3;
    this.manaCost = [0, 18, 19, 20, 21, 22, 24, 26, 28];
    var cooldown = 30;
    this.cooldown = [cooldown];
    var duration = 15;
    var manaRecoveryDuration = 10;
    this.duration = [duration];
    this.tierBonuses = [
        "Gains resistance to slowing and immobilization effects",
        "Gains mana recharge",
        "Faster cast and less mana"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration", duration),
        new Torchlight.t2.skills.Attribute(this, "Mana Recovery Duration", manaRecoveryDuration, 10),
        new Torchlight.t2.skills.Attribute(this, "Damage Reduction", calculateDamageReduction),
        new Torchlight.t2.skills.Attribute(this, "Damage Add %", calculateDamageAddPercent),
        new Torchlight.t2.skills.Attribute(this, "Slow Resistance", calculateSlowResistance, 5),
        new Torchlight.t2.skills.Attribute(this, "Immobilization Resistance", calculateSlowResistance, 5),
        new Torchlight.t2.skills.Attribute(this, "Mana Recovery", calculateManaRecovery, 10)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damageReduction = calculateDamageReduction(skillLevel);
        var damageAdd = calculateDamageAddPercent(skillLevel);
        var effects = [
            new Torchlight.t2.effects.DamagePercent(damageReduction, "Poison",   duration),
            new Torchlight.t2.effects.DamagePercent(damageReduction, "Electric", duration),
            new Torchlight.t2.effects.DamagePercent(damageReduction, "Ice",      duration),
            new Torchlight.t2.effects.DamagePercent(damageReduction, "Fire",     duration),
            new Torchlight.t2.effects.DamagePercent(damageAdd,       "Poison",   duration),
            new Torchlight.t2.effects.DamagePercent(damageAdd,       "Electric", duration),
            new Torchlight.t2.effects.DamagePercent(damageAdd,       "Ice",      duration),
            new Torchlight.t2.effects.DamagePercent(damageAdd,       "Fire",     duration)
        ];

        if (effects >= 5) {
            var slowResistance = calculateSlowResistance(skillLevel, playerLevel);
            effects.push(new Torchlight.t2.effects.Resistance(slowResistance, "Slow",duration));
            effects.push(new Torchlight.t2.effects.Resistance(slowResistance, "Immobilization", duration));
        }
        if (effects >= 10) {
            var manaRecovery = calculateManaRecovery(skillLevel, playerLevel);
            effects.push(new Torchlight.t2.effects.buffs.Recover(manaRecovery, Torchlight.t2.effects.Effect.recover.Mana, manaRecoveryDuration);
        }
        return effects;
    };

     /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamageReduction(skillLevel) {
        return -1 * (8 * (2 * skillLevel));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamageAddPercent(skillLevel) {
        return -1 * calculateDamageReduction(skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateSlowResistance(skillLevel, playerLevel) {
        return 42;
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateManaRecovery(skillLevel, playerLevel) {
        return 42;
    }


};
Torchlight.t2.skills.embermage.frost.ElementalBoon.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
