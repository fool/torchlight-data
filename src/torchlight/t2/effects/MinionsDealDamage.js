/**
 *
 * @param {Torchlight.lib.Range} damageRange How much damage they deal
 * @param {string} damageType                What kind of damage
 * @param {number} [duration]                Optional - over how many seconds
 * @constructor
 */
Torchlight.t2.effects.MinionsDealDamage = function MinionsDealDamage(damageRange, damageType, duration) {
    this.toString = function toString() {
        return "Minions deal " + damageRange.toString() + " " + damageType + this.durationText(duration) + " - Improves with Player Level";
    }
};
Torchlight.t2.effects.MinionsDealDamage.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);

