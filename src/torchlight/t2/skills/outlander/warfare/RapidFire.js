
Torchlight.t2.skills.outlander.warfare.RapidFire = function RapidFire() {
    this.name = "rapidfire";
    this.displayName = "Rapid Fire";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "warfare";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 12, 13, 14, 15, 17, 19, 21, 23, 26, 29, 33, 37, 41, 46, 51];
    this.getRange = function getRange(skillLevel) {
        if (skillLevel < 10) {
            return 7;
        }
        return 12;
    };

    this.tierBonuses = [
        "Errant shots are added every other shot",
        "Range extended to 12 meters",
        "Targets are ignited for 4 seconds"
    ];
    var armorReductionDuration = 8;
    var igniteDuration = 8;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Armor Reduction Duration", armorReductionDuration),
        new Torchlight.t2.skills.Attribute(this, "Ignite Duration", igniteDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Knockback", calculateKnockback)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var knockback = calculateKnockback(skillLevel);
        var allArmorReduction = calculateAllArmorReduction(skillLevel, playerLevel);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.ReducedArmor("All", allArmorReduction, armorReductionDuration)
        ];

        if (skillLevel >= 15) {
            effects.push(new Torchlight.t2.effects.Ignite(igniteDuration));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 23 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateKnockback(skillLevel) {
        return 2.4 + (0.2 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateAllArmorReduction(skillLevel, playerLevel) {
        return 0 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.warfare.RapidFire.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
