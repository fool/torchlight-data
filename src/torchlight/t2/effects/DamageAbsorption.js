/**
 * @param {number} absorption
 * @param {string} damageType
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.DamageAbsorption = function DamageAbsorption(absorption, damageType, duration) {
    this.toString = function toString() {
        return absorption + " points of " + damageType + " Damage Absorption" + this.durationText(duration);
    }
};
Torchlight.t2.effects.DamageAbsorption.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
