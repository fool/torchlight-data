Torchlight.t2.effects.HealthStolenToMasterPercent = function HealthStolenToMasterPercent(percent, duration) {
    var dur = "";
    if (typeof duration === "number") {
        dur = " over " + duration + " seconds";
    }

    this.toString = function toString() {
        return percent + "% Health stolen to master" + dur;
    }
};
Torchlight.t2.effects.HealthStolenToMasterPercent.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);

