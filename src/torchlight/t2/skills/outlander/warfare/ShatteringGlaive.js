
Torchlight.t2.skills.outlander.warfare.ShatteringGlaive = function ShatteringGlaive() {
    this.name = "shatteringglaive";
    this.displayName = "Shattering Glaive";
    this.description = "You hurl a throwing glaive, which shatters on impact, damaging and slowing all foes within 4.5 meters";
    this.tree = "warfare";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 16, 16, 16, 16, 17, 17, 18, 18, 20, 21, 23, 25, 27, 28, 30];
    this.getRange = function getRange(skillLevel) {
        if (skillLevel < 10) {
            return 4.5;
        }
        return 6;
    };
    this.tierBonuses = [
        "Adds a 25% chance that targets will flee for 3 seconds",
        "Detonation size increased to 6 meters",
        "Fire trails emanate from the point of detonation"
    ];
    var chanceToFlee = 25;
    var fleeDuration = 3;
    var movementSpeedDuration = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Movement Speed Duration", movementSpeedDuration),
        new Torchlight.t2.skills.Attribute(this, "Chance to Flee", chanceToFlee, 5),
        new Torchlight.t2.skills.Attribute(this, "Flee Duration", fleeDuration, 5),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Primary Physical Damage", calculatePrimaryPhysicalDamage),
        new Torchlight.t2.skills.Attribute(this, "Secondary Physical Damage", calculateSecondaryPhysicalDamage),
        new Torchlight.t2.skills.Attribute(this, "Knockback", calculateKnockback),
        new Torchlight.t2.skills.Attribute(this, "Movement Speed Reduction", calculateMovementSpeedReduction)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var primaryPhysicalDamage = calculatePrimaryPhysicalDamage(skillLevel, playerLevel);
        var movementSpeedReduction = calculateMovementSpeedReduction(skillLevel);
        var secondaryPhysicalDamage = calculateSecondaryPhysicalDamage(skillLevel, playerLevel);
        var knockback = calculateKnockback(skillLevel);
        var effects = [
            new Torchlight.t2.effects.Damage(primaryPhysicalDamage, "Physical"),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(movementSpeedReduction, Torchlight.t2.effects.Effect.speeds.Movement, movementSpeedDuration),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.Damage(secondaryPhysicalDamage, "Physical")
        ];

        if (skillLevel >= 5) {
            effects.push(new Torchlight.t2.effects.spells.Flee(chanceToFlee, fleeDuration));
        }

        return effects;
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculatePrimaryPhysicalDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(0 + (2 * skillLevel));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateMovementSpeedReduction(skillLevel) {
        return -1 * (14 + (1 * skillLevel));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateKnockback(skillLevel) {
        return 3.5 + (0.5 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateSecondaryPhysicalDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(0 + (2 * skillLevel));
    }
};
Torchlight.t2.skills.outlander.warfare.ShatteringGlaive.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
