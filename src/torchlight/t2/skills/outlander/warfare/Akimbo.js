
Torchlight.t2.skills.outlander.warfare.Akimbo = function Akimbo() {
    this.name = "akimbo";
    this.displayName = "Akimbo";
    this.description = "Your skill with pistols enables you to deal more damage when wielding two at once and occasionally fire both simultaneously... to deadly effect";
    this.tree = "warfare";
    this.passive = true;
    this.skillLevelRequiredTier = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Chance", calculateChance)
    ];

    /**
     * @param   {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var chance = calculateChance(skillLevel);

        return [
            new Torchlight.t2.effects.DualWieldBonus(chance),
            new Torchlight.t2.effects.CastSpell(chance, "Execute")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChance(skillLevel) {
        return 0 + (2 * skillLevel);
    }

};
Torchlight.t2.skills.outlander.warfare.Akimbo.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
