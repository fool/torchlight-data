
Torchlight.t2.skills.berserker.hunter.Howl = function Howl() {
    this.name = "howl";
    this.displayName = "Howl";
    this.description = "You Howl in fury, filling nearby foes with terror. Affected foes are slowed and become less able to defend themselves for several seconds.";
    this.tree = "hunter";
    this.skillLevelRequiredTier = 1;
    var cooldown = 0.1;
    this.cooldown = [cooldown];
    this.manaCost = [0,   13,   14,   15,    16,   18,   19,  21,    23,   25,   29,   32,   34,   38,   43,   47];
    this.range =    [0,    5,    5,    5,     5,  7.5,  7.5,  7.5,  7.5,  7.5,   10,   10,   10,   10,   10, 12.5];
    this.duration = [0,    6,  6.5,    7,   7.5,    8,  8.5,    9,  9.5,   10, 10.5,   11, 11.5,   12, 12.5,   13];
    this.tierBonuses = [
        "Range increased to 7.5 meters",
        "Range increased to 10 meters",
        "Range increased to 12.5 meters"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown",                cooldown),
        new Torchlight.t2.skills.Attribute(this, "Mana",                    this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Duration",                this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "Range",                   this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Damge %",                 calculateDamage),
        new Torchlight.t2.skills.Attribute(this, "Move/Attack/Cast speed%", calculateSpeedReduction)
    ];


    /**
     * @param {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var duration       = this.getDuration(skillLevel);
        var damage         = calculateDamage(skillLevel);
        var speedReduction = calculateSpeedReduction(skillLevel);

        return [
            new Torchlight.t2.effects.debuffs.SpeedDecrease(speedReduction, Torchlight.t2.effects.Effect.speeds.Movement, duration),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(speedReduction, Torchlight.t2.effects.Effect.speeds.Attack, duration),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(speedReduction, Torchlight.t2.effects.Effect.speeds.Cast, duration),
            new Torchlight.t2.effects.DamageTakenPercent(damage,    "All", duration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamage(skillLevel) {
        return 13 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateSpeedReduction(skillLevel) {
        return -1 * calculateDamage(skillLevel);
    }
};
Torchlight.t2.skills.berserker.hunter.Howl.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
