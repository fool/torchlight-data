/**
 * @param {number} charges
 * @constructor
 */
Torchlight.t2.effects.GenerateCharges = function GenerateCharges(charges) {
    this.toString = function toString() {
        return "Each unit hit generates " + charges + " charges";
    }
};
Torchlight.t2.effects.GenerateCharges.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
