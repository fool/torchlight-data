
Torchlight.t2.skills.engineer.construction.Bulwark = function Bulwark() {
    this.name = "bulwark";
    this.displayName = "Bulwark";
    this.description = "Your expertise with armor lets you get the most out of it, increasing its effectiveness, and reducing any damage that actually gets through";
    this.tree = "construction";
    this.passive = true;
    this.skillLevelRequiredTier = 0;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Physical Armor", calculatePhysicalArmor),
        new Torchlight.t2.skills.Attribute(this, "Physical Damage Reduction", calculatePhysicalDamageReduction)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var physicalArmor = calculatePhysicalArmor(skillLevel);
        var physicalDamageReduction = calculatePhysicalDamageReduction(skillLevel);
        return [
            new Torchlight.t2.effects.buffs.ArmorPercent(physicalArmor, "Physical"),
            new Torchlight.t2.effects.DamageReduction(physicalDamageReduction, "Physical")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePhysicalArmor(skillLevel) {
        return 0 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePhysicalDamageReduction(skillLevel) {
        return 1 + (1 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.construction.Bulwark.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
