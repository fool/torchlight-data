
Torchlight.t2.skills.berserker.tundra.GlacialShatter = function GlacialShatter() {
    this.name = "glacialshatter";
    this.displayName = "Glacial Shatter";
    this.description = "The earth is sundered and 5 geysers burst forth at the target location, blinding and damaging foes with superheated air and water.";
    this.tree = "tundra";
    this.skillLevelRequiredTier = 6;
    this.manaCost = [0, 25, 26, 26, 27, 27, 28, 29, 31, 32, 34, 36, 38, 40, 42, 44];
    var cooldown = 1;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "7 Geysers are created",
        "Enemies are ignited",
        "9 Geysers are created"
    ];
    var blindDuration = 5;
    var fireDuration = 3;
    var interrupt = 35;
    var knockback = 7;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown",          cooldown),
        new Torchlight.t2.skills.Attribute(this, "Blind Duration",    blindDuration),
        new Torchlight.t2.skills.Attribute(this, "Interrupt Chance",  interrupt),
        new Torchlight.t2.skills.Attribute(this, "Knockback",         knockback),
        new Torchlight.t2.skills.Attribute(this, "Fire Duration",     fireDuration, 10),
        new Torchlight.t2.skills.Attribute(this, "Blindness Chance",  calculateBlindnessChance),
        new Torchlight.t2.skills.Attribute(this, "Blindness %",       calculateBlindnessLevel),
        new Torchlight.t2.skills.Attribute(this, "Ice Damage",        calculateIceDamage),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage",       calculateFireDamage)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var blindChance = calculateBlindnessChance(skillLevel);
        var blindLevel  = calculateBlindnessLevel(skillLevel);
        var iceDamage   = calculateIceDamage(skillLevel, playerLevel);
        var fireDamage  = calculateFireDamage(skillLevel, playerLevel);

        var effects = [
            new Torchlight.t2.effects.Blindness(blindChance, blindLevel, blindDuration),
            new Torchlight.t2.effects.Damage(iceDamage, "Ice"),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.buffs.Interrupt(interrupt)
        ];

        if (skillLevel >= 10) {
            effects.push(new Torchlight.t2.effects.Damage(fireDamage, "Fire", fireDuration));
        }

        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBlindnessChance(skillLevel) {
        return 27 + (3 * skillLevel);
    }

    /**
     * @param   {nummber} skillLevel
     * @returns {number}
     */
    function calculateBlindnessLevel(skillLevel) {
        return 67 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateIceDamage(skillLevel, playerLevel) {
        var ranges = [
            [   0,    0],
            [ 149,  199],
            [ 166,  218],
            [ 185,  239],
            [ 213,  271],
            [ 243,  307],
            [ 288,  360],
            [ 339,  420],
            [ 411,  505],
            [ 494,  601],
            [ 608,  734],
            [ 740,  888],
            [ 920, 1097],
            [1132, 1341],
            [1414, 1667],
            [1748, 2049]
        ];

        var range = ranges[Torchlight.lib.squeeze(ranges, skillLevel)];
        return new Torchlight.lib.Range(range[0], range[1]);

    }

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateFireDamage(skillLevel, playerLevel) {
        var ranges = [
            [   0,    0],
            [   0,    0],
            [   0,    0],
            [   0,    0],
            [   0,    0],
            [   0,    0],
            [   0,    0],
            [   0,    0], /* the fire damage is a tier 2 bonus */
            [   0,    0], /* so everything before level 10 is all 0 */
            [   0,    0],
            [ 381,  762],
            [ 534,  978],
            [ 744, 1275],
            [1008, 1635],
            [1365, 2121],
            [1809, 2712]
        ];

        var range = ranges[Torchlight.lib.squeeze(ranges, skillLevel)];
        return new Torchlight.lib.Range(range[0], range[1]);
    }
};
Torchlight.t2.skills.berserker.tundra.GlacialShatter.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
