
Torchlight.t2.skills.berserker.tundra.NorthernRage = function NorthernRage() {
    this.name        = "northernrage";
    this.displayName = "Northern Rage";
    this.description = "You unleash an arctic blast from deep inside you, throwing out shards of ice to knock back and freeze nearby foes.";
    this.tree        = "tundra";
    this.skillLevelRequiredTier = 3;
    this.manaCost    = [0, 29, 30, 31, 31, 32, 34, 36, 38, 42, 45, 49, 54, 59, 65, 70];
    this.tierBonuses = [
        "The blast is strengthened to 4 fissures that travel further than the base skill",
        "The blast is strengthened to 5 fissures that travel further than tier I",
        "The blast is strengthened to 6 fissures that travel further than tier II"
    ];
    var weaponDps    = 20;
    var freezeChance = 80;
    var knockback    = 25;
    this.attributes  = [
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %",    weaponDps),
        new Torchlight.t2.skills.Attribute(this, "Freeze Chance",   freezeChance),
        new Torchlight.t2.skills.Attribute(this, "Knockback",       knockback),
        new Torchlight.t2.skills.Attribute(this, "Mana",            this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Ice Damage",      calculcateIceDamage),
        new Torchlight.t2.skills.Attribute(this, "Freeze Duration", calculateFreezeDuration),
        new Torchlight.t2.skills.Attribute(this, "Break Shields %", calculateShieldBreakChance)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var iceDamage         = calculcateIceDamage(skillLevel);
        var freezeDuration    = calculateFreezeDuration(skillLevel);
        var shieldBreakChance = calculateShieldBreakChance(skillLevel);

        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.Damage(new Torchlight.lib.Range(iceDamage), "Ice"),
            new Torchlight.t2.effects.spells.Freeze(freezeChance, freezeDuration),
            new Torchlight.t2.effects.spells.BreakShields(shieldBreakChance),
            new Torchlight.t2.effects.Knockback(knockback)
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculcateIceDamage(skillLevel) {
        return 5 + (10 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFreezeDuration(skillLevel) {
        return 2.5 + (0.5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateShieldBreakChance(skillLevel) {
        return 13 + (2 * skillLevel);

    }
};
Torchlight.t2.skills.berserker.tundra.NorthernRage.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
