/**
 * @param {Torchlight.lib.Range} range
 * @param {Number}              [duration]
 * @constructor
 */
Torchlight.t2.effects.PoisonGas = function PoisonGas(range, duration) {
    this.toString = function toString() {
        return range.toString() + " Poison Gas" + this.durationText(duration);
    }
};
Torchlight.t2.effects.PoisonGas.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);