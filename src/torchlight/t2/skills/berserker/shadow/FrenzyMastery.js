
Torchlight.t2.skills.berserker.shadow.FrenzyMastery = function FrenzyMastery() {
    this.name = "frenzymastery";
    this.displayName = "Frenzy Mastery";
    this.description = "Your feral nature grows more powerful and harder to suppress, increasing the duration of your Frenzy states";
    this.tree = "shadow";
    this.passive = true;
    this.skillLevelRequiredTier = 0;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Frenzy Duration Increase", calculateFrenzyDuration)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var frenzyDuration = calculateFrenzyDuration(skillLevel);

        return [
            new Torchlight.t2.effects.FrenzyDuration(frenzyDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFrenzyDuration(skillLevel) {
        return 5 + (1.5 * skillLevel);
    }

};
Torchlight.t2.skills.berserker.shadow.FrenzyMastery.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
