/**
 * There's a 5-6% chance this class sucks
 *
 * @param {number} lower
 * @param {number} [upper]
 * @constructor
 */
Torchlight.lib.Range = function Range(lower, upper) {
    if (typeof lower === "number") {
        this.lower = lower;
    } else {
        throw "Invalid range parameters";
    }

    if (typeof upper === "number") {
        this.upper = upper;
    } else {
        this.upper = lower;
    }

    /**
     * @returns {string}
     */
    this.toString = function toString() {
        if (this.lower === this.upper) {
            return "" + this.lower;
        }
        return this.lower + "-" + this.upper;
    }
};
