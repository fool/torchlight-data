/**
 *
 * @param {number} damageTaken
 * @param {string} damageType
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.DamageTakenPercent = function Armor(damageTaken, damageType, duration) {
    this.toString = function toString() {
        return damageType + " Damage taken is increased by " + damageTaken + "%" + this.durationText(duration);
    }
};
Torchlight.t2.effects.DamageTakenPercent.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
