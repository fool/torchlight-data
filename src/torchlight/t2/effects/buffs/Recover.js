/**
 * @param {number} value
 * @param {string} type
 * @param duration
 * @constructor
 */
Torchlight.t2.effects.buffs.Recover = function Recover(value, type, duration) {
    this.recoverType = Torchlight.t2.effects.Effect.validateRecoverType(type, Torchlight.t2.effects.Effect.recover.Health);
    this.value = value;
    this.duration = duration;

    this.toString = function toString() {
        var sign = "";
        if (value > 0) {
            sign = "+";
        }
        return sign + this.value + " " + this.recoverType + "  recovery" + this.durationText(duration);
    };


    this.combine = function combine(effect) {
        var result = null;
        if (effect instanceof Torchlight.t2.effects.buffs.Recover) {
            if (effect.recoverType === this.recoverType) {
                var combinedValue = this.value + effect.value;
                var combinedDuration = this.duration + effect.duration;
                result = new Torchlight.t2.effects.buffs.Recover(combinedValue, this.recoverType, combinedDuration);
            }
        }
        return result;
    };
};
Torchlight.t2.effects.buffs.Recover.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
