/**
 * @param {number} percent
 * @param {number} duration
 * @constructor
 */
Torchlight.t2.effects.DamageReflection = function DamageReflection(percent, duration) {
    this.toString = function toString() {
        return percent + "% Damage reflected back on attacker" + this.durationText(duration);
    }
};
Torchlight.t2.effects.DamageReflection.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
