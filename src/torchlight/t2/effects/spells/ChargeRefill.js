/**
 *
 * @param {number} percent   How much to refill
 * @param {number} [chance]  How likely is this to happen (Default = always)
 * @constructor
 */
Torchlight.t2.effects.spells.ChargeRefill = function ChargeRefill(percent, chance) {
    this.percent = percent;
    if (typeof chance !== "number" || chance >= 100) {
        chance = 100;
    }
    if (chance < 0) {
        chance = 0;
    }

    /**
     * guaranteed (initially) to be between 0 and 100
     * @type {number}
     */
    this.chance = chance;


    this.toString = function toString() {
        var fill = "Fill ";
        if (chance < 100) {
            fill = chance + "% chance to fill ";
        }
        return fill + percent + "% of your Charge Bar";
    };

    this.combine = function combine(effect) {
        var result = null;
        if (effect instanceof Torchlight.t2.effects.spells.ChargeRefill) {
            var combinedPercent = Math.min(this.percent + effect.percent, 100);
            var combinedChance = this.chance + effect.chance;
            result = new Torchlight.t2.effects.spells.ChargeRefill(combinedPercent, combinedChance);
        }
        return result;
    };
};
Torchlight.t2.effects.spells.ChargeRefill.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
