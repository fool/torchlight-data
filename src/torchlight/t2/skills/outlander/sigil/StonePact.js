
Torchlight.t2.skills.outlander.sigil.StonePact = function StonePact() {
    this.name = "stonepact";
    this.displayName = "Stone Pact";
    this.description = "Invoke an ancient curse that protects you and your allies from physical and elemental damage and reflects some damage back on your attackers.";
    this.tree = "sigil";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 21, 21, 21, 22, 22, 23, 24, 25, 27, 29, 32, 35, 37, 41, 44];
    var cooldown = 8;
    var duration = 20;
    this.cooldown = [cooldown];
    this.duration = [duration];
    this.tierBonuses = [
        "The pact reflects 25% of damage back at attacking monsters",
        "The pact reflects 50% of damage back at attacking monsters",
        "The pact reflects 75% of damage back at attacking monsters"
    ];
    var effectDuration = 3;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration", duration),
        new Torchlight.t2.skills.Attribute(this, "Effect Duration", effectDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "+All Armor", calculateAllArmor),
        new Torchlight.t2.skills.Attribute(this, "Health Recovered", calculateHealthRecovered),
        new Torchlight.t2.skills.Attribute(this, "Damage Reflected", calculateReflectedDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var allArmor        = calculateAllArmor(skillLevel);
        var healthRecovered = calculateHealthRecovered(skillLevel, playerLevel);

        var effects = [
            new Torchlight.t2.effects.buffs.ArmorPercent(allArmor, "All", effectDuration),
            new Torchlight.t2.effects.buffs.Recover(healthRecovered, Torchlight.t2.effects.Effect.recover.Health, effectDuration)
        ];

        if (skillLevel >= 5) {
            var damageReflected = calculateReflectedDamage(skillLevel);
            effects.push(new Torchlight.t2.effects.DamageReflection(damageReflected, effectDuration));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAllArmor(skillLevel) {
        return 14 + (2 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateHealthRecovered(skillLevel, playerLevel) {
        return 74 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateReflectedDamage(skillLevel) {
        return Torchlight.lib.squeeze([0, 25, 50, 75], skillLevel / 5);
    }

};
Torchlight.t2.skills.outlander.sigil.StonePact.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
