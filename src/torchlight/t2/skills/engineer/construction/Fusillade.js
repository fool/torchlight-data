
Torchlight.t2.skills.engineer.construction.Fusillade = function Fusillade() {
    this.name = "fusillade";
    this.displayName = "Fusillade";
    this.description = "You unleash a continuous barrage of long-range homing rockets from your cannon. Bursts of 2 rockets keep firing as long as the mouse button is held down";
    this.tree = "construction";
    this.skillLevelRequiredTier = 5;
    this.manaCost = [0, 23, 23, 24, 24, 25, 25, 26, 27, 29, 31, 33, 36, 39, 41, 44];
    this.manaRate = "second";
    this.tierBonuses = [
        "3 rockets are released per burst",
        "Rockets detonate in a 3 meter burst",
        "4 rockets are released per burst"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Rockets", calculateRockets)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Fire")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 28 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateRockets(skillLevel) {
        return Torchlight.lib.squeeze([2, 3, 3, 4], skillLevel / 5);
    }
};
Torchlight.t2.skills.engineer.construction.Fusillade.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
