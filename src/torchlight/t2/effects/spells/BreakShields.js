
Torchlight.t2.effects.spells.BreakShields = function BreakShields(percent) {
    this.percent = percent;

    this.toString = function toString() {
        return percent + "% chance to break enemy shields";
    };

    this.combine = function combine(effect) {
        var result = null;
        if (effect instanceof Torchlight.t2.effects.spells.BreakShields) {
            result = new Torchlight.t2.effects.spells.BreakShields(Math.min(this.percent + effect.percent, 100));
        }
        return result;
    };
};
Torchlight.t2.effects.spells.BreakShields.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
