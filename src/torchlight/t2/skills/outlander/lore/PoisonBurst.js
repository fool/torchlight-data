
Torchlight.t2.skills.outlander.lore.PoisonBurst = function PoisonBurst() {
    this.name = "poisonburst";
    this.displayName = "Poison Burst";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "lore";
    this.passive = true;
    this.skillLevelRequiredTier = 1;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Chance To Explode", calculateChanceToExplode)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var chanceToExplode = calculateChanceToExplode(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Poison"),
            new Torchlight.t2.effects.ExplosionOnDeath(chanceToExplode)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 47 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToExplode(skillLevel) {
        return 8 + (4 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.lore.PoisonBurst.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
