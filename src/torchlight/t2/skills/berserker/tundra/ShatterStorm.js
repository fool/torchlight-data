
Torchlight.t2.skills.berserker.tundra.ShatterStorm = function ShatterStorm() {
    this.name = "shatterstorm";
    this.displayName = "ShatterStorm";
    this.description = "When you kill frozen enemies, you blast nearby foes in a 4-meter ring of frost, leaving them more susceptible to further ice attacks.";
    this.tree = "tundra";
    this.passive = true;
    this.skillLevelRequiredTier = 1;
    var freezeDuration = 10;
    var immobilizeChance = 100;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Freeze Duration", freezeDuration),
        new Torchlight.t2.skills.Attribute(this, "Immobilization Chance", immobilizeChance),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Immobilize Duration", calculateImmobilizeDuration),
        new Torchlight.t2.skills.Attribute(this, "Freeze Chance", calculateFreezeChance),
        new Torchlight.t2.skills.Attribute(this, "Ice Armor Reduction", calculateIceArmorReduction),
        new Torchlight.t2.skills.Attribute(this, "Armor Reduction Duration", calculateImmobilizeDuration)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {

        var immobilizeDuration = calculateImmobilizeDuration(skillLevel);
        var freezeChance = calculateFreezeChance(skillLevel);
        var iceArmorReduction = calculateIceArmorReduction(skillLevel);
        var iceArmorReductionDuration = immobilizeDuration;

        return [
            new Torchlight.t2.effects.Immobilize(immobilizeChance, immobilizeDuration),
            new Torchlight.t2.effects.spells.Freeze(freezeChance, freezeDuration),
            new Torchlight.t2.effects.DamageIceArmorPercent(iceArmorReduction, iceArmorReductionDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateIceArmorReduction(skillLevel) {
        return -1 * calculateFreezeChance(skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateImmobilizeDuration(skillLevel) {
        return 2.9 + (1 * skillLevel);
    }
    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFreezeChance(skillLevel) {
        return 0 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.tundra.ShatterStorm.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
