
Torchlight.t2.skills.berserker.tundra.StormHatchet = function StormHatchet() {
    this.name = "stormhatchet";
    this.displayName = "Storm Hatchet";
    this.description = "You draw wild northern lightning out of your weapon to create an electrical throwing axe with a range of 15 meters. Each foe struck generates an additional 5% of the Charge Bar.";
    this.tree = "tundra";
    this.skillLevelRequiredTier = 1;
    this.manaCost =         [0, 18, 19, 21, 22, 24, 25, 28, 31, 34, 39, 43, 46, 52, 58, 64];
    this.range = [0, 15, 15, 15, 15, 20];
    this.tierBonuses = [
        "Hatchet range is increased to 20 meters and it flies faster",
        "3 hatchets are thrown at once",
        "Hatchets shatter shields"
    ];
    var foeStruckChance = 100;
    var chargeRefillPercent = 5;
    var breakShieldsChance = 100;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Charge Refill Percent", chargeRefillPercent),
        new Torchlight.t2.skills.Attribute(this, "Breaks Shields", breakShieldsChance, 15),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDpsPercent)
    ];

    this.getEffects = function getEffects(playerLevel, skillLevel) {
        var chargeRefill = new Torchlight.t2.effects.spells.ChargeRefill(chargeRefillPercent);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(calculateWeaponDpsPercent(skillLevel), "Electric"),
            new Torchlight.t2.effects.FoeStruck(foeStruckChance, chargeRefill)
        ];

        if (skillLevel >= 10) {
            effects.push(new Torchlight.t2.effects.ThreeHatchets());
        }

        if (skillLevel >= 15) {
            effects.push(new Torchlight.t2.effects.spells.BreakShields(breakShieldsChance));
        }

        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDpsPercent(skillLevel) {
        return 82 + (skillLevel * 3);
    }
};
Torchlight.t2.skills.berserker.tundra.StormHatchet.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);

