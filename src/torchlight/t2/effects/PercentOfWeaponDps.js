Torchlight.t2.effects.PercentOfWeaponDps = function PercentOfWeaponDps(percent, damageType) {
    if (damageType) {
        damageType = " as " + damageType + " damage";
    } else {
        damageType = "";
    }

    this.toString = function toString() {
        return "Inflicts " + percent + "% of weapon DPS" + damageType;
    }
};
Torchlight.t2.effects.PercentOfWeaponDps.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
