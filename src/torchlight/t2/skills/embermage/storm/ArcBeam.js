
Torchlight.t2.skills.embermage.storm.ArcBeam = function ArcBeam() {
    this.name = "arcbeam";
    this.displayName = "Arc Beam";
    this.description = "You pierce your foes with a channled beam of energy from you weapons. Destroyed enemies split the beam and strike nearby targets as a secondary effect.";
    this.tree = "storm";
    this.manaRate = "second";
    this.skillLevelRequiredTier = 3;
    this.manaCost = [0, 17, 18, 20, 21, 23, 25, 29, 33, 40, 46, 51, 58, 66, 74, 81];
    this.tierBonuses = [
        "The beam can split up to three times",
        "The beam can split up to four times",
        "The beam can split up to five times"
    ];
    var knockback = 5;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Knockback", knockback),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS", calculateWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Electric Damage", calculateElectricDamage),
        new Torchlight.t2.skills.Attribute(this, "Split Beam Weapon DPS", calculateSplitBeamWeaponDps),
        new Torchlight.t2.skills.Attribute(this, "Beams", calculateBeams)
    ];

    /**
     * http://media.indiedb.com/images/articles/1/153/152734/auto/arcbeam_zpsfc6a4fb1.jpg
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var splitBeamWeaponDps = calculateSplitBeamWeaponDps(skillLevel);
        var electricDamage = calculateElectricDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.ExplosionOnDeath(),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.Damage(electricDamage, "Electric"),
            new Torchlight.t2.effects.PercentOfWeaponDps(splitBeamWeaponDps, "Electric")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBeams(skillLevel) {
        return Torchlight.lib.squeeze([2, 3, 4, 5], skillLevel / 5);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateElectricDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(100, 1000);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 9 + (1 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateSplitBeamWeaponDps(skillLevel) {
        return 38 + (2 * skillLevel);
    }
};
Torchlight.t2.skills.embermage.storm.ArcBeam.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
