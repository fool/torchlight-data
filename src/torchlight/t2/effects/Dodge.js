/**
 * @param percent
 * @param [duration]
 * @constructor
 */
Torchlight.t2.effects.Dodge = function Dodge(percent, duration) {
    this.toString = function toString() {
        return "+" + percent + "% Dodge chance " + this.durationText(duration);
    }
};
Torchlight.t2.effects.Dodge.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
