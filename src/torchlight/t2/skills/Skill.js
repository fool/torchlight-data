/**
 * All getters that return skill data must be prepared to accept three arguments in this order:
 *
 *    skillLevel, playerLevel, ??/
 *
 *    i forgot what the 3rd one is but someting outhere uses it.
 *
 *
 * @constructor
 */
Torchlight.t2.skills.Skill = {
    /**
     * @type {number[][]}
     */
    levelRequiredTiers: [
        [0,  1,  2,  3,  6, 10, 14, 19, 25, 32, 40, 49, 58, 68, 79, 92], /* 0 */
        [0,  7,  8,  9, 11, 13, 17, 22, 28, 35, 43, 52, 60, 70, 81, 93], /* 1 */
        [0, 14, 15, 16, 18, 20, 23, 27, 32, 38, 46, 55, 63, 72, 83, 94], /* 2 */
        [0, 21, 22, 23, 24, 26, 29, 33, 37, 43, 49, 58, 66, 75, 85, 95], /* 3 */
        [0, 28, 29, 30, 31, 33, 35, 38, 42, 46, 53, 62, 69, 77, 87, 96], /* 4 */
        [0, 35, 36, 37, 38, 40, 42, 45, 48, 52, 59, 66, 74, 81, 89, 97], /* 5 */
        [0, 42, 43, 44, 46, 48, 51, 54, 58, 62, 67, 72, 78, 84, 91, 98]  /* 6 */
    ],
    minLevel: 0,
    maxLevel: 15,

    /**
     * Forces a level to be between the min and the max range
     * @param   {number} level
     * @returns {number}
     */
    squeezeLevel: function(level) {
        return Math.max(Math.min(level, this.maxLevel), this.minLevel);
    }
};


/**
 * YOu want a way to get all the static properties and the dynamic properties.
 * you show the static ones on top and the bottom ones below in the table.
 */
Torchlight.t2.skills.Skill.prototype = {
    /**
     * This must be one word, all lower case, alpha letters only. This is for programmatic use such as class names in
     * HTML or file names on disk.
     * @type {string}
     */
    name: "Error: Missing skill name",

    /**
     * This can be whatever is needed to match the in-game display.
     * @type {string}
     */
    displayName: "Error: Missing display name",

    description: "Error: Missing skill description",

    passive: false,

    tree: "Error: missing skill tree",

    /**
     * Must be "cast" or "second". Refers to how mana is spent.
     *
     * @type {string}
     */
    manaRate: "cast",

    /**
     * The index in levelRequiredTiers
     * @type {number}
     */
    skillLevelRequiredTier: undefined,

    tierBonuses: [],

    cooldown: undefined,

    duration: undefined,

    manaCost: undefined,

    arc: undefined,

    range: undefined,

    /**
     * @type {Torchlight.t2.skills.Attribute[]}
     */
    attributes: [],

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    getCooldown: function getCooldown(skillLevel) {
        if (this.cooldown instanceof Array) {
            return Torchlight.lib.squeeze(this.cooldown, skillLevel);
        } else {
            throw new Torchlight.t2.skills.InvalidSkillException("Invalid Cooldown");
        }
    },

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    getDuration: function getDuration(skillLevel) {
        if (this.duration instanceof Array) {
            return Torchlight.lib.squeeze(this.duration, skillLevel);
        } else {
            throw new Torchlight.t2.skills.InvalidSkillException("Invalid Duration");
        }
    },

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    getManaCost: function getManaCost(skillLevel) {
        if (this.manaCost instanceof Array) {
            return Torchlight.lib.squeeze(this.manaCost, skillLevel);
        } else {
            throw new Torchlight.t2.skills.InvalidSkillException("Invalid Mana Cost");
        }
    },

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    getRange: function getRange(skillLevel) {
        if (this.range instanceof Array) {
            return Torchlight.lib.squeeze(this.range, skillLevel);
        } else {
            throw new Torchlight.t2.skills.InvalidSkillException("Invalid Range");
        }
    },

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    getArc: function getArc(skillLevel) {
        if (this.arc instanceof Array) {
            return Torchlight.lib.squeeze(this.arc, skillLevel);
        } else {
            throw new Torchlight.t2.skills.InvalidSkillException("Invalid Arc");
        }
    },

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    getLevelRequired: function getLevelRequired(skillLevel) {
        return Torchlight.lib.squeeze(Torchlight.t2.skills.Skill.levelRequiredTiers[this.skillLevelRequiredTier], skillLevel);
    },

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    getEffects: function getEffects(skillLevel, playerLevel) {
        throw new Torchlight.t2.skills.InvalidSkillException("getEffects must be defined");
    },

    /**
     * @returns {string[]}
     */
    getTierBonuses: function getTierBonuses() {
        return this.tierBonuses;
    }
};
