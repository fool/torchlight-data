/**
 * Static
 */
Torchlight.t2.effects.Effect = {
    target: {
        Self: 'self',
        Enemy: 'enemy',
        Friendly: 'friendly',
        Area: 'area'
    },
    when: {
        Passive: 'passive',
        Cast: 'cast',
        StrikingEnemy: 'striking enemy',
        EachEnemyInRange: 'each enemy in range'
    },

    speeds: {
        Movement: "movement",
        Cast: "cast",
        Attack: "attack"
    },
    speedTypes: [this.speeds.Movement, this.speeds.Cast, this.speeds.Attack],

    recover: {
        Health: "health",
        Mana: "mana",
        Charge: "charge"
    },
    recoverTypes: [this.recover.Health, this.recover.Mana, this.recover.Charge],

    /**
     *
     * @param   {string} input
     * @param   {string} defaultValue
     * @returns {string} The speed name or the default
     */
    validateSpeedName: function validateSpeedName(input, defaultValue) {
        var result = defaultValue;
        var index = this.speedTypes.indexOf(input);
        if (index !== -1) {
            result = this.speeds[this.speedTypes[index]];
        }
        return result;
    },

    /**
     *
     * @param   {string} input
     * @param   {string} defaultValue
     * @returns {string} The speed name or the default
     */
    validateRecoverType: function validateRecoverType(input, defaultValue) {
        var result = defaultValue;
        var index = this.recoverTypes.indexOf(input);
        if (index !== -1) {
            result = this.recover[this.recoverTypes[index]];
        }
        return result;
    }
};

/**
 * Inherited
 */
Torchlight.t2.effects.Effect.prototype = {
    /**
     * @type boolean
     */
    improvesWithPlayerLevel: false,

    /**
     * @type string
     */
    target: Torchlight.t2.effects.Effect.target.Self,

    /**
     * @type string
     */
    when: Torchlight.t2.effects.Effect.when.Passive,

    /**
     * @param   {number} duration
     * @returns {string}
     */
    durationText: function durationText(duration) {
        if (duration) {
            return "over " + duration + " seconds";
        }
        return "";
    },

    /**
     * @param   {number} percent
     * @returns {string}
     */
    affectedByPercent: function affectedByPercent(percent) {
        var p = "increased";
        if (percent < 0) {
            p = "reduced";
            percent = percent * -1;
        }
        return p + " by " + percent + "%";
    },

    /**
     * @returns {{string: string}}
     */
    toObject: function toObject() {
        return {
            string: this.toString()
        };
    },

    /**
     * @param   {Torchlight.t2.effects.Effect} effect
     * @returns {boolean}
     *
    equals: function equals(effect) {
        throw "Error: equals() not implemented";
    },*/

    /**
     * @returns {string}
     */
    toString: function toString() {
        throw "Error: I am missing my shit";
    },

    /**
     * @param   {Torchlight.t2.effects.Effect} effect
     * @returns {Torchlight.t2.effects.Effect|null} newEffect If the combination is not possible the result is undefined.
     */
    combine: function combine(effect) {
        throw "Error: combine() not implemented";
    }
};
