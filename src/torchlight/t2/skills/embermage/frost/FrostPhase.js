
Torchlight.t2.skills.embermage.frost.FrostPhase = function FrostPhase() {
    this.name = "frostphase";
    this.displayName = "Frost Phase";
    this.description = "You vanish from one location and instantly reappear in another, dealing cold damage in both places within a 2.5 meter burst.";
    this.tree = "frost";
    this.skillLevelRequiredTier = 2;
    this.manaCost = [0, 18, 36];
    var cooldown = 0.7;
    this.cooldown = [cooldown];
    var freezeDuration = 3;
    this.tierBonuses = [
        "Freeze radius increased to 3.75 meters",
        "Freeze radius increased to 5 meters",
        "Freeze radius increased to 6.25 meters"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Freeze Duration", freezeDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Ice Damage", calculateIceDamage),
        new Torchlight.t2.skills.Attribute(this, "Freeze Radius", calculateFreezeRadius),
        new Torchlight.t2.skills.Attribute(this, "Chance to Freeze", calculateChanceToFreeze),
        new Torchlight.t2.skills.Attribute(this, "Knockback", calculateKnockback)
    ];

    /**
     * http://i.imgur.com/eT4o7.png
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        return [];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFreezeRadius(skillLevel) {
        return Torchlight.lib.squeeze([2.5, 3.75, 5, 6.25], skillLevel / 5);
    }

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateIceDamage(skillLevel, playerLevel) {
        return 300;
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateKnockback(skillLevel) {
        return 13 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToFreeze(skillLevel) {
        return 5 + (5 * skillLevel);
    }

};
Torchlight.t2.skills.embermage.frost.FrostPhase.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
