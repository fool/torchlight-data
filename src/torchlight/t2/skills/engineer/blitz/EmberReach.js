
Torchlight.t2.skills.engineer.blitz.EmberReach = function EmberReach() {
    this.name = "emberreach";
    this.displayName = "Ember Reach";
    this.description = "A surge of magnetic power from the Engineer's armor draws foes into striking range. Additionally, each Charge the Engineer has built up adds a 20% chance to stun affected enemies for 3 seconds, but does not consume Charges in the process.";
    this.tree = "blitz";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 14, 14, 15, 15, 12, 12, 12, 13, 14, 12, 13, 14, 15, 16, 12];
    var cooldown  = 0.6;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "Mana cost reduced by 20%. Damage Taken effect lasts 4 seconds",
        "Mana cost reduced by 40%. Damage Taken effect lasts 6 seconds",
        "Mana cost reduced by 60%. Damage Taken effect lasts 8 seconds"
    ];
    this.getRange = function (skillLevel) {
        return 11.5 + (0.5 * skillLevel);
    };
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Physical Damage Taken %", calculatePhysicalDamageTakenIncrease),
        new Torchlight.t2.skills.Attribute(this, "Physical Damage Duration", calculatePhysicalDamageTakenIncreaseDuration)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damageTaken = calculatePhysicalDamageTakenIncrease(skillLevel);
        var damageDuration = calculatePhysicalDamageTakenIncreaseDuration(skillLevel);
        return [
            new Torchlight.t2.effects.DamageTakenPercent(damageTaken, "Physical", damageDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePhysicalDamageTakenIncrease(skillLevel) {
        return -4 + (4 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePhysicalDamageTakenIncreaseDuration(skillLevel) {
        return Torchlight.lib.squeeze([2, 4, 6, 8], skillLevel / 5);
    }
};
Torchlight.t2.skills.engineer.blitz.EmberReach.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
