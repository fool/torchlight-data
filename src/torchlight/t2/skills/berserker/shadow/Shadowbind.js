
Torchlight.t2.skills.berserker.shadow.Shadowbind = function Shadowbind() {
    this.name = "shadowbind";
    this.displayName = "Shadowbind";
    this.description = "You bind a group of foes within a 5 meter radius of your target location. All your subsequent melee attacks transfer a percentage of damage to all shadowbound targets. Requires melee weapons to trigger.";
    this.tree = "shadow";
    this.skillLevelRequiredTier = 2;
    this.manaCost = [0, 11, 11, 11, 12, 12, 12, 13, 13, 14, 16, 18, 20, 22, 24, 27];
    this.range    = [0,  5,  5,  5,  5,  7,  7,  7,  7,  7,  9,  9,  9,  9,  9, 11];
    var cooldown  = 5;
    this.cooldown = [cooldown];
    this.effects  = [
        "Radius of effect increased to 7 meters",
        "Radius of effect increased to 9 meters",
        "Radius of effect increased to 11 meters"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "% Damage Transferred", calculateTransferDamage),
        new Torchlight.t2.skills.Attribute(this, "Transferred Duration", calculateTransferDuration)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var percent = calculateTransferDamage(skillLevel);
        var duration = calculateTransferDuration(skillLevel);
        return [
            new Torchlight.t2.effects.TransferDamage(percent, duration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateTransferDamage(skillLevel) {
        return 13 + (2   * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateTransferDuration(skillLevel) {
        return 7.5 + (0.5 * skillLevel)
    }
};
Torchlight.t2.skills.berserker.shadow.Shadowbind.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
