
Torchlight.t2.skills.embermage.frost.IcyBlast = function IcyBlast() {
    this.name = "icyblast";
    this.displayName = "Icy Blast";
    this.description = "You unleash a hail of 5 ricocheting icy bolts to slow and immobilize your foes.";
    this.tree = "frost";
    this.skillLevelRequiredTier = 0;
    this.manaCost = [0, 11, 12, 13, 14, 15, 18, 20, 22, 24, 27, 31, 34, 37, 42, 46];
    var cooldown = 0.8;
    this.cooldown = [cooldown];
    var freezeDuration = 3;
    var enemyDamageReduction = 20;
    var enemyDamageReductionDuration = 6;

    this.duration = [0];
    this.tierBonuses = [
        "Target's damage is reduced by 20% for 6 seconds.",
        "Chance to immobilize is increased to 75% for 4 seconds.",
        "Increases to 7 bolts over a wider arc."
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Freeze Duration", freezeDuration),
        new Torchlight.t2.skills.Attribute(this, "Enemy Damage Reduction", enemyDamageReduction, 5),
        new Torchlight.t2.skills.Attribute(this, "Enemy Damage Reduction Duration", enemyDamageReductionDuration, 5),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDpsPercent),
        new Torchlight.t2.skills.Attribute(this, "Freeze Chance", calculateFreezeChance),
        new Torchlight.t2.skills.Attribute(this, "Immobilize Chance", calculateImmobilizeChance),
        new Torchlight.t2.skills.Attribute(this, "Immobilize Duration", calculateImmobilizeDuration),
        new Torchlight.t2.skills.Attribute(this, "Bolt Count", calculateBoltCount)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var iceDamage = calculateWeaponDpsPercent(skillLevel);
        var freezeChance = calculateFreezeChance(skillLevel);
        var immobilizeChance = calculateImmobilizeChance(skillLevel);
        var immobilizeDuration = calculateImmobilizeDuration(skillLevel);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(iceDamage, "Ice"),
            new Torchlight.t2.effects.spells.Freeze(freezeChance, freezeDuration),
            new Torchlight.t2.effects.Immobilize(immobilizeChance, immobilizeDuration)
        ];

        if (skillLevel >= 5) {
            effects.push(new Torchlight.t2.effects.EnemyDamageReduced(enemyDamageReduction, "Physical", enemyDamageReductionDuration));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDpsPercent(skillLevel) {
        return 28 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFreezeChance(skillLevel) {
        return 47 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateImmobilizeChance(skillLevel) {
        if (skillLevel < 10) {
            return 50;
        }
        return 75;
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateImmobilizeDuration(skillLevel) {
        if (skillLevel < 10) {
            return 3;
        }
        return 4;
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateBoltCount(skillLevel) {
        if (skillLevel < 15) {
            return 5;
        }
        return 7;
    }
};
Torchlight.t2.skills.embermage.frost.IcyBlast.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
