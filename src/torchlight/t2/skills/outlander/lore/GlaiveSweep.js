
Torchlight.t2.skills.outlander.lore.GlaiveSweep = function GlaiveSweep() {
    this.name = "glaivesweep";
    this.displayName = "Glaive Sweep";
    this.description = "You sweep your glaive in a 360 degree arc, knocking back and stunning foes within four meters, while generating additional Charge for each enemy struck";
    this.tree = "lore";
    this.skillLevelRequiredTier = 2;
    this.manaCost = [0, 12, 12, 13, 13, 14, 15, 16, 17, 19, 21, 24, 26, 28, 31, 34];
    this.getRange = function getRange(skillLevel) {
        return Torchlight.lib.squeeze([4, 4, 5.5], skillLevel / 5);
    };
    this.tierBonuses = [
        "Targets Bleed",
        "Range Increased to 5.5 meters",
        "Stun Chance increased to 95% "
    ];
    var knockback = 40;
    var interrupt = 50;
    var bleedDuration = 5;
    var stunDuration = 3;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Knockback", knockback),
        new Torchlight.t2.skills.Attribute(this, "Interrupt", interrupt),
        new Torchlight.t2.skills.Attribute(this, "Bleed Duration Effect Duration", bleedDuration),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", stunDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Dps", calculateDps),
        new Torchlight.t2.skills.Attribute(this, "calculateStun", calculateStun),
        new Torchlight.t2.skills.Attribute(this, "BleedDamage", calculateBleedDamage),
        new Torchlight.t2.skills.Attribute(this, "ChargeGenerated", calculateChargeGenerated)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps       = calculateDps(skillLevel);
        var stunChance      = calculateStun(skillLevel);
        var bleedDamage     = new Torchlight.lib.Range(calculateBleedDamage(skillLevel, playerLevel));
        var chargeGenerated = calculateChargeGenerated(skillLevel);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.buffs.Interrupt(interrupt),
            new Torchlight.t2.effects.Stun(stunChance, stunDuration),
            new Torchlight.t2.effects.spells.ChargeRefill(100, chargeGenerated)
        ];
        if (skillLevel >= 10) {
            effects.push(new Torchlight.t2.effects.Damage(bleedDamage, "Physical", bleedDuration));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDps(skillLevel) {
        return 28 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStun(skillLevel) {
        return Torchlight.lib.squeeze([50, 50, 50, 95], skillLevel / 5);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateBleedDamage(skillLevel, playerLevel) {
        return 67 + (3 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChargeGenerated(skillLevel) {
        return 4.5 + (0.5 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.lore.GlaiveSweep.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
