
Torchlight.t2.skills.outlander.lore.TanglingShot = function TanglingShot() {
    this.skillLevelRequiredTier = 1;
    this.name        = "tanglingshot";
    this.displayName = "Tangling Shot";
    this.description = "You fire a cursed projectile that binds and immobilizes your target. Enemies within a " +
                       "3.5 meter radius of your target have a secondary chance of also being entangled. Entangled " +
                       "foes are slowly crushed, taking damage over time.";
    this.tree        = "lore";
    this.getRange    = function getRange(skillLevel) {
        return Torchlight.lib.squeeze([3.5, 4.5, 5.5, 6.5], skillLevel / 5);
    };
    this.manaCost    = [0, 11, 11, 11, 12, 12, 13, 14, 15, 17, 19, 22, 24, 26, 29, 31];
    var secondaryEffectDuration = 3;
    this.tierBonuses = [
        "Range 4.5, Secondary immobilization chance 65%",
        "Range 5.5, Secondary immobilization chance 80%",
        "Range 6.5, Secondary immobilization chance 95%"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Secondary Effect Duration", secondaryEffectDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Primary Duration", calculatePrimaryDuration),
        new Torchlight.t2.skills.Attribute(this, "Poison Damage", calculatePoisonDamage),
        new Torchlight.t2.skills.Attribute(this, "Secondary Immobilization Chance", calculateSecondaryImmobilizeChance),
        new Torchlight.t2.skills.Attribute(this, "Secondary Poison Damage", calculateSecondaryPoisonDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var primaryDuration               = calculatePrimaryDuration(skillLevel);
        var poisonDamage                  = new Torchlight.lib.Range(calculatePoisonDamage(skillLevel, playerLevel));
        var secondaryImmobilizationChance = calculateSecondaryImmobilizeChance(skillLevel);
        var secondaryPoisonDamage         = new Torchlight.lib.Range(calculateSecondaryPoisonDamage(skillLevel, playerLevel));
        return [
            new Torchlight.t2.effects.Immobilize(100, primaryDuration),
            new Torchlight.t2.effects.Damage(poisonDamage, "Poison", primaryDuration),
            new Torchlight.t2.effects.Immobilize(secondaryImmobilizationChance, secondaryEffectDuration),
            new Torchlight.t2.effects.Damage(secondaryPoisonDamage, "Poison", secondaryEffectDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculatePrimaryDuration(skillLevel) {
        return 2.8 + (0.2 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculatePoisonDamage(skillLevel, playerLevel) {
        return 0.9 + (0.1 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateSecondaryImmobilizeChance(skillLevel) {
        return Torchlight.lib.squeeze([50, 65, 80, 95], skillLevel / 5);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateSecondaryPoisonDamage(skillLevel, playerLevel) {
        return 0.9 + (0.1 * skillLevel);
    }
};
Torchlight.t2.skills.outlander.lore.TanglingShot.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
