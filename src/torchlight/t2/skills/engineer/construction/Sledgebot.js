
Torchlight.t2.skills.engineer.construction.Sledgebot = function Sledgebot() {
    this.name = "sledgebot";
    this.displayName = "Sledgebot";
    this.description = "The Engineer assembles a large clockwork drone that does heavy melee damage. Summoning time increases with level, later tiers add special attacks.";
    this.tree = "construction";
    this.skillLevelRequiredTier = 6;
    this.manaCost = [0, 57, 58, 59, 60, 62, 64, 66, 69, 73, 80, 85, 89, 95, 100];
    var cooldown = 90;
    this.cooldown = [cooldown];
    this.getDuration = function getDuration(skillLevel) {
        return 27 + (3 * skillLevel);
    };
    this.tierBonuses = [
        "Sledgebot gains sweep attack",
        "Sledgebot gains smash attack",
        "Sledgebot gains rocket attack"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "All Damage", calculateAllDamage),
        new Torchlight.t2.skills.Attribute(this, "All Armor", calculateAllArmor),
        new Torchlight.t2.skills.Attribute(this, "Minion Damage", calculateMinionDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var allDamage = calculateAllDamage(skillLevel);
        var allArmor = calculateAllArmor(skillLevel);
        var minionDamage = calculateMinionDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.DamagePercent(allDamage, "All"),
            new Torchlight.t2.effects.buffs.ArmorPercent(allArmor, "All"),
            new Torchlight.t2.effects.MinionsDealDamage(minionDamage, "Physical")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAllDamage(skillLevel) {
        return 0 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateAllArmor(skillLevel) {
        return 0 + (5 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateMinionDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(18 + (2 * skillLevel));
    }

};
Torchlight.t2.skills.engineer.construction.Sledgebot.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
