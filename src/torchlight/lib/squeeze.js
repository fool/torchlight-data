/**
 * @param   {Array}  array
 * @param   {number} index
 * @returns {number}
 */
Torchlight.lib.squeeze = function(array, index) {
    return array[Math.max(Math.min(Torchlight.lib.truncate(index), array.length), 0)];
};
