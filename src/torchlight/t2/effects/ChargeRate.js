/**
 *
 * @param {number} percent
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.ChargeRate = function ChargeRate(percent, duration) {
    var word = "increased";
    if (percent < 0) {
        word = "reduced";
    }

    this.toString = function toString() {
        return "Charge rate " + word + " by " + percent + "%" + this.durationText(duration);
    }
};
Torchlight.t2.effects.ChargeRate.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
