
Torchlight.t2.skills.engineer.aegis.Forcefield = function Forcefield() {
    this.name = "forcefield";
    this.displayName = "Forcefield";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "aegis";
    this.skillLevelRequiredTier = 1;
    this.manaCost = [0, 32, 33, 34, 35, 28, 30, 33, 36, 41, 46, 52, 56, 63, 69, 48];
    var cooldown = 8;
    var duration = 30;
    this.cooldown = [cooldown];
    this.duration = [duration];
    this.tierBonuses = [
        "Mana cost and casting time are reduced by 20%",
        "Allies receive a full strength shield. ",
        "Mana cost and casting time are reduced by 50%"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration", duration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Absorption", calculateAbsorption),
        new Torchlight.t2.skills.Attribute(this, "Knockback Resistance", calculateKnockbackResistance)
    ];

    /**
     * http://www.abload.de/img/torchlight22012-09-23vou2o.png
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var knockbackResistance = calculateKnockbackResistance(skillLevel);
        var absorption = calculateAbsorption(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.DamageAbsorption(absorption, "All", duration),
            new Torchlight.t2.effects.KnockbackResistance(knockbackResistance)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateKnockbackResistance(skillLevel) {
        return -5 + (5 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateAbsorption(skillLevel, playerLevel) {
        return -5 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.aegis.Forcefield.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
