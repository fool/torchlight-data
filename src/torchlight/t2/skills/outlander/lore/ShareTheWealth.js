
Torchlight.t2.skills.outlander.lore.ShareTheWealth = function ShareTheWealth() {
    this.name = "sharethewealth";
    this.displayName = "Share The Wealth";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "lore";
    this.passive = true;
    this.skillLevelRequiredTier = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Shared Charge", calculateSharedCharge)
    ];

    /**
     * @param   {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var sharedCharge = calculateSharedCharge(skillLevel);
        return [
            new Torchlight.t2.effects.SharedCharge(sharedCharge)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateSharedCharge(skillLevel) {
        return 16 + (4 * skillLevel);
    }

};
Torchlight.t2.skills.outlander.lore.ShareTheWealth.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
