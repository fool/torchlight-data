
Torchlight.t2.skills.embermage.frost.IceBrand = function IceBrand() {
    this.name = "icebrand";
    this.displayName = "Ice Brand";
    this.description = "When you hit a frozen enemy, you do an additional burst of ice damage.";
    this.tree = "frost";
    this.skillLevelRequiredTier = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Ice Damage", calculateIceDamage)
    ];
    this.passive = true;

    /**
     * http://i.imgur.com/eT4o7.png
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var iceDamage = calculateIceDamage(skillLevel, playerLevel);
        return [
            new Torchlight.t2.effects.Damage(iceDamage, "Ice")
        ];
    };

    /**
     *
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateIceDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(1000, 5000);
    }
};
Torchlight.t2.skills.embermage.frost.IceBrand.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
