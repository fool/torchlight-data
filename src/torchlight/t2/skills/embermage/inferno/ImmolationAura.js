
Torchlight.t2.skills.embermage.inferno.ImmolationAura = function ImmolationAura() {
    this.name = "immolationaura";
    this.displayName = "Immolation Aura";
    this.description = "A vortex of flame surrounds you, damaging foes within a 3 meter radius.";
    this.tree = "inferno";
    this.skillLevelRequiredTier = 5;
    var cooldown = 1;
    var range = 3;
    this.cooldown = [cooldown];
    this.range = [range];
    this.manaCost = [0, 23, 23, 24, 24, 25, 25, 26, 27, 29, 31, 34, 36, 39, 41, 44];
    this.getDuration = function getDuration(skillLevel) {
        return 28 + (2 * skillLevel);
    };
    var fireDamageDuration = 1;

    this.tierBonuses = [
        "The aura absorbs 5% of all incoming damage",
        "The aura absorbs 10% of all incoming damage",
        "The aura absorbs 15% of all incoming damage"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Range", range),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage Duration", fireDamageDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Duration", this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "Damage Reduction %", calculateDamageReduction),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage", calculateFireDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var fireDamage = calculateFireDamage(skillLevel, playerLevel);
        var effects = [
            new Torchlight.t2.effects.Damage(fireDamage, "Fire", fireDamageDuration)
        ];
        if (skillLevel >= 5) {
            var damageReduction = calculateDamageReduction(skillLevel);
            effects.push(new Torchlight.t2.effects.DamageReduction(damageReduction, "All"));
        }

        return effects
    };

    /**
     * @param   {number}skillLevel
     * @returns {number}
     */
    function calculateDamageReduction(skillLevel) {
        return Torchlight.lib.squeeze([0, 5, 10, 15], skillLevel / 5);
    }

    /**
     * @param   {number}skillLevel
     * @param   {number}playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateFireDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(600);
    }
};
Torchlight.t2.skills.embermage.inferno.ImmolationAura.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
