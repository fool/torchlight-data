/**
 * @param   {number} percent
 * @constructor
 */
Torchlight.t2.effects.RangedBonusDamage = function RangedBonusDamage(percent) {
    this.toString = function toString() {
        return "+ " + percent + "% Ranged Weapon Damage bonus";
    }
};
Torchlight.t2.effects.RangedBonusDamage.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
