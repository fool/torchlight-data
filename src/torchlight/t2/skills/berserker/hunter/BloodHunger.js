
Torchlight.t2.skills.berserker.hunter.BloodHunger = function BloodHunger() {
    this.name = "bloodhunger";
    this.displayName = "Blood Hunger";
    this.description = "Whenever you land a critical strike with a melee weapon, you draw life essence from the carnage, healing your wounds.";
    this.tree = "hunter";
    this.passive = true;
    this.skillLevelRequiredTier = 0;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "% Health Healed", calculateHealRate)
    ];

    /**
     * @param {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var healRate = calculateHealRate(skillLevel);

        return [
            new Torchlight.t2.effects.HealOnCritical(healRate)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateHealRate(skillLevel) {
        return 4.5 + (0.5 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.hunter.BloodHunger.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
