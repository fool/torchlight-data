/**
 *
 * @param {number} percent
 * @constructor
 */
Torchlight.t2.effects.SharedCharge = function SharedCharge(percent) {
    this.toString = function toString() {
        return "Shared Charge benefits: " + percent + "%";
    }
};
Torchlight.t2.effects.SharedCharge.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
