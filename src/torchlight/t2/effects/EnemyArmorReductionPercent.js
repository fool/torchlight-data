/**
 *
 * @param {number} damageReduction
 * @param {string} damageType
 * @param {number} [duration]
 * @constructor
 */
Torchlight.t2.effects.EnemyArmorReductionPercent = function EnemyArmorReductionPercent(damageReduction, damageType, duration) {
    this.toString = function toString() {
        return damageReduction + "% to " + damageType + " Armor" + this.durationText(duration);
    }
};
Torchlight.t2.effects.EnemyArmorReductionPercent.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
