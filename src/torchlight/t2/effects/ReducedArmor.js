/**
 *
 * @param {string} armorType
 * @param {number} reduction how much armor to reduce
 * @param {number} [duration]  optional
 * @constructor
 */
Torchlight.t2.effects.ReducedArmor = function ReducedArmor(armorType, reduction, duration) {
    this.toString = function toString() {
        return armorType + " Armor is Reduced by " + reduction + this.durationText(duration);
    }
};
Torchlight.t2.effects.ReducedArmor.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);

