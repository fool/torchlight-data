
Torchlight.t2.effects.CastSpell = function CastSpell(percent, spell, location) {
    this.toString = function toString() {
        return percent + "% chance to cast " + spell + " from " + location;
    }
};
Torchlight.t2.effects.CastSpell.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
