
Torchlight.t2.skills.outlander.warfare.LongRangeMastery = function LongRangeMastery() {
    this.name = "longrangemastery";
    this.displayName = "Long Range Mastery";
    this.description = "You call forth a wall of thorned vines that prevent foes from approaching";
    this.tree = "warfare";
    this.passive = true;
    this.skillLevelRequiredTier = 0;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Ranged Weapon Damage", calculateRangedWeaponDamageBonus),
        new Torchlight.t2.skills.Attribute(this, "Extra Range", calculateExtraRange)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var rangedDamage = calculateRangedWeaponDamageBonus(skillLevel);
        var range = calculateExtraRange(skillLevel);
        return [
            new Torchlight.t2.effects.RangedBonusDamage(rangedDamage),
            new Torchlight.t2.effects.ExtraRange(range)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateRangedWeaponDamageBonus(skillLevel) {
        return 0 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateExtraRange(skillLevel) {
        var parts = [0, 0.33, 0.67];
        return (skillLevel / 3) + parts[skillLevel % 3];
    }

};
Torchlight.t2.skills.outlander.warfare.LongRangeMastery.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
