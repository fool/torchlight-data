
Torchlight.t2.effects.DamagePercent = function DamagePercent(percent, damageType, duration) {
    var d = "";
    if (duration) {
        d = "over " + duration + " seconds";
    }
    this.toString = function toString() {
        return "+ " + percent + " to " + damageType + " Damage" + d;
    }
};
Torchlight.t2.effects.DamagePercent.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);

