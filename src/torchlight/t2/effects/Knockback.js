
Torchlight.t2.effects.Knockback = function Knockback(value) {
    this.toString = function toString() {
        return "+" + value + " Knockback";
    }
};
Torchlight.t2.effects.Knockback.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
