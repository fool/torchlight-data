
Torchlight.t2.effects.HealOnCritical = function HealOnCritical(percent) {
    this.toString = function toString() {
        return "Each critical strike heals " + percent + "% of your maximum health";
    }
};
Torchlight.t2.effects.HealOnCritical.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
