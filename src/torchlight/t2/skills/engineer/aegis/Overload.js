
Torchlight.t2.skills.engineer.aegis.Overload = function Overload() {
    this.name = "overload";
    this.displayName = "Overload";
    this.description = "The Engineer deliberately short-circuits his armor to project a nova of intense Ember energy, electrocuting multiple foes in all directions. The damage is, in turn, multiplied by the number of Charges the Engineer has built up. Each Charge yields +50% damage";
    this.tree = "aegis";
    this.skillLevelRequiredTier = 2;
    this.manaCost = [0, 27, 28, 28, 30, 31, 33, 36, 38, 42, 46, 52, 57, 62, 69, 75];
    var cooldown = 1.4;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "Up to 7 foes in an 8 meter radius are electrocuted",
        "Up to 9 foes in an 10 meter radius are electrocuted",
        "Up to 11 foes in an 12 meter radius are electrocuted"
    ];
    var knockback = 50;
    var stunDuration = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", stunDuration),
        new Torchlight.t2.skills.Attribute(this, "Knockback", knockback),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS", calculateDps),
        new Torchlight.t2.skills.Attribute(this, "Electric Damage", calculateElectricDamage),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance", calculateStunChance)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var dps = calculateDps(skillLevel);
        var electricDamage = new Torchlight.lib.Range(calculateElectricDamage(skillLevel, playerLevel));
        var stunChance = calculateStunChance(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(dps, "Electric"),
            new Torchlight.t2.effects.Damage(electricDamage, "Electric"),
            new Torchlight.t2.effects.Knockback(knockback),
            new Torchlight.t2.effects.Stun(stunChance, stunDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDps(skillLevel) {
        return 19 + (1 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunChance(skillLevel) {
        return -5 + (5 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateElectricDamage(skillLevel, playerLevel) {
        return -5 + (5 * skillLevel);
    }
};
Torchlight.t2.skills.engineer.aegis.Overload.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
