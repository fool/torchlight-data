
Torchlight.t2.skills.outlander.warfare.VenomousHail = function VenomousHail() {
    this.name = "venomoushail";
    this.displayName = "Venomous Hail";
    this.description = "You fire a flurry of poisonous bolts skyward, which streak to earth in a 4 meter radius around the target location. The hail hits with 8 waves per flurry";
    this.tree = "warfare";
    this.skillLevelRequiredTier = 6;
    this.manaCost = [0, 33, 34, 34, 35, 36, 37, 38, 40, 42, 45, 46, 49, 52, 55, 58];
    this.getRange = function getRange(skillLevel) {
        if (skillLevel < 5) {
            return 4;
        }
        return 5.5;
    };
    var cooldown = 2;
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "Impact radius increased to 5.5 meters",
        "Duration of flurry increased to 12 waves",
        "Shatters shields "
    ];
    var shieldBreakChance = 100;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Shield Break", shieldBreakChance, 15),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Waves", calculateWaves),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS", calculateWeaponDps)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDps = calculateWeaponDps(skillLevel);
        var effects = [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Poison")
        ];

        if (skillLevel >= 15) {
            effects.push(new Torchlight.t2.effects.spells.BreakShields(shieldBreakChance));
        }
        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDps(skillLevel) {
        return 47 + (3 *skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWaves(skillLevel) {
        return Torchlight.lib.squeeze([8, 8, 12], skillLevel / 5);
    }
};
Torchlight.t2.skills.outlander.warfare.VenomousHail.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
