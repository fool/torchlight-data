
Torchlight.t2.skills.berserker.tundra.IceShield = function IceShield() {
    this.name = "iceshield";
    this.displayName = "Ice Shield";
    this.description = "You call forth an ice shield that reflects projectile missiles back at attackers.";
    this.tree = "tundra";
    this.skillLevelRequiredTier = 4;
    this.tierBonuses = [
        "The shield reflects 75% of missile damage",
        "The shield reflects 100% of missile damage",
        "The shield reflects 125% of missile damage"
    ];
    var duration = 10;
    var cooldown = 8;
    this.duration = [duration];
    this.cooldown = [cooldown];
    this.manaCost = [0, 30, 31, 31, 32, 33, 34, 35, 37, 39, 43, 47, 51, 55, 60, 64];

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown",        cooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration",        duration),
        new Torchlight.t2.skills.Attribute(this, "Mana Cost",       this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Recharge Chance", calculateChargeBarChance),
        new Torchlight.t2.skills.Attribute(this, "Reflect Chance",  calculateReflectChance),
        new Torchlight.t2.skills.Attribute(this, "Reflect Amount",  calculateReflectDps)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var chargeBarChance = calculateChargeBarChance(skillLevel);
        var reflectChance   = calculateReflectChance(skillLevel);
        var reflectDps      = calculateReflectDps(skillLevel);
        var chargeRefill    = new Torchlight.t2.effects.spells.ChargeRefill(10);

        return [
            new Torchlight.t2.effects.WhenStruck(chargeBarChance, chargeRefill),
            new Torchlight.t2.effects.ReflectMissiles(reflectChance, reflectDps)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChargeBarChance(skillLevel) {
        return 40 + (4 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateReflectChance(skillLevel) {
        return 45 + (5 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateReflectDps(skillLevel) {
        return Torchlight.lib.squeeze([50, 75, 100, 125], skillLevel / 5);
    }
};
Torchlight.t2.skills.berserker.tundra.IceShield.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
