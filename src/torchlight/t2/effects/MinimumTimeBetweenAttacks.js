
Torchlight.t2.effects.MinimumTimeBetweenAttacks = function MinimumTimeBetweenAttacks(seconds) {
    this.toString = function toString() {
        return "Minimum time between attacks: " + seconds + " seconds";
    }
};
Torchlight.t2.effects.MinimumTimeBetweenAttacks.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
