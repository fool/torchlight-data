
Torchlight.t2.skills.berserker.tundra.Stormclaw = function Stormclaw() {
    this.name     = "stormclaw";
    this.displayName = "Stormclaw";
    this.description = "You imbue your weapons with an electrical energy. This power can cause a lightning bolt to arc toward nearby targets when you strike a foe. The lightning can chain up to 2 times. Requires base melee weapon attacks to trigger";
    this.tree     = "tundra";
    this.skillLevelRequiredTier = 1;
    this.manaCost = [0, 18, 19, 21, 22, 24, 25, 28, 31, 34, 39, 43, 46, 52, 58, 64];
    var duration  = 40;
    var cooldown  = 20;
    this.duration = [duration];
    this.cooldown = [cooldown];
    this.tierBonuses = [
        "Lightning can chain up to 3 times",
        "Lightning can chain up to 4 times",
        "Lightning can chain up to 5 times"
    ];
    var lightningChance = 50;

    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Duration", duration),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Lightning Chance", lightningChance),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Weapon DPS %", calculateWeaponDpsPercent)
    ];

    this.getEffects = function getEffects(playerLevel, skillLevel) {
        var weaponDps = calculateWeaponDpsPercent(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(weaponDps, "Electric"),
            new Torchlight.t2.effects.CastSpell(lightningChance, "Lightning", "target")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDpsPercent(skillLevel) {
        return 37 + (skillLevel * 3);
    }
};
Torchlight.t2.skills.berserker.tundra.Stormclaw.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
