
Torchlight.t2.effects.ExplosionRadius = function ExplosionRadius(radius) {
    this.toString = function toString() {
        return "Explosion Radius: " + radius + " meters";
    }
};
Torchlight.t2.effects.ExplosionRadius.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
