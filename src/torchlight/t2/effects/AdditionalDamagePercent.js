/**
 * +6% Ice Damage
 *
 * @param {Number} percent     6
 * @param {String} damageType  "Ice Damage"
 * @constructor
 */
Torchlight.t2.effects.AdditionalDamagePercent = function AdditionalDamagePercent(percent, damageType) {
    this.toString = function toString() {
        return "+" + percent + "% " + damageType;
    }
};
Torchlight.t2.effects.AdditionalDamagePercent.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
