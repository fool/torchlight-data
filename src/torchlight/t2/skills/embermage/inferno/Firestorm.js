
Torchlight.t2.skills.embermage.inferno.Firestorm = function Firestorm() {
    this.name = "firestorm";
    this.displayName = "Firestorm";
    this.description = "You call down burning cinders from the sky over a 15 meter radius, setting foes alight and increasing their susceptibility to fire damage. Firestorm gains no Charge.";
    this.tree = "inferno";
    this.skillLevelRequiredTier = 6;
    this.manaCost = [0, 33, 34, 34, 35, 36, 38, 39, 40, 42, 45, 47, 49, 52, 56, 58];
    this.cooldown = function getCooldown(skillLevel) {
        return Math.max(2.6 - (0.1 * skillLevel), 0);
    };
    var duration = 6;
    var range = 15;
    var fireDamageReduction = 30;
    this.range = [range];
    this.duration = [duration];
    this.tierBonuses = [
        "Enemies do 30% less fire damage when affected",
        "Targets killed emit burning splinters",
        "Burning splinter range doubled"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Duration", duration),
        new Torchlight.t2.skills.Attribute(this, "Range", range),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage Reduction", fireDamageReduction, 5),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage", calculateFireDamage),
        new Torchlight.t2.skills.Attribute(this, "Fire Damage Taken", calculateFireDamageTaken),
        new Torchlight.t2.skills.Attribute(this, "Burning Splinter Damage", burningSplinterDamage, 10)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var fireDamage      = calculateFireDamage(skillLevel, playerLevel);
        var fireDamageTaken = calculateFireDamageTaken(skillLevel);
        var effects = [
            new Torchlight.t2.effects.Damage(fireDamage, "Fire", duration),
            new Torchlight.t2.effects.DamageTakenPercent(fireDamageTaken, "Fire", duration),
            new Torchlight.t2.effects.EnemyDamageReduced(fireDamageReduction, "Fire", duration)
        ];
        if (skillLevel >= 10) {
            var burningSplinterDamage = burningSplinterDamage(skillLevel);
            effects.push(new Torchlight.t2.effects.Damage(burningSplinterDamage, "Fire"));
        }

        return effects;
    };

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateFireDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(100, 500);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateFireDamageTaken(skillLevel) {
        return 18 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function burningSplinterDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(1000, 5000);
    }
};
Torchlight.t2.skills.embermage.inferno.Firestorm.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
