
Torchlight.t2.skills.engineer.construction.ShockGrenade = function ShockGrenade() {
    this.name = "shockgrenade";
    this.displayName = "Shock Grenade";
    this.description = "The Engineer lobs an electrically-charged grenade, which shocks and immobilizes nearby enemies. If the Engineer has at least one Charge, he instead lobs three grenades, at the cost of one Charge";
    this.tree = "construction";
    this.skillLevelRequiredTier = 4;
    this.manaCost = [0, 14, 14, 15, 15, 15, 16, 16, 17, 18, 20, 22, 24, 26, 28, 30];
    var cooldown = 1;
    this.cooldown = [cooldown];
    this.getRange = function getRange(skillLevel) {
        return Torchlight.lib.squeeze([4, 5, 5, 6], skillLevel / 5)
    };
    this.tierBonuses = [
        "Grenade shock radius grows to 5 meters",
        "Grenade stun time increased to 5 seconds",
        "Grenade shock radius grows to 6 meters and stun time increased to 6 seconds"
    ];
    var interruptChance = 100;
    var effectDuration = 4;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Interrupt Chance", interruptChance),
        new Torchlight.t2.skills.Attribute(this, "Shock/Damage Duration", effectDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Range", this.getRange),
        new Torchlight.t2.skills.Attribute(this, "Electric Damage", calculateElectricDamage),
        new Torchlight.t2.skills.Attribute(this, "Shock Chance", calculateShockChance),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance", calculateStunChance),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", calculateStunDuration)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var electricDamage = calculateElectricDamage(skillLevel, playerLevel);
        var shockChance = calculateShockChance(skillLevel);
        var stunChance = calculateStunChance(skillLevel);
        var stunDuration = calculateStunDuration(skillLevel);
        return [
            new Torchlight.t2.effects.Shock(shockChance, effectDuration),
            new Torchlight.t2.effects.Stun(stunChance, stunDuration),
            new Torchlight.t2.effects.Damage(electricDamage, "Electric", effectDuration),
            new Torchlight.t2.effects.buffs.Interrupt(interruptChance)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateShockChance(skillLevel) {
        return 18 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunChance(skillLevel) {
        return 48 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunDuration(skillLevel) {
        return Torchlight.lib.squeeze([4, 4, 5], skillLevel / 5);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateElectricDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(28 + (2 * skillLevel));
    }

};
Torchlight.t2.skills.engineer.construction.ShockGrenade.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
