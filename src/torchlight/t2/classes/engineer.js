Torchlight.t2.classes.engineer = {};
Torchlight.t2.classes.engineer.skills = {
    aegis: [
        new Torchlight.t2.skills.engineer.aegis.ShieldBash(),
        new Torchlight.t2.skills.engineer.aegis.Forcefield(),
        new Torchlight.t2.skills.engineer.aegis.Overload(),
        new Torchlight.t2.skills.engineer.aegis.DynamoField(),
        new Torchlight.t2.skills.engineer.aegis.Tremor(),
        new Torchlight.t2.skills.engineer.aegis.FireBash(),
        new Torchlight.t2.skills.engineer.aegis.ImmobilizationCopter(),
        new Torchlight.t2.skills.engineer.aegis.SwordAndBoard(),
        new Torchlight.t2.skills.engineer.aegis.AegisOfFate(),
        new Torchlight.t2.skills.engineer.aegis.ChargeReconstitution()
    ],
    blitz: [
        new Torchlight.t2.skills.engineer.blitz.FlameHammer(),
        new Torchlight.t2.skills.engineer.blitz.SeismicSlam(),
        new Torchlight.t2.skills.engineer.blitz.EmberHammer(),
        new Torchlight.t2.skills.engineer.blitz.Onslaught(),
        new Torchlight.t2.skills.engineer.blitz.EmberReach(),
        new Torchlight.t2.skills.engineer.blitz.StormBurst(),
        new Torchlight.t2.skills.engineer.blitz.Emberquake(),
        new Torchlight.t2.skills.engineer.blitz.HeavyLifting(),
        new Torchlight.t2.skills.engineer.blitz.Supercharge(),
        new Torchlight.t2.skills.engineer.blitz.CoupDeGrace()
    ],
    construction: [
        new Torchlight.t2.skills.engineer.construction.HealingBot(),
        new Torchlight.t2.skills.engineer.construction.BlastCannon(),
        new Torchlight.t2.skills.engineer.construction.SpiderMines(),
        new Torchlight.t2.skills.engineer.construction.GunBot(),
        new Torchlight.t2.skills.engineer.construction.ShockGrenade(),
        new Torchlight.t2.skills.engineer.construction.Fusillade(),
        new Torchlight.t2.skills.engineer.construction.Sledgebot(),
        new Torchlight.t2.skills.engineer.construction.Bulwark(),
        new Torchlight.t2.skills.engineer.construction.FireAndSpark(),
        new Torchlight.t2.skills.engineer.construction.ChargeDomination()
    ]
};
Torchlight.t2.classes.engineer.getSkill = function getSkill(name) {
    return Torchlight.t2.classes.character.getSkillFromTree(name, Torchlight.t2.classes.engineer.skills);
};
