
Torchlight.t2.skills.engineer.construction.FireAndSpark = function FireAndSpark() {
    this.name = "fireandspark";
    this.displayName = "Fire and Spark";
    this.description = "The Spark to make things work ... and fire to make things die.";
    this.tree = "construction";
    this.passive = true;
    this.skillLevelRequiredTier = 1;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Damage %", calculateDamage)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damage = calculateDamage(skillLevel);
        return [
            new Torchlight.t2.effects.DamagePercent(damage, "Fire"),
            new Torchlight.t2.effects.DamagePercent(damage, "Electric")
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateDamage(skillLevel) {
        return 0 + (5 * skillLevel);
    }

};
Torchlight.t2.skills.engineer.construction.FireAndSpark.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
