
Torchlight.t2.skills.berserker.shadow.SavageRush = function SavageRush() {
    this.name = "savagerush";
    this.displayName = "Savage Rush";
    this.description = "The shadow wolf spirit overtakes you! While the skill remains active, you dash continuously through foes, leaving them bleeding in your wake.";
    this.tree = "shadow";
    this.manaRate = "second";
    this.skillLevelRequiredTier = 3;
    this.manaCost =          [0, 12, 12, 13, 13, 14, 14, 15, 16, 17, 19, 21, 23, 25, 27, 30];
    var cooldown = 1;
    var duration = 5;
    var movementSpeed = 20;
    this.cooldown = [cooldown];
    this.duration = duration;
    this.tierBonuses = [
        "Enemy Armor decreased by 20% for 6 seconds",
        "Enemy Armor decreased by 30% for 8 seconds",
        "Enemy Armor decreased by 40% for 10 seconds"
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Cooldown", cooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration", duration),
        new Torchlight.t2.skills.Attribute(this, "Movement Speed", movementSpeed),
        new Torchlight.t2.skills.Attribute(this, "Weapon Damage", calculateWeaponDamage),
        new Torchlight.t2.skills.Attribute(this, "Physical Damage", calculatePhysicalDamage),
        new Torchlight.t2.skills.Attribute(this, "Armor Reduction", calculateEnemyArmorReduction)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var weaponDamage   = calculateWeaponDamage(skillLevel);
        var physicalDamage = calculatePhysicalDamage(skillLevel, playerLevel);
        var armorReduction = calculateEnemyArmorReduction(skillLevel);

        return [
            new Torchlight.t2.effects.PercentOfWeaponDamage(weaponDamage, "Physical"),
            new Torchlight.t2.effects.Damage(new Torchlight.lib.Range(physicalDamage), "Physical", duration),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(movementSpeed, Torchlight.t2.effects.Effect.speeds.Movement),
            new Torchlight.t2.effects.EnemyArmorDecreased(armorReduction.percent, armorReduction.duration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateWeaponDamage(skillLevel) {
        return 18 + (2 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculatePhysicalDamage(skillLevel, playerLevel) {
        return skillLevel * 5 + playerLevel * 2;
    }

    /**
     * @param   {number} skillLevel
     * @returns {object}
     */
    function calculateEnemyArmorReduction(skillLevel) {
        var percent  = Torchlight.lib.squeeze([10, 20, 30, 40], skillLevel / 5);
        var duration = Torchlight.lib.squeeze([4, 6, 8, 10], skillLevel / 5);
        return {
            percent: percent,
            duration: duration
        }
    }
};
Torchlight.t2.skills.berserker.shadow.SavageRush.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
