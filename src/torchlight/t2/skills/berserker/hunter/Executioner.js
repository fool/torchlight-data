
Torchlight.t2.skills.berserker.hunter.Executioner = function Executioner() {
    this.name = "executioner";
    this.displayName = "Executioner";
    this.description = "Your ferocity in combat enables you to more frequently strike with two weapons at once. When you successfully execute, you gain Charge more quickly for a short period of time. Only effective when wielding two weapons of the same type.";
    this.passive = true;
    this.tree = "hunter";
    this.skillLevelRequiredTier = 1;
    var duration = 2;
    this.duration = [duration];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Duration",             duration),
        new Torchlight.t2.skills.Attribute(this, "Chance To Execute",    calculateChanceToExecute),
        new Torchlight.t2.skills.Attribute(this, "Charge Rate Increase", calculateChargeRateIncrease)
    ];

    /**
     * @param {number} skillLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel) {
        var executionRate      = calculateChanceToExecute(skillLevel);
        var chargeRateIncrease = calculateChargeRateIncrease(skillLevel);

        return [
            new Torchlight.t2.effects.Execute(executionRate),
            new Torchlight.t2.effects.ChargeRate(chargeRateIncrease, this.getDuration(skillLevel))
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToExecute(skillLevel) {
        return 0 + (2 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChargeRateIncrease(skillLevel) {
        return 3 + (3 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.hunter.Executioner.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
