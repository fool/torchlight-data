/**
 * http://stackoverflow.com/questions/661562/how-to-format-a-float-in-javascript
 *
 * @param   {number} value       The float to round
 * @param   {number} [precision] How many digits you want to include
 * @returns {String} Always gauranteed to be a whole number
 */
Torchlight.lib.toFixed = function toFixed(value, precision) {
    var power = Math.pow(10, precision || 0);
    return String(Math.round(value * power) / power);
};
