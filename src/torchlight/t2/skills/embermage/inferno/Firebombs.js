
Torchlight.t2.skills.embermage.inferno.Firebombs = function Firebombs() {
    this.name = "firebombs";
    this.displayName = "Firebombs";
    this.description = "You hurl three gouts of flame that ignite the ground for 3 seconds and sometimes cause foes to stumble in panic as they burn. Firebombs gain no Charge.";
    this.tree = "inferno";
    this.skillLevelRequiredTier = 2;
    this.manaCost = [0, 17, 17, 18, 18,  19,  20,  22,  24, 26,   29,  33,  36,  39,  42,  47];
    this.cooldown = [0,  2,  2,  2,  2, 1.6, 1.6, 1.6, 1.6, 1.6, 1.2, 1.2, 1.2, 1.2, 1.2, 0.8];
    this.duration = [0,  3,  3,  3,  3,   4,   4,   4,   4,   4,   5,   5,   5,   5,   5,   6];
    this.tierBonuses = [
        "Pools of flame last 4 seconds. Cooldown time reduced to 1.6 seconds.",
        "Pools of flame last 5 seconds. Cooldown time reduced to 1.2 seconds.",
        "Pools of flame last 6 seconds. Cooldown time reduced to .8 seconds."
    ];
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Cooldown", this.getCooldown),
        new Torchlight.t2.skills.Attribute(this, "Duration", this.getDuration),
        new Torchlight.t2.skills.Attribute(this, "% Movement Slowdown", calculateMovementSlowdown),
        new Torchlight.t2.skills.Attribute(this, "Chance to Flee", calculateChanceToFlee),
        new Torchlight.t2.skills.Attribute(this, "Slow Duration", calculateSlowDuration)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var fireDamage       = calculateFireDamage(skillLevel, playerLevel);
        var movementSlowdown = calculateMovementSlowdown(skillLevel);
        var chanceToFlee     = calculateChanceToFlee(skillLevel);
        var slowDuration     = calculateSlowDuration(skillLevel);
        return [
            new Torchlight.t2.effects.Damage(fireDamage, "Fire", slowDuration),
            new Torchlight.t2.effects.debuffs.SpeedDecrease(movementSlowdown, Torchlight.t2.effects.Effect.speeds.Movement, slowDuration),
            new Torchlight.t2.effects.spells.Flee(chanceToFlee, slowDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateFireDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(9 + (3 * skillLevel));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateMovementSlowdown(skillLevel) {
        return -1 * (9 + (3 * skillLevel));
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToFlee(skillLevel) {
        return skillLevel;
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateSlowDuration(skillLevel) {
        return 1.9 + (0.1 * skillLevel);
    }


};
Torchlight.t2.skills.embermage.inferno.Firebombs.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
