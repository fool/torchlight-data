
Torchlight.t2.effects.EnemyDamageReduced = function EnemyDamageReduced(damageReduction, damageType, duration) {
    this.toString = function toString() {
        return "Enemy " + damageType + " damage is reduced by " + damageReduction + "%" + this.durationText(duration);
    }
};
Torchlight.t2.effects.EnemyDamageReduced.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
