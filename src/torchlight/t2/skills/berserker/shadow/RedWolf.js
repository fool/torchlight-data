
Torchlight.t2.skills.berserker.shadow.RedWolf = function RedWolf() {
    this.name = "redwolf";
    this.displayName = "RedWolf";
    this.description = "Whenever you inflict a critical hit, up to two neighboring enemies may be savaged by a manifestation of your bloodlust.";
    this.tree = "shadow";
    this.passive = true;
    this.skillLevelRequiredTier = 2;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Damage", calculateDamage)
    ];

    /**
     * @param {number} skillLevel
     * @param {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var damage = calculateDamage(skillLevel, playerLevel);

        return [
            new Torchlight.t2.effects.Damage(new Torchlight.lib.Range(damage), "Physical")
        ];
    };

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {number}
     */
    function calculateDamage(skillLevel, playerLevel) {
        return 2.9 + (1 * skillLevel);
    }
};
Torchlight.t2.skills.berserker.shadow.RedWolf.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
