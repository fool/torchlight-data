/**
 * -3% Phyical Damage taken for each monster within 4m
 *
 * @param {number} percent
 * @param {string} damageType
 * @param {number} range
 * @constructor
 */
Torchlight.t2.effects.DamageTakenForMonster = function DamageTakenForMonster(percent, damageType, range) {
    var sign = "-";
    if (percent === 0) {
        sign = "";
    } else if (percent > 0) {
        sign = "+";
    }
    this.toString = function toString() {
        return sign + percent + "% " + damageType + " damage taken for each monster within " + range + "m";
    }
};
Torchlight.t2.effects.DamageTakenForMonster.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
