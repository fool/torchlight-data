
Torchlight.t2.skills.embermage.storm.ShockingBurst = function ShockingBurst() {
    this.name = "shockingburst";
    this.displayName = "Shocking Burst";
    this.description = "You unleash channeled bursts of crackling energy, shocking and stunning all foes up to 8 meters away in a 75 degree angle. The energy burst continues and drains mana while the mouse button is held down";
    this.tree = "storm";
    this.manaRate = "second";
    this.skillLevelRequiredTier = 1;
    this.manaCost = [0,  19,  20,  21,  22,  23,  25,  27,  30,  34,  38,  42,  46,  51,  57,  63];
    this.arc      = [0,  75,  75,  75,  75, 105, 105, 105, 105, 105, 135, 135, 135, 135, 135, 165];
    this.tierBonuses = [
        "Arc widened to 105",
        "Arc widened to 135",
        "Arc widened to 165"
    ];
    var electricDamageDuration = 1;
    this.attributes = [
        new Torchlight.t2.skills.Attribute(this, "Electric Damage Duration", electricDamageDuration),
        new Torchlight.t2.skills.Attribute(this, "Mana", this.getManaCost),
        new Torchlight.t2.skills.Attribute(this, "Arc", this.getArc),
        new Torchlight.t2.skills.Attribute(this, "Electric DPS %", calculateElectricDps),
        new Torchlight.t2.skills.Attribute(this, "Electric Damage", calculateElectricDamage),
        new Torchlight.t2.skills.Attribute(this, "Stun Chance", calculateChanceToStun),
        new Torchlight.t2.skills.Attribute(this, "Stun Duration", calculateStunDuration)
    ];

    /**
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.t2.effects[]}
     */
    this.getEffects = function getEffects(skillLevel, playerLevel) {
        var electricDps = calculateElectricDps(skillLevel);
        var electricDamage = calculateElectricDamage(skillLevel, playerLevel);
        var stunChance = calculateChanceToStun(skillLevel);
        var stunDuration = calculateStunDuration(skillLevel);
        return [
            new Torchlight.t2.effects.PercentOfWeaponDps(electricDps, "Electric"),
            new Torchlight.t2.effects.Damage(electricDamage, "Electric", electricDamageDuration),
            new Torchlight.t2.effects.ExplosionOnDeath(),
            new Torchlight.t2.effects.Stun(stunChance, stunDuration)
        ];
    };

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateElectricDps(skillLevel) {
        return 19 + (1 * skillLevel);
    }

    /**
     * @fixme
     * @param   {number} skillLevel
     * @param   {number} playerLevel
     * @returns {Torchlight.lib.Range}
     */
    function calculateElectricDamage(skillLevel, playerLevel) {
        return new Torchlight.lib.Range(500, 1000);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateChanceToStun(skillLevel) {
        return 11 + (1 * skillLevel);
    }

    /**
     * @param   {number} skillLevel
     * @returns {number}
     */
    function calculateStunDuration(skillLevel) {
        return 0.9 + (0.1 * skillLevel);
    }
};
Torchlight.t2.skills.embermage.storm.ShockingBurst.prototype = Object.create(Torchlight.t2.skills.Skill.prototype);
