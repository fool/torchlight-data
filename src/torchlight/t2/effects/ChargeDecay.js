/**
 *
 * @param percent
 * @param [duration]
 * @constructor
 */
Torchlight.t2.effects.ChargeDecay = function ChargeDecay(percent, duration) {
    var word = "increased";
    if (percent < 0) {
        word = "reduced";
    }

    this.toString = function toString() {
        return "Charge decay reate " + word + " by " + percent + "%" + this.durationText(duration);
    }
};
Torchlight.t2.effects.ChargeDecay.prototype = Object.create(Torchlight.t2.effects.Effect.prototype);
